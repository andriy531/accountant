import { createMuiTheme } from 'material-ui/styles';

export const theme = createMuiTheme({
  overrides: {
    MuiInput: {
      input: {
        fontFamily: 'SF-Display-Light, sans-serif',
        fontSize: '14px'
      },
      underline: {
        '&:hover:before': {
          backgroundColor: '#7d68a3 !important'
        }

      },
      inkbar: {
        '&:after': {
          backgroundColor: '#5BC2A8'
        }
      }
    },
    MuiInputLabel: {
      root: {
        color: '#757575',
        fontFamily: 'SF-Display-Light, sans-serif',
        fontSize: '14px'
      }
    },
    MuiDialog: {
      root: {
        fontFamily: 'SF-Display-Light, sans-serif'
      },
      paper: {
        borderRadius: '5px'
      },
      paperWidthSm: {
        maxWidth: '75vw'
      }
    },
    MuiDialogContentText: {
      root: {
        fontFamily: 'SF-Display-Light, sans-serif'
      }
    },
    MuiTypography: {
      root: {
        fontFamily: 'SF-Display-Light, sans-serif'
      },
      title: {
        fontFamily: 'SF-Display-Light, sans-serif'
      },
      body1: {
        fontFamily: 'SF-Display-Light, sans-serif'
      },
      body2: {
        fontFamily: 'SF-Display-Light, sans-serif'
      }
    },
    MuiButton: {
      root: {
        fontFamily: 'SF-Display-Regular, sans-serif'
      }
    },
    MuiListItemText: {
      text: {
        fontFamily: 'SF-Display-Light, sans-serif',
      }
    },
    MuiTab: {
      root: {
        fontFamily: 'SF-Display-Regular, sans-serif'
      }
    },
    MuiTabIndicator: {
      colorAccent: {
        backgroundColor: '#7D68A3'
      }
    },
    MuiCheckbox: {
      checked: {
        color: '#5BC2A8'
      }
    },
    MuiMenuItem: {
      root: {
        fontFamily: 'SF-Display-Light, sans-serif'
      }
    },
    MuiFormHelperText: {
      root: {
        fontFamily: 'SF-Display-Light, sans-serif'
      }
    },
    MuiRadio: {
      checked: {
        color: '#5BC2A8',
      }
    },
    MuiTooltip: {
      tooltip: {
        backgroundColor: '#fff',
        fontFamily: 'SF-Display-Light, sans-serif',
        color: '#000',
        opacity: '1 !important',
        boxShadow: '0px 5px 5px -3px rgba(0, 0, 0, 0.2), 0px 8px 10px 1px rgba(0, 0, 0, 0.14), 0px 3px 14px 2px rgba(0, 0, 0, 0.12)',
      }
    },
    MuiLinearProgress: {
      primaryColorBar: {
        backgroundColor: '#5BC2A8'
      },
      primaryColor: {
        backgroundColor: '#d8dae1'
      }
    }
  }
});
