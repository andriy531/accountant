import React from 'react'
import { Redirect } from 'react-router-dom'
import SideMenu from '../containers/SideMenu'
import AppHeader from '../containers/AppHeader'
import CreateService from '../containers/CreateService'
import CreateClient from '../containers/CreateClient'
import './index.css'

export function requireAuthentication(Component) {
  class AuthenticatedComponent extends React.Component {
    constructor(props){
    	super(props);
    	this.state = {
        isLoggedIn: false
      };
    }
    componentWillMount() {
      const cpa_data = localStorage.getItem('cpa_data');
      if(cpa_data) {
        this.setState({isLoggedIn: true})
      }
    }
    render () {
      return this.state.isLoggedIn
        ? <div id="app-view">
            <SideMenu/>
            <div id="page-view">
              <AppHeader/>
              <Component {...this.props}/>
              <CreateService/>
              <CreateClient/>
            </div>
          </div>
        : <Redirect to="/signin"/>
    }
  }

  return AuthenticatedComponent;
}
