import React, { Component } from 'react'
import './filters.css'
import Button from 'material-ui/Button'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import { FormControl } from 'material-ui/Form'
import Input, { InputLabel } from 'material-ui/Input'
import Select from 'material-ui/Select'
import TextField from 'material-ui/TextField'
import { MenuItem } from 'material-ui/Menu'
import Radio, { RadioGroup } from 'material-ui/Radio'
import { FormControlLabel } from 'material-ui/Form'
import moment from 'moment'
import Search from '../searchField'
import { getServicesSearchAutosuggestApi } from '../../services/api'


class CustomInput extends Component {
  render() {
    return (
      <TextField
        placeholder={this.props.placeholder}
        onClick={this.props.onClick}
        onChange={this.props.onChange}
        value={this.props.value}
        error={this.props.error}
      />
    )
  }
}

export default class ServicesFilters extends Component {

  constructor(props) {
  	super(props);
  	this.state = {
      dateFilterType: 'date-range',
      fiscalEndYearType: 'date-range',
      engagementsType: 'number-range',
      filters: {
        sortByDate: '',
        sortByName: '',
        sortByBillingAmount: '',
        filterByBillingType: '',
        fromDate: '',
        toDate: '',
        fromEngagements: '',
        toEngagements: '',
        searchQuery: '',
      },
    };
  }

  handleChangeFilterType(e, prop) {
    let updatedFilters = this.state.filters;
    if(prop === 'dateFilterType') {
      updatedFilters = {
        ...updatedFilters,
        fromDate: '',
        toDate: ''
      }
    }
    if(prop === 'engagementsType') {
      updatedFilters = {
        ...updatedFilters,
        fromEngagements: '',
        toEngagements: ''
      }
    }
    this.setState({[prop]: e.target.value, filters: updatedFilters})
  }

  onChangeFilter(data, notFilterImmediately) {
    let filters = { ...this.state.filters, ...data };
    if(Object.keys(data)[0] === 'sortByName') {
      filters.sortByDate = '';
      filters.sortByBillingAmount = '';
    } else if(Object.keys(data)[0] === 'sortByDate') {
      filters.sortByName = '';
      filters.sortByBillingAmount = '';
    } else if(Object.keys(data)[0] === 'sortByBillingAmount') {
      filters.sortByName = '';
      filters.sortByDate = '';
    }

    this.setState({filters})
    if(!notFilterImmediately) {
      this.handleFilter(filters);
    }
  }

  handleFilter(filters) {
    this.props.handleFilter(filters);
  }

  resetFilter() {
    const filters = {
      sortByDate: '',
      sortByName: '',
      sortByBillingAmount: '',
      filterByBillingType: '',
      fromDate: '',
      toDate: '',
      fromEngagements: '',
      toEngagements: '',
      searchQuery: '',
    }
    this.setState({ filters })
    this.props.resetFilter();
  }

  fetchSuggestion(value) {
    return getServicesSearchAutosuggestApi(value);
  }

  render() {
    const { sortByName, sortByDate, sortByBillingAmount, filterByBillingType, fromDate, toDate, fromEngagements, toEngagements } = this.state.filters;
    return (
      <div id="services-filters">
        <div className="flex-columns">
          <div className="filter-column">
            <div className="col-wrap">
              <FormControl className="form-control">
                <InputLabel htmlFor="name">Service name</InputLabel>
                <Select
                  value={sortByName}
                  onChange={(e) => this.onChangeFilter({sortByName: e.target.value})}
                  input={<Input id="name" />}
                  className="select"
                >
                  <MenuItem value=""><span className="select-item">None</span></MenuItem>
                  <MenuItem value={"asc"}><span className="select-item">A...Z</span></MenuItem>
                  <MenuItem value={"desc"}><span className="select-item">Z...A</span></MenuItem>
                </Select>
              </FormControl>
              <br/>
              <FormControl className="form-control">
                <InputLabel htmlFor="type">Billing amount</InputLabel>
                <Select
                  value={sortByBillingAmount}
                  onChange={(e) => this.onChangeFilter({sortByBillingAmount: e.target.value})}
                  input={<Input id="type" />}
                  className="select"
                >
                  <MenuItem value=""><span className="select-item">None</span></MenuItem>
                  <MenuItem value={"desc"}><span className="select-item">Ascending</span></MenuItem>
                  <MenuItem value={"asc"}><span className="select-item">Descending</span></MenuItem>
                </Select>
              </FormControl>
            </div>
            <div className="col-wrap">
              <FormControl className="form-control">
                <InputLabel htmlFor="registration">Date created</InputLabel>
                <Select
                  value={sortByDate}
                  onChange={(e) => this.onChangeFilter({sortByDate: e.target.value})}
                  input={<Input id="registration" />}
                  className="select"
                >
                  <MenuItem value=""><span className="select-item">None</span></MenuItem>
                  <MenuItem value={"desc"}><span className="select-item">Recently</span></MenuItem>
                  <MenuItem value={"asc"}><span className="select-item">Old</span></MenuItem>
                </Select>
              </FormControl>
              <br/>
              <FormControl className="form-control">
                <InputLabel htmlFor="status">Billing type</InputLabel>
                <Select
                  value={filterByBillingType}
                  onChange={(e) => this.onChangeFilter({filterByBillingType: e.target.value})}
                  input={<Input id="status" />}
                  className="select"
                >
                  <MenuItem value=""><span className="select-item">None</span></MenuItem>
                  {this.props.billings && this.props.billings.map((b, i) => {
                    return <MenuItem key={i} value={b.billingType}><span className="select-item">{b.billingType}</span></MenuItem>
                  })}
                </Select>
              </FormControl>
            </div>
            <div className="col-wrap">
              <FormControl className="form-control">
                <InputLabel htmlFor="searchBy" shrink={true}>Search by</InputLabel>
                <p className="search-by">Service name</p>
              </FormControl>
              <br/>
              <FormControl className="date-range-form">
                <div className="controlled-label">
                  <span>Created: </span>
                  <FormControl component="fieldset">
                    <RadioGroup
                      aria-label="date"
                      name="date"
                      className="radio-group"
                      value={this.state.dateFilterType}
                      onChange={(e) => this.handleChangeFilterType(e, 'dateFilterType')}
                    >
                      <FormControlLabel className="control-label" value="date" control={<Radio className="radio-btn"/>} label="Date" />
                      <FormControlLabel className="control-label" value="date-range" control={<Radio className="radio-btn"/>} label="Date range" />
                    </RadioGroup>
                  </FormControl>
                </div>
                {this.state.dateFilterType === 'date-range' ? <div className="date-range-view">
                  <DatePicker
                    selected={fromDate ? moment(fromDate, "MM-DD-YYYY") : ''}
                    selectsStart
                    startDate={fromDate ? moment(fromDate, "MM-DD-YYYY") : ''}
                    endDate={toDate ? moment(toDate, "MM-DD-YYYY") : ''}
                    onChange={(date) => this.onChangeFilter({fromDate: date ? moment(date).format('MM-DD-YYYY') : ''})}
                    placeholderText="Start date"
                    customInput={<CustomInput label="Start date"/>}
                  />
                  <DatePicker
                      selected={toDate ? moment(toDate, "MM-DD-YYYY") : ''}
                      selectsEnd
                      startDate={fromDate ? moment(fromDate, "MM-DD-YYYY") : ''}
                      endDate={toDate ? moment(toDate, "MM-DD-YYYY") : ''}
                      onChange={(date) => this.onChangeFilter({toDate: date ? moment(date).format('MM-DD-YYYY') : ''})}
                      placeholderText="End date"
                      customInput={<CustomInput label="End date"/>}
                  />
                </div> : <div className="date-range-view"><DatePicker
                  selected={fromDate ? moment(fromDate, "MM-DD-YYYY") : ''}
                  onChange={(date) => {
                    let parsedDate = date ? moment(date).format('MM-DD-YYYY') : '';
                    this.onChangeFilter({fromDate: parsedDate, toDate: parsedDate})}
                  }
                  placeholderText="Date"
                  customInput={<CustomInput label="Date"/>}
                /></div>}
              </FormControl>
            </div>
            <div className="col-wrap">
              <FormControl className="form-control wide">
                <div className="search-view">
                  <Search
                    fetchSuggestion={this.fetchSuggestion.bind(this)}
                    handleChange={(value) => this.onChangeFilter({searchQuery: value}, true)}
                    label={"Search"}
                    placeholder="Service name"
                    style={{fontSize: '13px', width: '215px'}}
                    value={this.state.filters.searchQuery}
                  />
                </div>
              </FormControl>
              <br/>
              <FormControl className="form-control wide engagements-control">
                <div className="controlled-label">
                  <span>Engagements: </span>
                  <FormControl component="fieldset">
                    <RadioGroup
                      aria-label="engagements"
                      name="engagements"
                      className="radio-group"
                      value={this.state.engagementsType}
                      onChange={(e) => this.handleChangeFilterType(e, 'engagementsType')}
                    >
                      <FormControlLabel className="control-label" value="number" control={<Radio className="radio-btn"/>} label="Number" />
                      <FormControlLabel className="control-label" value="number-range" control={<Radio className="radio-btn"/>} label="Range" />
                    </RadioGroup>
                  </FormControl>
                </div>
                {this.state.engagementsType === 'number-range' ?
                  <div className="number-range">
                    <TextField
                      value={fromEngagements}
                      type="number"
                      placeholder="from"
                      className="number"
                      onChange={(e) => this.onChangeFilter({fromEngagements: e.target.value})}
                    />
                    <TextField
                      value={toEngagements}
                      type="number"
                      placeholder="to"
                      className="number"
                      onChange={(e) => this.onChangeFilter({toEngagements: e.target.value})}
                    />
                  </div> : <div className="number-range">
                    <TextField
                      value={fromEngagements}
                      type="number"
                      placeholder="number"
                      className="number"
                      onChange={(e) => this.onChangeFilter({fromEngagements: e.target.value, toEngagements: e.target.value})}
                    />
                  </div>
                }
              </FormControl>
            </div>
          </div>
        </div>
        <div className="filter-actions">
          <Button className="green-btn filter-btn" onClick={() => this.handleFilter(this.state.filters)}>
            Search
          </Button>
          <Button className="btn reset-btn" onClick={() => this.resetFilter()}>
            Reset
          </Button>
        </div>
      </div>
    )
  }
}
