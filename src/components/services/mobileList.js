import React, { Component } from 'react'
import './mobileList.css'
import FilterListIcon from 'material-ui-icons/FilterList'
import Button from 'material-ui/Button'
import ExpandMoreIcon from 'material-ui-icons/ExpandMore'
import IconButton from 'material-ui/IconButton'
import Collapse from 'material-ui/transitions/Collapse'
import Divider from 'material-ui/Divider'
import moment from 'moment'
import ReactPaginate from 'react-paginate'
import KeyboardArrowLeftIcon from 'material-ui-icons/KeyboardArrowLeft'
import KeyboardArrowRightIcon from 'material-ui-icons/KeyboardArrowRight'
import ServicesFiltersMobile from './filtersMobile'


export default class ServicesMobile extends Component {
  constructor(props){
  	super(props);
  	this.state = {
      expandedId: null,
      openFilters: false
    };
  }

  handleToggleExpand(expandedId) {
    this.setState({expandedId: expandedId === this.state.expandedId ? null : expandedId})
  }

  toggleFiltersView() {
    this.setState({openFilters: !this.state.openFilters})
  }

  render() {
    const { openFilters } = this.state;
    return (
      <div id="services-list-mobile">
        <div className="filter-view">
          <p className="filtered-by">{this.props.filteredBy && !openFilters && 'Filtered'}</p>
          <p
            className="filter-switch"
            onClick={() => this.toggleFiltersView()}
            >Filter services <FilterListIcon className="filter-icon"/>
          </p>
        </div>
        {openFilters ? <div>
          <ServicesFiltersMobile
            handleFilter={this.props.handleFilter}
            resetFilter={this.props.resetFilters}
            billings={this.props.billings}
            toggleFiltersView={this.toggleFiltersView.bind(this)}
            filters={this.props.filters}
          />
        </div> : <div>
          {
            this.props.services.map((s, i) => {
              return  (<div className="service-card" key={i}>
                <div className="info-view">
                  <div className="service-info">
                    <div className="name-block">
                      <p>{s.name}</p>
                      <IconButton
                        className={this.state.expandedId === s.service_id ? 'expand expandOpen': 'expand'}
                        onClick={() => this.handleToggleExpand(s.service_id)}
                        aria-label="Show more"
                      >
                          <ExpandMoreIcon/>
                      </IconButton>
                    </div>
                    <span className="service-date">{moment(s.dateCreated).format('DD/MM/YYYY')}</span>
                  </div>
                </div>
                <Collapse in={this.state.expandedId === s.service_id} transitionDuration="auto" unmountOnExit>
                  <Divider/>
                  <div className="collapsed-view">
                    <table className="info-table">
                      <tbody>
                        <tr>
                          <td>Billing amount</td>
                          <td>{s.billingAmount}</td>
                        </tr>
                        <tr>
                          <td>Billing type</td>
                          <td>{s.billingType}</td>
                        </tr>
                        <tr>
                          <td>Engagements</td>
                          <td>{s.associatedEngagementsNumber}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div className="actions">
                    <Button
                      onClick={() => this.props.onEditClick(s)}
                    >
                        edit
                    </Button>
                    <Button
                      onClick={() => this.props.onCopyClick(s)}
                    >
                        copy
                    </Button>
                    <Button
                      onClick={() => this.props.onDeleteClick(s)}
                    >
                        delete
                    </Button>
                  </div>
                </Collapse>
              </div>)
            })
          }
          {
            !!this.props.services.length && <ReactPaginate previousLabel={<IconButton aria-label="previeus"><KeyboardArrowLeftIcon/></IconButton>}
                       nextLabel={<IconButton aria-label="next"><KeyboardArrowRightIcon/></IconButton>}
                       breakLabel={<a href="">...</a>}
                       breakClassName={"break-me"}
                       pageCount={this.props.pageCount}
                       marginPagesDisplayed={1}
                       pageRangeDisplayed={2}
                       onPageChange={(page) => this.props.handlePageClick(page)}
                       containerClassName={"pagination"}
                       activeClassName={"active"}
                       disabledClassName={"disabled"}
                       forcePage={this.props.page_number - 1}
                     />
           }
         </div>
       }
      </div>
    )
  }
}
