import React from 'react'
import './basicInformation.css'
import TextField from 'material-ui/TextField'
import Select from 'react-select'

const BasicInfo = () => {
  return (
    <div id="basic-information-view">
      <h3 className="card-header">Basic information</h3>
      <p className="additional-text">input basic information about your engagement</p>
      <TextField
        id="engagement-name"
        label="Engagement Name"
        margin="normal"
        fullWidth
        placeholder="Enter Your Engagement Name"
      />
      <div className="two-fields-line">
        <TextField
          id="start-date"
          label="Start date"
          type="date"
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
          fullWidth
        />
        &emsp;&emsp;
        <TextField
          id="deadline"
          label="Deadline"
          type="date"
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
          fullWidth
        />
      </div>
      <div className="two-fields-line">
        <div className="select-wrapper">
          <p className="select-label">Client</p>
          <Select
            name="client"
            value="one"
            className="select-field"
            placeholder="Select Client"
          />
        </div>
        &emsp;&emsp;
        <div className="select-wrapper">
          <p className="select-label">Partner</p>
          <Select
            name="partner"
            value="one"
            className="select-field"
            placeholder="Select Partner"
          />
        </div>
      </div>
      <TextField
        id="description"
        label="Description"
        multiline
        rowsMax="4"
        margin="normal"
        placeholder="Enter engagement description"
        fullWidth
      />
    </div>
  )
}

export default BasicInfo
