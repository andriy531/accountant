import React from 'react'
import './services.css'
import Select from 'react-select'
import Check from 'material-ui-icons/Check'
import FaPencil from 'react-icons/lib/fa/pencil'
import FaTrash from 'react-icons/lib/fa/trash'


const ServicesInfo = () => {
  return (
    <div id="services-view">
      <h3 className="card-header">Services</h3>
      <p className="additional-text">Choose one or more services</p>
      <div className="select-wrapper">
        <p className="select-label">Choose service</p>
        <Select
          name="service"
          value="one"
          className="select-field"
          placeholder="Select one or few services"
        />
      </div>
      <div className="services-wrap">
        <div className="service-item-wrap">
          <Check className="check-icon"/>
          <div className="service-item">
            <p>Bookkeeping and accountant for whole company</p>
            <p className="billing-text">Total Billing: <span className="price">$350.00</span></p>
            <div>
              <span className="action"><FaPencil className="action-icon"/>edit</span>&emsp;
              <span className="action"><FaTrash className="action-icon"/>delete</span>
            </div>
          </div>
        </div>
        <div className="service-item-wrap">
          <Check className="check-icon"/>
          <div className="service-item">
            <p>Bookkeeping and accountant for whole company</p>
            <p className="billing-text">Total Billing: <span className="price">$350.00</span></p>
            <div>
              <span className="action"><FaPencil className="action-icon"/>edit</span>&emsp;
              <span className="action"><FaTrash className="action-icon"/>delete</span>
            </div>
          </div>
        </div>
        <div className="service-item-wrap">
          <Check className="check-icon"/>
          <div className="service-item">
            <p>Bookkeeping and accountant for whole company</p>
            <p className="billing-text">Total Billing: <span className="price">$350.00</span></p>
            <div>
              <span className="action"><FaPencil className="action-icon"/>edit</span>&emsp;
              <span className="action"><FaTrash className="action-icon"/>delete</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ServicesInfo
