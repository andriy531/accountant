import React, { Component } from 'react'
import './techstack.css'
import Button from 'material-ui/Button'
import ThechStackCard from '../techStack/techStackCard'
import CreateTechStack from '../techStack/createTechStack'
import SelectTechStack from '../techStack/selectTechStack'


class ThechstackInfo extends Component {
  constructor(props){
  	super(props);
  	this.state = {
      openCreateStack: false,
      openChooseStack: false
    };
  }

  handleCloseCreateStackDialog() {
    this.setState({
      openCreateStack: false
    })
  }

  handleCloseChooseStackDialog() {
    this.setState({
      openChooseStack: false
    })
  }

  handleAddStack(stack) {
    let { logo, name, url } = stack;
    this.props.handleAddTechStack(logo, name, url);
  }

  render() {
    return (
      <div id="techstack-view">
        <h3 className="card-header">Tech Stack</h3>
        <p className="additional-text">Choose a tech stack or create your own</p>
        <div className="tech-stack-wrap">
          {
            this.props.techStackList.map((stack, i) =>
              <ThechStackCard
                key={i}
                index={i}
                logo={stack.logo}
                name={stack.name}
                url={stack.url}
                handleRemoveTechStack={this.props.handleRemoveTechStack}
              />
            )
          }
          <br/>
          <Button raised className="btn add-stack-btn" onClick={() => this.setState({ openCreateStack: true })}>
            Create Stack
          </Button>
          <br/><br/>
          <Button raised className="btn add-stack-btn" onClick={() => this.setState({ openChooseStack: true })}>
            Choose Stack
          </Button>
          <br/><br/>
        </div>
        <CreateTechStack open={this.state.openCreateStack} handleClose={this.handleCloseCreateStackDialog.bind(this)}/>
        <SelectTechStack
          open={this.state.openChooseStack}
          handleClose={this.handleCloseChooseStackDialog.bind(this)}
          options={this.props.techStackList}
          handleAddStack={this.handleAddStack.bind(this)}
        />
      </div>
    )
  }
}

export default ThechstackInfo
