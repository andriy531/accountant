import React, { Component } from 'react'
import './billing.css'
import TextField from 'material-ui/TextField'
import Grid from 'material-ui/Grid'
import MenuItem from 'material-ui/Menu/MenuItem'
import NumberFormat from 'react-number-format'
import Input from 'material-ui/Input'
import InputLabel from 'material-ui/Input/InputLabel'
import FormControl from 'material-ui/Form/FormControl'
import _ from 'lodash'
import Hidden from 'material-ui/Hidden'

const NumberPriceFormat = (props) => {
    return (
        <NumberFormat
        {...props}
        onChange={(event, values) => {
            props.onChange({
            target: {
                value: values.value,
            },
            });
        }}
        thousandSeparator
        prefix="$"
        />
    );
}

export default class BillingTab extends Component {
  constructor(props) {
      super(props);
      this.state = {
        billingType: this.props.billings && this.props.billings[0].billingType,
        billingDescription: this.props.billings && this.props.billings[0].description,
        totalValue: null,
        quantity: null,
        price: null
      };
  }

  componentWillReceiveProps(nextProps) {
      if(!_.isEqual(this.props.billings, nextProps.billings) && nextProps.billings.length) {
        this.setState({
            billingType: nextProps.billings[0].billingType,
            billingDescription: nextProps.billings[0].description,
        })
        const { totalValue, price, quantity } = this.state;
        this.props.handleBillingInfoChange({
            billingType: nextProps.billings[0].billingType,
            totalValue: totalValue || 0,
            quantity: quantity || 1,
            price: price || 0
        })
      }
  }

  componentDidMount() {
      if(this.props.billingInfo && this.props.billingInfo.billingType) {
        const billing =  _.find(this.props.billings, (o) => o.billingType === this.props.billingInfo.billingType );
        this.setState({
          billingType: this.props.billingInfo.billingType,
          billingDescription: billing.description,
          totalValue: this.props.billingInfo.totalValue,
          quantity: this.props.billingInfo.quantity,
          price: this.props.billingInfo.price
        });
      }
  }

  handleChangeBillingType(e) {
    const billingType = e.target.value;
    const billing =  _.find(this.props.billings, (o) => o.billingType === billingType )
    this.setState({
        billingType: billingType,
        billingDescription: billing.description
    });

    const { totalValue, price, quantity } = this.state;

    this.props.handleBillingInfoChange({
        billingType: billingType,
        totalValue: totalValue || 0,
        quantity: quantity || 1,
        price: price || 0
    })
  };

  handleChangePrice(e) {
      const price = e.target.value;
      let totalValue;
      if(this.state.quantity && price) {
          totalValue = this.state.quantity * price;
          this.setState({totalValue, price})
      } else {
        totalValue = +price;
        this.setState({ price, totalValue: price })
      }

      this.props.handleBillingInfoChange({
        billingType: this.state.billingType,
        totalValue: totalValue || 0,
        quantity: this.state.quantity || 1,
        price: price || 0
      })
  }

  handleChangeQuantity(e) {
    const quantity = +e.target.value;
    let totalValue;
    if(quantity && this.state.price) {
        totalValue = quantity * this.state.price;
        this.setState({totalValue, quantity})
    } else {
      totalValue = +this.state.price;
      this.setState({ quantity, totalValue })
    }
    this.props.handleBillingInfoChange({
        billingType: this.state.billingType,
        totalValue: totalValue || 0,
        quantity: quantity || 1,
        price: this.state.price || 0
      })
  }


  render() {
    const { billings, billingsPending, billingsError } = this.props;
    return (
      <div id="billing-tab">
        <Hidden smDown>
          <p className="billing-header">Billing Information</p>
        </Hidden>
        <br/>
        <Grid container spacing={0} className="billing-content">
            <Grid item xs={12} md={5} className="left-side">
                {billings && <TextField
                    select
                    label="Billing Type"
                    value={this.state.billingType}
                    onChange={(e) => this.handleChangeBillingType(e)}
                    fullWidth
                    disabled={this.props.servicePending}
                    >
                    {billings.map(option => (
                        <MenuItem key={option.bid} value={option.billingType}>
                            {option.billingType}
                        </MenuItem>
                    ))}
                </TextField>}
                {!billings && billingsPending && <p className="tip">Please wait - billing types are loading...</p>}
                {!billings && billingsError && <p className="tip">{billingsError}</p>}
                <div className="two-inputs-line">
                    <FormControl margin="normal">
                        <InputLabel htmlFor="price">
                            Price
                        </InputLabel>
                        <Input
                            id="price"
                            value={this.state.price || ''}
                            onChange={(e) => this.handleChangePrice(e)}
                            inputComponent={NumberPriceFormat}
                            placeholder="Enter price"
                            disabled={this.props.servicePending}
                        />
                    </FormControl>
                    <TextField
                        id="quantity"
                        label="Quantity"
                        value={this.state.quantity || ''}
                        onChange={(e) => this.handleChangeQuantity(e)}
                        placeholder="Enter quantity"
                        type="number"
                        margin="normal"
                        className="quantity-input"
                        disabled={this.props.servicePending}
                    />
                </div>
                {/* <p className="total-value">Total Value: <span className="total-price">$ {this.state.totalValue}</span></p> */}
            </Grid>
            <Grid item xs={12} md={7} className="right-side">
                <div className="description-view">
                    <p className="description-label">Description of billing type</p>
                    <p className="description-text">
                        {this.state.billingDescription}
                    </p>
                </div>
            </Grid>
            <p className="total-value">Total Value: <span className="total-price">$ {this.state.totalValue}</span></p>
        </Grid>
      </div>
    )
  }
}
