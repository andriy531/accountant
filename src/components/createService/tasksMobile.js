import React, { Component } from 'react'
import './tasks.css'
import TextField from 'material-ui/TextField'
import Check from 'material-ui-icons/Check'
import Button from 'material-ui/Button'
import Dialog, {
  DialogActions,
  DialogContent,
} from 'material-ui/Dialog'
import Slide from 'material-ui/transitions/Slide'
import FilterListIcon from 'material-ui-icons/FilterList'
import Menu, { MenuItem } from 'material-ui/Menu'
import AddIcon from 'material-ui-icons/Add'
//import humanizeDuration from 'humanize-duration'


export default class TasksMobile extends Component {
  constructor(props){
    super(props);
    this.state = {

    };
  }

  handleAddTask() {
    this.tasksView.scrollTop = 0;
    this.props.handleAddTask();
  }

  render() {
    const { data, taskToEdit, editingTaskName, /*editingTaskDate,*/ editingTaskRepeat } = this.props;
    return (
          <div id="mobile-tasks-view" ref={(e) => this.tasksView = e}>
            <div className="filter-view">
              <p className="filtered-by">Filtered by {this.props.tabValue === 0 ? 'client' : 'team'} task</p>
              <p
                className="filter-switch"
                onClick={() => this.props.handleToggleTaskFilter()}
                ref={(e) => this.TaskFilterAnchorEl = e}
                >Filter tasks <FilterListIcon className="filter-icon"/>
              </p>
              <Menu
                id="task-filter"
                open={this.props.openTaskFilter}
                anchorEl={this.TaskFilterAnchorEl}
                onRequestClose={() => this.props.handleToggleTaskFilter()}
              >
                <p className="menu-header">Show</p>
                <MenuItem className="menu-item" onClick={() => this.props.handleFilterChange(0)}><Check className="check-icon" style={{color: (this.props.tabValue === 0 ? '#5bc2a8' : 'transparent')}}/> Client Tasks</MenuItem>
                <MenuItem className="menu-item" onClick={() => this.props.handleFilterChange(1)}><Check className="check-icon" style={{color: (this.props.tabValue === 1 ? '#5bc2a8' : 'transparent')}}/> Team Tasks</MenuItem>
              </Menu>
            </div>
            {data.map((n, i) => {
              return (
                <div className="task-card" key={i}>
                  <div className="info-view">
                    <div className="task-info">
                      <p>{n.name}</p>
                      <span className="task-date">repeats {n.repeat}</span>
                    </div>
                  </div>
                  <div className="actions">
                    <Button
                      onClick={() => this.props.handleOpenAddDialog(n)}
                    >
                        edit
                    </Button>
                    <Button
                      onClick={() => this.props.handleOpenDeleteDialog(n)}
                    >
                        delete
                    </Button>
                  </div>
                </div>
            )})}
            <Button
              fab
              color="primary"
              aria-label="add"
              className="add-task-fab"
              onClick={() => this.props.handleOpenAddDialog()}
              >
              <AddIcon />
            </Button>
        <Dialog
          open={this.props.openAddDialog}
          transition={Slide}
          onRequestClose={() => this.props.handleCloseAddDialog()}
          className="add-task-dialog"
          >
          <p className="add-task-header">{taskToEdit ? 'Edit this task' : 'Add new task'}</p>
          <DialogContent>
            {taskToEdit ?
              <TextField
                label="Task name"
                placeholder="Enter your task name"
                className="task-name"
                onChange={(e) => this.props.handleEditingTaskNameChange(e)}
                value={editingTaskName !== undefined ? editingTaskName: taskToEdit.name}
                fullWidth
              />
              :
              <TextField
                label="Task name"
                placeholder="Enter your task name"
                className="task-name"
                onChange={(e) => this.props.handleNewTaskNameChange(e)}
                value={this.props.newTaskName ? this.props.newTaskName: ''}
                fullWidth
              />
            }
            {/* {taskToEdit ?
              <TextField
                placeholder="Due date"
                onChange={(e) => this.props.handleEditingTaskDateChange(e)}
                value={editingTaskDate !== undefined ? editingTaskDate: humanizeDuration(taskToEdit.dueDate)}
                onBlur={(e) => this.props.handleTaskDateBlur(e)}
                className="due-date"
              /> :
              <TextField
                label="Due date"
                placeholder="1w 2d 3h"
                onChange={(e) => this.props.handleNewTaskDateChange(e)}
                value={this.props.newTaskDate ? this.props.newTaskDate: ''}
                onBlur={(e) => this.props.handleTaskDateBlur(e)}
                className="due-date"
              /> } */}
              {taskToEdit ?
                <TextField
                    select
                    value={editingTaskRepeat !== undefined ? editingTaskRepeat : taskToEdit.repeat}
                    onChange={(e) => this.props.handleEditingNewTaskRepeat(e)}
                    className="repeat-select"
                    >
                    {this.props.repeats.map(option => (
                        <MenuItem key={option} value={option}>
                            {option}
                        </MenuItem>
                    ))}
                </TextField>:
                <TextField
                    select
                    label="Repeat"
                    value={this.props.repeat}
                    onChange={(e) => this.props.handleNewTaskRepeat(e)}
                    className="repeat-select"
                    >
                    {this.props.repeats.map(option => (
                        <MenuItem key={option} value={option}>
                            {option}
                        </MenuItem>
                    ))}
                </TextField>
              }
          </DialogContent>
          <DialogActions className="dialog-actions">
            {taskToEdit ?
              <Button
                className="add-btn"
                onClick={() => this.props.handleEditTask() }
                >
                Save
              </Button> :
              <Button
              className="add-btn"
              onClick={() => this.handleAddTask()}
              disabled={!this.props.newTaskName || this.props.servicePending}
              >
              Add
            </Button>}
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}
