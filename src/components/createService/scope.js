import React, { Component } from 'react'
import './scope.css'
import TextField from 'material-ui/TextField'
import Hidden from 'material-ui/Hidden'


export default class ScopeTab extends Component {
  constructor(props) {
  	super(props);
  	this.state = {
      serviceNameError: null
    };
  }

  onServiceNameBlur() {
    if(!this.props.serviceName) {
      this.setState({serviceNameError: 'Service Name is Required!'})
    }
  }

  handleServiceNameChange(e) {
    this.props.handleServiceNameChange(e);
    this.setState({serviceNameError: null})
  }

  render() {
    const { serviceNameError } = this.state;
    return (
      <div id="scope-tab">
        <Hidden smDown><p className="scope-header">Scope of Service</p></Hidden>
        <br/>
        <TextField
          fullWidth
          label={<span>Service name <span className="require-asterisk">*</span></span>}
          placeholder="Enter your service name"
          className="service-name-input"
          defaultValue={this.props.serviceName}
          onChange={(e) => this.handleServiceNameChange(e)}
          onBlur={() => this.onServiceNameBlur()}
          helperText={serviceNameError && serviceNameError}
          FormHelperTextProps={{error: true}}
          disabled={this.props.servicePending}
        />
        <TextField
          fullWidth
          id="description"
          label="Description"
          multiline
          rows="3"
          rowsMax="7"
          placeholder="Enter your scope description"
          onChange={(e) => this.props.handleScopeChange(e)}
          value={this.props.scope || ''}
          disabled={this.props.servicePending}
        />
      </div>
    )
  }
}
