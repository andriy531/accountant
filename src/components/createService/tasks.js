import React, { Component } from 'react'
import './tasks.css'
import Divider from 'material-ui/Divider'
import Tabs, { Tab } from 'material-ui/Tabs'
import TextField from 'material-ui/TextField'
import Input from 'material-ui/Input'
import Table, {
  TableBody,
  TableCell,
  TableRow,
  TableHead,
  TableSortLabel,
} from 'material-ui/Table'
import DeleteIcon from 'material-ui-icons/Delete'
import EditIcon from 'material-ui-icons/Edit'
import IconButton from 'material-ui/IconButton'
import SaveIcon from 'material-ui-icons/Save'
import Close from 'material-ui-icons/Close'
import _ from 'lodash'
import uuidv1 from 'uuid/v1'
import Check from 'material-ui-icons/Check'
import Button from 'material-ui/Button'
import Dialog, {
  DialogActions,
  DialogContent,
  DialogTitle,
} from 'material-ui/Dialog'
import Slide from 'material-ui/transitions/Slide'
import Paper from 'material-ui/Paper'
import Hidden from 'material-ui/Hidden'
import { MenuItem } from 'material-ui/Menu'
import timestring from '../../services/timestring'
import humanizeDuration from 'humanize-duration'
import TasksMobile from './tasksMobile'

const repeats = ['daily', 'weekly', 'monthly', 'quarterly', 'annually', 'no-repeats'];

export default class TasksTab extends Component {
  constructor(props){
    super(props);
    this.state = {
      tabValue: 0,
      data: [],
      clientTasksData: this.props.clientTasksData || [],
      teamTasksData: this.props.teamTasksData || [],
      order: 'asc',
      newTaskName: null,
      //newTaskDate: null,
      editingTaskName: undefined,
      editingTaskDate: undefined,
      editingTaskRepeat: undefined,
      openDeleteDialog: false,
      openTaskFilter: false,
      openAddDialog: false,
      repeat: repeats[0]
    };
  }

  componentDidMount() {
    const { clientTasksData, teamTasksData, tabValue } = this.state;
    this.setState({ data: tabValue === 0 ? clientTasksData : teamTasksData });
  }

  handleTabChange = (event, tabValue) => {
    const { clientTasksData, teamTasksData } = this.state;
    this.setState({ tabValue, data: tabValue === 0 ? clientTasksData : teamTasksData });
  };

  handleFilterChange = (tabValue) => {
    const { clientTasksData, teamTasksData } = this.state;
    this.setState({ tabValue, data: tabValue === 0 ? clientTasksData : teamTasksData });
    this.handleToggleTaskFilter();
  };

  editRow(row) {
    this.setState({taskToEdit: row})
  }

  cancelEditRow() {
    this.setState({taskToEdit: null,  editingTaskName: undefined, editingTaskDate: undefined, editingTaskRepeat: undefined})
  }

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    const data = this.state.data.sort(
      (a, b) => (order === 'desc' ? b[orderBy] > a[orderBy] : a[orderBy] > b[orderBy]),
    );

    this.setState({ data, order, orderBy });
  };

  handleAddTask() {
    const { newTaskName, /*newTaskDate,*/ repeat, data, tabValue } = this.state;
    const whichTasks = tabValue === 0 ? 'clientTasksData' : 'teamTasksData';
    const newData = [{uuid: uuidv1(), name: newTaskName, /*dueDate: timestring(newTaskDate, 'ms'),*/ repeat}, ...data];
    this.setState({
      data: newData,
      [whichTasks]: newData,
      newTaskName: null,
      //newTaskDate: null,
      taskToDelete: null,
      repeat: repeats[0]
    });
    this.props.handleTasksChange({[whichTasks]: newData})
    this.tasksView && (this.tasksView.scrollTop = 0);
    this.handleCloseAddDialog();
  }

  handleDeleteTask() {
    const { taskToDelete, data, tabValue } = this.state;
    _.remove(data, (t) => t.uuid === taskToDelete.uuid);
    const whichTasks = tabValue === 0 ? 'clientTasksData' : 'teamTasksData';
    this.setState({data, [whichTasks]: data});
    this.props.handleTasksChange({[whichTasks]: data})
    this.handleCloseDeleteDialog();
  }

  handleEditTask() {
    const { taskToEdit, data, editingTaskName, editingTaskDate, editingTaskRepeat, tabValue } = this.state;
    const whichTasks = tabValue === 0 ? 'clientTasksData' : 'teamTasksData';
    let index = _.findIndex(data, {uuid: taskToEdit.uuid});
    let updatedTask = {...taskToEdit };
    if(editingTaskName) { updatedTask.name = editingTaskName; }
    if(editingTaskDate) { updatedTask.dueDate = timestring(editingTaskDate, 'ms'); }
    if(editingTaskRepeat) { updatedTask.repeat = editingTaskRepeat; }
    let newData = [...data];
    newData.splice(index, 1, updatedTask);
    this.setState({data: newData, [whichTasks]: newData});
    this.props.handleTasksChange({[whichTasks]: newData})
    this.handleCloseAddDialog();
  }

  handleNewTaskNameChange(e) {
    const taskName = e.target.value;
    this.setState({newTaskName: taskName})
  }

  handleNewTaskDateChange(e) {
    const taskDate = e.target.value;
    this.setState({newTaskDate: taskDate})
  }

  handleTaskDateBlur(e) {
    const taskDate = e.target.value;
    const notificationOpts = {
      title: 'Invalid due date!',
      message: 'Due date pattern (1year 2month 3weeks 4days 5mins)',
      position: 'tr',
      autoDismiss: 5
    };
    try {
      const time = timestring(taskDate, 'ms');
      if(taskDate && !time) {
        this.props.notificationError(notificationOpts);
        this.setState({newTaskDate: null, editingTaskDate: undefined});
      }
    } catch (e) {
      this.setState({newTaskDate: null, editingTaskDate: undefined});
      this.props.notificationError(notificationOpts);
    }
  }

  handleNewTaskRepeat(e) {
    const repeat = e.target.value;
    this.setState({repeat: repeat})
  }

  handleEditingTaskNameChange(e) {
    const taskName = e.target.value;
    this.setState({editingTaskName: taskName})
  }

  handleEditingTaskDateChange(e) {
    const taskDate = e.target.value;
    this.setState({editingTaskDate: taskDate})
  }

  handleEditingNewTaskRepeat(e) {
    const taskRepeat = e.target.value;
    this.setState({editingTaskRepeat: taskRepeat})
  }

  handleOpenDeleteDialog(task) {
    this.setState({ openDeleteDialog: true, taskToDelete: task });
  }

  handleOpenAddDialog(task) {
    this.setState({ openAddDialog: true, taskToEdit: task });
  }

  handleCloseAddDialog() {
    this.setState({ openAddDialog: false });
    this.cancelEditRow();
  }

  handleCloseDeleteDialog() {
    this.setState({ openDeleteDialog: false, taskToDelete: null });
  }

  handleToggleTaskFilter() {
    this.setState({ openTaskFilter: !this.state.openTaskFilter });
  };

  render() {
    const { data, order, orderBy, taskToEdit, newTaskName, /*newTaskDate,*/ editingTaskName, editingTaskDate, editingTaskRepeat } = this.state;
    return (
      <div id="tasks-tab">
        <Hidden smDown>
          <div>
            <Tabs value={this.state.tabValue} onChange={this.handleTabChange.bind(this)} className="horizontal-tabs">
              <Tab label="Client Tasks" className="tab"/>
              <Tab label="Team Tasks" className="tab"/>
            </Tabs>
            <Divider/>
            <div className="table-wrapper" ref={(e) => this.tasksView = e}>
              <Table id="custom-table">
                <TableHead id="table-head">
                  <TableRow>
                    <TableCell className="checkbox-cell" padding="checkbox"/>
                    <TableCell padding="none">
                        <TableSortLabel
                          active={orderBy === 'name'}
                          direction={order}
                          onClick={(e) => this.handleRequestSort(e, 'name')}
                        >
                          <span>{this.state.tabValue === 0 ? 'Client' : 'Team'} tasks ({data.length})</span>
                        </TableSortLabel>
                    </TableCell>
                    {/* <TableCell>
                        <TableSortLabel
                          active={orderBy === 'dueDate'}
                          direction={order}
                          onClick={(e) => this.handleRequestSort(e, 'dueDate')}
                        >
                          <span>Due date</span>
                        </TableSortLabel>
                    </TableCell> */}
                    <TableCell padding="none">
                        <TableSortLabel
                          active={orderBy === 'repeat'}
                          direction={order}
                          onClick={(e) => this.handleRequestSort(e, 'repeat')}
                        >
                          <span>Repeat</span>
                        </TableSortLabel>
                    </TableCell>
                    <TableCell/>
                  </TableRow>
                </TableHead>
                <TableBody id="table-body">
                  {data.map((n, i) => {
                    return (
                      <TableRow
                        hover
                        tabIndex={-1}
                        key={i}
                      >
                        <TableCell padding="checkbox">
                          <Check className="check-icon"/>
                        </TableCell>
                        {taskToEdit && taskToEdit.uuid === n.uuid ? <TableCell padding="none">
                          <Input
                            placeholder="Enter task name"
                            onChange={(e) => this.handleEditingTaskNameChange(e)}
                            value={editingTaskName !== undefined ? editingTaskName: n.name}
                          />
                          </TableCell> : <TableCell padding="none">{n.name}</TableCell>}
                        {/* {taskToEdit && taskToEdit.uuid === n.uuid ? <TableCell>
                          <TextField
                            placeholder="Due date"
                            onChange={(e) => this.handleEditingTaskDateChange(e)}
                            value={editingTaskDate !== undefined ? editingTaskDate: humanizeDuration(n.dueDate)}
                            onBlur={(e) => this.handleTaskDateBlur(e)}
                          />
                        </TableCell> : <TableCell>{humanizeDuration(n.dueDate)}</TableCell>} */}
                          {taskToEdit && taskToEdit.uuid === n.uuid ? <TableCell>
                            <TextField
                                select
                                value={editingTaskRepeat !== undefined ? editingTaskRepeat : n.repeat}
                                onChange={(e) => this.handleEditingNewTaskRepeat(e)}
                                className="repeat-select"
                                >
                                {repeats.map(option => (
                                    <MenuItem key={option} value={option}>
                                        {option}
                                    </MenuItem>
                                ))}
                            </TextField>
                            </TableCell> : <TableCell>{n.repeat}</TableCell>}
                        <TableCell className="action-cell">
                          {taskToEdit && taskToEdit.uuid === n.uuid ?
                            <IconButton onClick={() => this.cancelEditRow()} title="Cancel changes" className="icon-btn">
                              <Close className="green-icon"/>
                            </IconButton> :
                            <IconButton onClick={() => this.editRow(n)} title="Edit row" className="icon-btn">
                              <EditIcon />
                            </IconButton>
                          }
                          {taskToEdit && taskToEdit.uuid === n.uuid?
                            <IconButton onClick={() => this.handleEditTask()} title="Save changes" className="icon-btn">
                              <SaveIcon className="green-icon"/>
                            </IconButton> :
                            <IconButton onClick={() => this.handleOpenDeleteDialog(n)} title="Delete row" className="icon-btn">
                              <DeleteIcon />
                            </IconButton>
                          }
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
              {!data.length && <p className="empty-tasks-tip">You don't have any tasks yet</p>}
            </div>
            <div className="new-task-form">
              <TextField
                label="Task name"
                placeholder="Enter your task name"
                className="task-name"
                onChange={(e) => this.handleNewTaskNameChange(e)}
                value={this.state.newTaskName ? this.state.newTaskName: ''}
              />
              {/* <TextField
                label="Due date"
                placeholder="1w 2d 3h"
                onChange={(e) => this.handleNewTaskDateChange(e)}
                value={this.state.newTaskDate ? this.state.newTaskDate: ''}
                onBlur={(e) => this.handleTaskDateBlur(e)}
              /> */}
              <TextField
                  select
                  label="Repeat"
                  value={this.state.repeat}
                  onChange={(e) => this.handleNewTaskRepeat(e)}
                  className="repeat-select"
                  >
                  {repeats.map(option => (
                      <MenuItem key={option} value={option}>
                          {option}
                      </MenuItem>
                  ))}
              </TextField>
              <Button
                raised
                onClick={() => this.handleAddTask()}
                disabled={!this.state.newTaskName || this.props.servicePending}
              >
                  Add new task
              </Button>
            </div>
          </div>
        </Hidden>
        <Hidden mdUp>
          <TasksMobile
            tabValue={this.state.tabValue}
            data={data}
            taskToEdit={taskToEdit}
            newTaskName={newTaskName}
            //newTaskDate={newTaskDate}
            editingTaskName={editingTaskName}
            editingTaskDate={editingTaskDate}
            editingTaskRepeat={editingTaskRepeat}
            handleToggleTaskFilter={this.handleToggleTaskFilter.bind(this)}
            handleFilterChange={this.handleFilterChange.bind(this)}
            openTaskFilter={this.state.openTaskFilter}
            handleOpenAddDialog={this.handleOpenAddDialog.bind(this)}
            handleCloseAddDialog={this.handleCloseAddDialog.bind(this)}
            openAddDialog={this.state.openAddDialog}
            handleNewTaskNameChange={this.handleNewTaskNameChange.bind(this)}
            handleEditingTaskNameChange={this.handleEditingTaskNameChange.bind(this)}
            handleNewTaskDateChange={this.handleNewTaskDateChange.bind(this)}
            handleEditingTaskDateChange={this.handleEditingTaskDateChange.bind(this)}
            servicePending={this.props.servicePending}
            handleAddTask={this.handleAddTask.bind(this)}
            handleTaskDateBlur={this.handleTaskDateBlur.bind(this)}
            repeats={repeats}
            repeat={this.state.repeat}
            handleNewTaskRepeat={this.handleNewTaskRepeat.bind(this)}
            handleEditingNewTaskRepeat={this.handleEditingNewTaskRepeat.bind(this)}
            handleOpenDeleteDialog={this.handleOpenDeleteDialog.bind(this)}
            handleEditTask={this.handleEditTask.bind(this)}
          />
        </Hidden>
        <Dialog
          open={this.state.openDeleteDialog}
          transition={Slide}
          onRequestClose={() => this.handleCloseDeleteDialog()}
          className="delete-task-dialog"
          >
          <Hidden smDown>
            <DialogTitle>{"Are you sure you want to delete this task?"}</DialogTitle>
          </Hidden>
          <Hidden mdUp>
            <p className="delete-mobile-header">Delete this task?</p>
          </Hidden>
          <DialogContent>
            <br/>
            <Paper>
              <Hidden smDown>
                <Table id="custom-table">
                  <TableHead id="table-head">
                    <TableRow>
                      <TableCell>
                        <span>Task</span>
                      </TableCell>
                      <TableCell>
                        <span>Due date</span>
                      </TableCell>
                      <TableCell>
                        <span>Repeat</span>
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody id="table-body">
                    <TableRow>
                      <TableCell>
                        <span>{this.state.taskToDelete && this.state.taskToDelete.name}</span>
                      </TableCell>
                      <TableCell>
                        <span>{this.state.taskToDelete && humanizeDuration(this.state.taskToDelete.dueDate)}</span>
                      </TableCell>
                      <TableCell>
                        <span>{this.state.taskToDelete && this.state.taskToDelete.repeat}</span>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </Hidden>
              <Hidden mdUp>
                <div className="info-view">
                  <div className="task-info">
                    <p>{this.state.taskToDelete && this.state.taskToDelete.name}</p>
                    <span className="task-date">{this.state.taskToDelete && (`${humanizeDuration(this.state.taskToDelete.dueDate)}, repeats ${this.state.taskToDelete.repeat}`)}</span>
                  </div>
                </div>
              </Hidden>
            </Paper>
          </DialogContent>
          <DialogActions>
            <div>
              <Hidden smDown>
                <Button onClick={() => this.handleCloseDeleteDialog()}>
                  Cancel
                </Button>
              </Hidden>
            </div>
            <Button onClick={() => this.handleDeleteTask()} className="delete-btn">
              Delete
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}
