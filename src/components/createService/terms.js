import React, { Component } from 'react'
import './terms.css'
import TextField from 'material-ui/TextField'
import Hidden from 'material-ui/Hidden'


export default class TermsTab extends Component {
  constructor(props) {
  	super(props);
  	this.state = {

    };
  }

  render() {
    return (
      <div id="terms-tab">
        <Hidden smDown>
          <p className="terms-header">Terms and conditions</p>
        </Hidden>
        <br/>
        <TextField
          fullWidth
          id="terms"
          label="Terms and conditions"
          multiline
          rows="3"
          rowsMax="7"
          placeholder="Enter your terms and conditions"
          value={this.props.terms || ''}
          onChange={(e) => this.props.handleTermsChange(e)}
          disabled={this.props.servicePending}
        />
      </div>
    )
  }
}
