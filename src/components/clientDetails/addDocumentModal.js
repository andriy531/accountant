import React, { Component } from 'react'
import './addDocumentModal.css'
import FileUpload from 'material-ui-icons/FileUpload'
import Button from 'material-ui/Button'
import Dialog, { DialogContent } from 'material-ui/Dialog'
import Slide from 'material-ui/transitions/Slide'
import TextField from 'material-ui/TextField'
import _ from 'lodash'
import Close from 'material-ui-icons/Close'
import Check from 'material-ui-icons/Check'
import Edit from 'material-ui-icons/Edit'
import { AsyncCreatable } from 'react-select'
import Dropzone from 'react-dropzone'
import { getDocumentUploadLink, uploadDocumentApi, getDocumentTypeAutosuggestApi, deleteDocumentApi } from '../../services/api'
import { LinearProgress } from 'material-ui/Progress'
import FaFileO from 'react-icons/lib/fa/file-o'
import axios from 'axios'
import FormHelperText from 'material-ui/Form/FormHelperText'

const CancelToken = axios.CancelToken;
const fileNameReqExp = /^[a-z0-9_.@() -]+$/i;

export default class AddDocument extends Component {

  constructor(props){
  	super(props);
  	this.state = {
      tags: [],
      tempTags: [],
      completed: 0,
      file: null,
      documentUploadLink: {},
      uploadingStatus: '',
      uploadingFailed: false,
      fileName: '',
      fileNameError: null,
      fileUploading: false,
      type: '',
      summary: ''
    };
  }

  onTagChange (value) {
		this.setState({
			tempTags: value,
		});
	}

  getTags (input) {
    return getDocumentTypeAutosuggestApi(input, 'tags').then((res) => {
      return { options: _.map(res.data, (o, i) => { return {value: o}})}
    })
	}

  handleAddTags() {
    this.setState({tags: this.state.tempTags, showTagInput: false});
  }

  cancelAddTags() {
    this.setState({tempTags: this.state.tags, showTagInput: false})
  }

  onDropFile(acceptedFiles, rejectedFiles) {
    if (rejectedFiles.length) {
      const fileSize = Math.round(rejectedFiles[0].size / 1000000 * 10 ) / 10;
      const maxSize = 200; // 200 mb
      if (fileSize > maxSize) {
        alert('You can`t attach file more than 200 Mb. File size is ' + Math.round(rejectedFiles[0].size / 1000000 * 10) / 10 + ' Mb');
      }
      this.setState({completed: 0, file: null})
    } else {
      const { name } = acceptedFiles[0];
      this.setState({completed: 0, file: acceptedFiles[0], uploadingStatus: '', uploadingFailed: false, fileName: acceptedFiles[0].name, fileNameError: null, fileUploading: true})
      getDocumentUploadLink(name).then((res) => {

        this.setState({documentUploadLink: res.data})
        let source = CancelToken.source()
        this.UploadingSource = source;
        uploadDocumentApi(res.data.signedUrl, acceptedFiles[0], source.token, (e) => this.onUploadProgress(e))
        .then((res) => {
          if(res.status === 200) {
            this.setState({uploadingStatus: 'Success', uploadingFailed: false, fileUploading: false});
          }
        })
        .catch((err) => {
          this.setState({uploadingStatus: err.data.error, uploadingFailed: true, fileUploading: false});
        });
      })
    }
  }

  onUploadProgress(e) {
    const { completed } = this.state;
    if (completed > 100) {
      this.setState({ completed: 0 });
    } else {
      const completed = Math.round((e.loaded/e.total) * 1000) / 10;
      this.setState({ completed, uploadingStatus: '', uploadingFailed: false});
    }
  };

  loadedMbString() {
    const { completed, file } = this.state;
    if(file) {
      let totalSize = Math.round(file.size / 1000000 * 10 ) / 10;
      let loadedSize = Math.round((completed * totalSize/100) * 10) / 10;
      return `${loadedSize}/${totalSize} mb`;
    }
  }

  cancelUploading() {
    const { fileUploading, documentUploadLink } = this.state;
    if(fileUploading) {
      this.UploadingSource.cancel('Operation canceled by the user.');
    } else {
      deleteDocumentApi({location: documentUploadLink.url})
    }
    this.setState({file: null, documentUploadLink: {}, fileName: '', summary: '', tags: [], tempTags: [], type: ''});
  }

  onFileNameChange(e) {
    const fileName = e.target.value;
    if(fileNameReqExp.test(fileName)) {
      this.setState({fileName, fileNameError: null})
    } else {
      this.setState({fileName, fileNameError: 'Invalid file name...'})
    }
  }
  validateFileName() {
    if(!this.state.fileName) {
      this.setState({fileNameError: `File name is Required!`})
    }
  }

  getTypes (input) {
    return getDocumentTypeAutosuggestApi(input, 'type').then((res) => {
      return { options: _.map(res.data, (o, i) => { return {value: o}})}
    })
	}

  onChangeType(value) {
    this.setState({type: value, typeError: null})
  }

  validateFileType() {
    if(!this.state.type) {
      this.setState({typeError: `Type is Required!`})
    }
  }

  closeAddDocModal() {
    const { file } = this.state;
    if(file) {
      this.cancelUploading();
    }
    this.props.closeAddDocModal();
  }

  onSummaryChange(e) {
    this.setState({summary: e.target.value})
  }

  saveDocument() {
    const { fileName, summary, type, tags, documentUploadLink, file } = this.state;
    const { id } = this.props.match.params;
    const body = {
      fileName,
      summary,
      type: type ? type.value : '',
      location: documentUploadLink.url,
      tags: _.map(tags, (o) => o.value),
      size: file.size
    }

    if(this.checkRequiredFields()) {
      this.props.postClientDocumentRequest({client_id: id, body});
      this.props.closeAddDocModal();
    }
  }

  checkRequiredFields() {
    const { fileName, type } = this.state;
    if(!fileName) {
      this.setState({fileNameError: 'File name is Required!'});
      return false;
    } else if(!type) {
      this.setState({typeError: 'Type is Required!'});
      return false;
    } else {
      return true;
    }
  }

  render() {
    const { file, uploadingStatus, uploadingFailed, fileUploading } = this.state;
    return (
      <Dialog
        open={this.props.showModal}
        transition={Slide}
        >
        <DialogContent id="add-document">
          <div className="content">
            <p className="header">Add Document</p>
            <TextField
              fullWidth
              id="doc-name"
              label="Document name *"
              placeholder="Enter name..."
              value={this.state.fileName}
              onChange={(e) => this.onFileNameChange(e)}
              onBlur={() => this.validateFileName()}
              className="text-field"
              helperText={this.state.fileNameError}
              FormHelperTextProps={{error: true}}
            />
            <TextField
              fullWidth
              id="doc-summary"
              label="Summary"
              placeholder="Add summary..."
              value={this.state.summary}
              onChange={(e) => this.onSummaryChange(e)}
              multiline
              className="text-field"
            />
            <div className="select-wrapper">
              <p className="select-label">Type *</p>
              <AsyncCreatable
                className="select-field"
                multi={false}
                valueKey="value"
                labelKey="value"
                loadOptions={(input) => this.getTypes(input)}
                value={this.state.type}
                onChange={(value) => this.onChangeType(value)}
                clearable={false}
                arrowRenderer={() => null}
                placeholder="Choose type or create new..."
                onBlur={() => this.validateFileType()}
              />
              <FormHelperText error>{this.state.typeError}</FormHelperText>
            </div>
            {!this.state.showTagInput && !this.state.tags.length && <p className="pale add-tags-tip" onClick={() => this.setState({showTagInput: true})}>Add tags...</p>}
            { this.state.showTagInput &&
              <div className="tags-view">
                <AsyncCreatable
                  autofocus
                  className="tag-input"
                  multi={true}
                  valueKey="value"
                  labelKey="value"
                  loadOptions={(input) => this.getTags(input)}
                  value={this.state.tempTags}
                  onChange={(value) => this.onTagChange(value)}
                  clearable={false}
                  arrowRenderer={() => null}
                />
                <div className="tag-actions">
                  <Check className="check-icon" onClick={() => this.handleAddTags()}/>
                  <Close className="close-icon" onClick={() => this.cancelAddTags()}/>
                </div>
              </div>
            }
            {!this.state.showTagInput &&
              <div className="tag-list">
                {this.state.tags.map((t, i) => {
                  return <span key={i} className="tag">{t.value}</span>
                })}
                {!!this.state.tags.length && <Edit className="pencil" onClick={() => this.setState({showTagInput: true})}/>}
              </div>
            }
            {!file ? <div className="add-doc-zone">
                <p className="add-doc"  onClick={() => this.docDropzoneRef.open()}><FileUpload/> CHOOSE FILE</p>
                <Dropzone
                  ref={(node) => { this.docDropzoneRef = node; }}
                  onDrop={this.onDropFile.bind(this)}
                  className="logo-dropzone"
                  maxSize={1000000 * 200}
                  multiple={false}
                >
                </Dropzone>
            </div> :
            <div className="progress-bar-view">
              <div className="extention">
                <FaFileO/>
              </div>
              <div className="progress-wrap">
                <div className="file-name-block">
                  <p className="file-name">
                    <span className="name">{file && file.name}</span> <span className="size">{this.loadedMbString()}</span>
                  </p>
                  <Close className="cancel-icon" onClick={() => this.cancelUploading()}/>
                </div>
                <LinearProgress mode="determinate" value={this.state.completed} className="progress-bar"/>
                {uploadingStatus ? <p className={uploadingFailed ? "progress failed" : "progress success"}>{uploadingStatus}</p> : <p className="progress">{this.state.completed}% done</p>}
              </div>
            </div>
            }
          </div>
        </DialogContent>
        <div className="dialog-actions">
          <Button
            className="btn cancel-btn cancel-upload-btn"
            onClick={() => this.closeAddDocModal()}
            >
              Cancel
          </Button>
          <Button
            className="btn green-btn upload-btn"
            onClick={() => this.saveDocument()}
            disabled={fileUploading || !file}
            >
              Upload
          </Button>
        </div>
      </Dialog>
    )
  }
}
