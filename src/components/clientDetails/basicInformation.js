import React, { Component } from 'react'
import Divider from 'material-ui/Divider'
import './basicInformation.css'
import { AsyncCreatable } from 'react-select'
//import Hidden from 'material-ui/Hidden'
import Avatar from 'react-avatar'
import moment from 'moment'
import Close from 'material-ui-icons/Close'
import Check from 'material-ui-icons/Check'
import Edit from 'material-ui-icons/Edit'
import { getClientSearchAutosuggestApi } from '../../services/api'
import _ from 'lodash'


export default class ClientBasicInfo extends Component {
  constructor(props){
  	super(props);
  	this.state = {
      tags: [],
      tempTags: []
    };
  }

  onTagChange (value) {
		this.setState({
			tempTags: value,
		});
	}

  getTags (input) {
    const { basicInfo } = this.props.client;
		if (!input) {
			return Promise.resolve({ options: _.map(basicInfo.tags, (o, i) => { return {value: o}})});
		}

    return getClientSearchAutosuggestApi('tags', input).then((res) => {
      return { options: _.map(res.data, (o, i) => { return {value: o}})}
    })
	}

  componentWillReceiveProps(nextProps) {
    const { basicInfo } = nextProps.client;
    if(basicInfo) {
      let tags = _.map(basicInfo.tags, (o, i) => { return {value: o}});
      if(!nextProps.client.pending) {
        this.setState({tags: tags, tempTags: tags})
      }
    }
  }

  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.getClientBasicInfoRequest({id});
  }

  handleAddTags() {
    this.setState({tags: this.state.tempTags, showTagInput: false});
    const { basicInfo } = this.props.client;
    let tags = _.map(this.state.tempTags, (o) => o.value)
    let body = {...basicInfo, tags: tags};
    this.props.updateClientBasicInfoRequest({body, client_id: basicInfo.client_id})
  }

  cancelAddTags() {
    this.setState({tempTags: this.state.tags, showTagInput: false})
  }

  render() {
    const { basicInfo } = this.props.client;
    if(basicInfo) {
      return (
        <div id="client-basic-info">
          <Avatar
            size={150}
            round
            name={basicInfo.firstName + ' ' + basicInfo.lastName}
            className="avatar"
            color="#9da5b9"
          />
          <h2 className="client-name">{basicInfo.firstName + ' ' + basicInfo.lastName}</h2>
          <p className="pale client-comany">{basicInfo.company} Inverita Soft</p>
          <Divider/>
          <div className="table-view">
            <table className="info-table">
              <tbody>
                <tr>
                  <td>Type:</td>
                  <td>{basicInfo.type}</td>
                </tr>
                <tr>
                  <td>Fiscal year end:</td>
                  <td>{basicInfo.fiscalEndYear && moment(basicInfo.fiscalEndYear).format('DD/MM/YYYY')}</td>
                </tr>
                <tr>
                  <td>Status:</td>
                  <td>{basicInfo.status}</td>
                </tr>
                <tr>
                  <td>Registration date:</td>
                  <td>{moment(basicInfo.registrationDate).format('DD/MM/YYYY')}</td>
                </tr>
                <tr>
                  <td>Country:</td>
                  <td>{basicInfo.clientCountry}</td>
                </tr>
              </tbody>
            </table>
            <table className="info-table">
              <tbody>
                <tr>
                  <td>Engagements:</td>
                  <td>{basicInfo.engagementsNumber}</td>
                </tr>
                <tr>
                  <td>E-mail:</td>
                  <td><a href={`mailto:${basicInfo.email}`}>{basicInfo.email}</a></td>
                </tr>
                <tr>
                  <td>Phone:</td>
                  <td><a href={`tel:${basicInfo.phone}`}>{basicInfo.phone}</a></td>
                </tr>
                <tr>
                  <td>Website:</td>
                  <td><a href={basicInfo.website} target="_blank" rel="noopener noreferrer">{basicInfo.website}</a></td>
                </tr>
                <tr>
                  <td>State:</td>
                  <td>{basicInfo.clientState}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <Divider/>
          {!this.state.showTagInput && !this.state.tags.length && <p className="pale add-tags-tip" onClick={() => this.setState({showTagInput: true})}>Add tags...</p>}
          { this.state.showTagInput &&
            <div className="tags-view">
              <AsyncCreatable
                autofocus
                className="tag-input"
                multi={true}
                valueKey="value"
                labelKey="value"
                loadOptions={(input) => this.getTags(input)}
                value={this.state.tempTags}
                onChange={(value) => this.onTagChange(value)}
                clearable={false}
                arrowRenderer={() => null}
              />
              <div className="tag-actions">
                <Check className="check-icon" onClick={() => this.handleAddTags()}/>
                <Close className="close-icon" onClick={() => this.cancelAddTags()}/>
              </div>
            </div>
          }
          {!this.state.showTagInput &&
            <div className="tag-list">
              {this.state.tags.map((t, i) => {
                return <span key={i} className="tag">{t.value}</span>
              })}
              {!!this.state.tags.length && <Edit className="pencil" onClick={() => this.setState({showTagInput: true})}/>}
            </div>
          }
        </div>
      )
    } else {
      return (
        <div id="client-basic-info">
          {this.props.client.basicInfoError ? <p className="pale please-wait">{this.props.client.basicInfoError}</p> : <p className="pale please-wait">Please wait...</p>}
        </div>
      )
    }
  }
}
