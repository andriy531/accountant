import React, { Component } from 'react'
import './notesMobile.css'
import Button from 'material-ui/Button'
import IconButton from 'material-ui/IconButton'
import Hidden from 'material-ui/Hidden'
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog'
import Slide from 'material-ui/transitions/Slide'
import moment from 'moment'
import Edit from 'material-ui-icons/Edit'
import Delete from 'material-ui-icons/Delete'
import TextField from 'material-ui/TextField'
import Waypoint from 'react-waypoint'
import { FormControl } from 'material-ui/Form'
import Input, { InputLabel } from 'material-ui/Input'
import Select from 'material-ui/Select'
import { MenuItem } from 'material-ui/Menu'
import { getNotesAuthorsApi } from '../../services/api'


export default class ClientNotesMobile extends Component {
  constructor(props){
  	super(props);
  	this.state = {
      newNote: null,
      openDeleteDialog: false,
      noteToDelete: null,
      noteToEdit: null,
      page: 1,
      hasMore: true,
      filters: {
        sortByDate: '',
        sortByAuthor: ''
      },
      noteAuthors: []
    };
  }

  handleOpenDeleteDialog(note) {
    this.setState({ openDeleteDialog: true, noteToDelete: note });
  }

  handleCloseDeleteDialog() {
    this.setState({ openDeleteDialog: false, noteToDelete: null });
  }

  handleSaveNote() {
    const { id } = this.props.match.params;
    const { newNote } = this.state;
    if(newNote) {
      this.props.postClientNoteRequest({ client_id: id, note: {description: newNote.trim()}})
      this.setState({newNote: null})
      this.notesView.scrollIntoView();
    }
  }

  handleDeleteNote() {
    const { noteToDelete } = this.state;
    this.props.deleteClientNoteRequest({noteToDelete});
    this.handleCloseDeleteDialog();
  }

  handleChangeNewNote(e) {
    const note = e.target.value;
    this.setState({newNote: note})
  }

  openEditField(note) {
    this.setState({noteToEdit: note})
  }

  closeEditField(note) {
    this.setState({noteToEdit: null})
  }

  handleChangeNoteToEdit(e) {
    const noteText = e.target.value;
    const { noteToEdit } = this.state;
    this.setState({noteToEdit: {...noteToEdit, description: noteText}})
  }

  handleEditNote() {
    const { noteToEdit } = this.state;
    if(noteToEdit) {
      this.props.editClientNoteRequest({noteToEdit})
      this.setState({noteToEdit: null})
    }
  }

  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.getClientNotesRequest({id, page: 1});
    getNotesAuthorsApi(id).then((res) => {
      this.setState({noteAuthors: res.data})
    }, err => {
      this.setState({noteAuthors: []})
    });
  }

  handleLoadMore() {
    const { id } = this.props.match.params;
    const lengthOfNotes = this.props.client.notes.lengthOfNotes;
    const notes = this.props.client.notes.data;
    let page = this.state.page + 1;
    if(page <= Math.ceil(lengthOfNotes/10) && lengthOfNotes > notes.length) {
      const { filters } = this.state;
      this.props.getClientNotesRequest({id, page, filters});
      this.setState({page})
    }
  }

  onChangeFilter(data) {
    let filters = { ...this.state.filters, ...data };
    this.setState({filters, page: 1})
    const { id } = this.props.match.params;
    this.props.getClientNotesRequest({id, page: 1, filters})
  }

  render() {
    const { notes } = this.props.client;
    const { sortByDate, sortByAuthor } = this.state.filters;
    return (
      <div id="client-notes-mobile">
        <div className="notes-view" ref={(e) => this.notesView = e}>
          <div className="filters">
            <FormControl className="form-control">
              <InputLabel htmlFor="registration">Date</InputLabel>
              <Select
                value={sortByDate}
                onChange={(e) => this.onChangeFilter({sortByDate: e.target.value})}
                input={<Input id="date" />}
                className="select"
              >
                <MenuItem value=""><span className="select-item">None</span></MenuItem>
                <MenuItem value={"desc"}><span className="select-item">Recently</span></MenuItem>
                <MenuItem value={"asc"}><span className="select-item">Old</span></MenuItem>
              </Select>
            </FormControl>
            <FormControl className="form-control">
              <InputLabel htmlFor="registration">Author</InputLabel>
              <Select
                value={sortByAuthor}
                onChange={(e) => this.onChangeFilter({sortByAuthor: e.target.value})}
                input={<Input id="author" />}
                className="select"
              >
                <MenuItem value=""><span className="select-item">None</span></MenuItem>
                {this.state.noteAuthors.map((a, i) => {
                  return <MenuItem key={i} value={a}><span className="select-item">{a}</span></MenuItem>
                })}
              </Select>
            </FormControl>
          </div>
          <h3>Notes ({notes.lengthOfNotes})</h3>
          {
            notes.data.map((n, i) => {
              return <div className="note" key={i}>
                <div className="note-header">
                  <span className="time">{moment(n.dateCreated).fromNow()}<br/>{n.author}</span>
                  <div className="actions">
                    <IconButton className="action-btn" onClick={() => this.openEditField(n)}><Edit/></IconButton>
                    <IconButton className="action-btn" onClick={() => this.handleOpenDeleteDialog(n)}><Delete/></IconButton>
                  </div>
                </div>
                {
                  this.state.noteToEdit && this.state.noteToEdit.note_id === n.note_id ?
                  <div className="editable-view">
                    <TextField
                      placeholder="Type your note"
                      multiline
                      className="edit-textarea"
                      margin="normal"
                      onChange={(e) =>  this.handleChangeNoteToEdit(e)}
                      value={this.state.noteToEdit.description}
                    />
                    <div className="edit-actions">
                      <Button onClick={() => this.closeEditField()} className="cancel-btn">Cancel</Button>
                      <Button onClick={() => this.handleEditNote()} className="save-btn">Save</Button>
                    </div>
                  </div> :
                  <div><div className="note-block">
                    <p className="note-text">
                      {n.description}
                    </p>
                  </div>
                </div>
                }
              </div>
            })
          }
          {!this.props.client.pending && <Waypoint
            onEnter={() => this.handleLoadMore()}
          />}
          <br/>
        </div>
        <div className="new-note-view">
          <div className="note-input">
            <textarea
              placeholder="Type a new note..."
              onChange={(e) => this.handleChangeNewNote(e)}
              value={this.state.newNote ? this.state.newNote : ''}
              disabled={this.props.client.pending}
            />
          </div>
          <Button raised className="green-btn save-note-btn" onClick={() => this.handleSaveNote()}>
             <Hidden xsDown><span>Save note</span></Hidden>
             <Hidden smUp><span>Add note</span></Hidden>
          </Button>
        </div>
        <Dialog open={this.state.openDeleteDialog} transition={Slide} onRequestClose={() => this.handleCloseDeleteDialog()}>
          <DialogTitle>{"Are you sure you want to delete this note?"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              {this.state.noteToDelete && this.state.noteToDelete.description}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => this.handleCloseDeleteDialog()} className="cancel-btn">
              Cancel
            </Button>
            <Button onClick={() => this.handleDeleteNote()} className="delete-btn">
              Delete
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}
