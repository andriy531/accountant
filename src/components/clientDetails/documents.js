import React, { Component } from 'react'
import Divider from 'material-ui/Divider'
import './documents.css'
import Tabs, { Tab } from 'material-ui/Tabs'
import FileUpload from 'material-ui-icons/FileUpload'
import AddDocument from './addDocumentModal'
import CustomTable from '../../components/table'
import FileDownload from 'material-ui-icons/FileDownload'
import ReactPaginate from 'react-paginate'
import KeyboardArrowLeftIcon from 'material-ui-icons/KeyboardArrowLeft'
import KeyboardArrowRightIcon from 'material-ui-icons/KeyboardArrowRight'
import IconButton from 'material-ui/IconButton'
import fileDownloader from '../../services/fileDownloader'
import FaFileO from 'react-icons/lib/fa/file-o'
import { extensionIcons } from '../../services/fileExtensionIcons'

const tabs = [
  {
    id: 'all',
    label: 'All',
  },
  {
    id: 'messanger',
    label: 'Messanger',
  },
  {
    id: 'proposal',
    label: 'Documents',
  },
  {
    id: 'engagements',
    label: 'Engagements',
  },
  {
    id: 'other',
    label: 'Other',
  }
];

const columnData = [
  { id: 'name', label: 'Document name', render: (row) => {return <span><span className="doc-icon">{extensionIcons[row.name.split('.').pop()] || <FaFileO/>}</span> {row.name}</span>} },
  { id: 'type', label: 'Type' },
  { id: ['firstName', 'lastName'], label: 'Upload by' },
  { id: 'uploadDate', label: 'Upload date', date: true}
];


export default class Documents extends Component {
  constructor(props){
  	super(props);
  	this.state = {
      tabValue: 'all',
      showModal: false,
      page_number: 1,
      limit: 10
    };
  }

  handleTabChange(e, value) {
    this.setState({ tabValue: value });
  }

  showAddDocModal() {
    this.setState({showModal: true})
  }

  closeAddDocModal() {
    this.setState({showModal: false})
  }

  handlePageClick(page) {
    let { selected } = page;
    const { filters, limit } = this.state;
    let page_number = ++selected;
    this.setState({page_number});
    const { id } = this.props.match.params;
    this.props.getClientDocumentsRequest({client_id: id, page_number, filters, limit});
  }

  componentDidMount() {
    let limit = this.state.limit;
    if(window.innerWidth < 960) {
      limit = 5;
      this.setState({limit: 5})
    }
    const { id } = this.props.match.params;
    this.props.getClientDocumentsRequest({client_id: id, page_number: 1, limit})
  }

  render() {
    const { limit } = this.state;
    return (
      <div id="client-documents">
        <Tabs value={this.state.tabValue} onChange={this.handleTabChange.bind(this)} className="documents-tabs">
          {tabs.map((t, i) => <Tab key={i} value={t.id} label={t.label} className="tab"/>)}
        </Tabs>
        <Divider/>
        <div className="documents-toolbar">
          <p className="add-doc" onClick={() => this.showAddDocModal()}><FileUpload/> ADD DOCUMENT</p>
        </div>
        <div className="documents-table">
          <Divider/>
          <CustomTable
            data={this.props.documents}
            columnData={columnData}
            showEdit
            showDelete
            onDeleteClick={() => {}}
            onEditClick={() => {}}
            onCopyClick={() => {}}
            customActions={[
              <div action={(row) => fileDownloader(row.link, row.name)} visibility={(row) => true}><FileDownload/> Download</div>
            ]}
          />
          <ReactPaginate
            previousLabel={<IconButton aria-label="previeus"><KeyboardArrowLeftIcon/></IconButton>}
            nextLabel={<IconButton aria-label="next"><KeyboardArrowRightIcon/></IconButton>}
            breakLabel={<a href="">...</a>}
            breakClassName={"break-me"}
            pageCount={Math.ceil(this.props.lengthOfDocuments/limit)}
            marginPagesDisplayed={1}
            pageRangeDisplayed={2}
            onPageChange={(page) => this.handlePageClick(page)}
            containerClassName={"pagination"}
            activeClassName={"active"}
            disabledClassName={"disabled"}
            forcePage={this.state.page_number - 1}
         />
        </div>
        {this.state.showModal && <AddDocument
          showModal={this.state.showModal}
          closeAddDocModal={this.closeAddDocModal.bind(this)}
          {...this.props}
        />}
      </div>
    )
  }
}
