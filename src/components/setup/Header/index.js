import React from 'react'
import './index.css'
import SquareLogo from '../../../static/images/square-logo.svg'
import IconLogOut from '../../../static/images/icon_log_out.png'
import Hidden from 'material-ui/Hidden'
import IconButton from 'material-ui/IconButton';

const SetupHeader = ({ stepInfo, push }) => {
  return (
    <div className="setup-header-view">
      <Hidden mdUp>
        <img className="square-logo" src={SquareLogo} alt=""/>
      </Hidden>
      <div className="setup-header-text">
        <h1>Just a few quick things to set up your account</h1>
        {stepInfo.number && <p>Step {stepInfo.number} of 2</p>}
        <p>{stepInfo.description}</p>
      </div>
      <Hidden mdUp>
        <IconButton className="logout-icon" onClick={() => push('/signin')}>
          <img src={IconLogOut} alt=""/>
        </IconButton>
      </Hidden>
      <Hidden mdUp><div className="devider"></div></Hidden>
    </div>
  )
}

export default SetupHeader
