import React, { Component } from 'react';
import '../index.css';
import SetupHeader from '../Header';
import Grid from 'material-ui/Grid';
import Input from 'material-ui/Input/Input';
import InputLabel from 'material-ui/Input/InputLabel';
import FormControl from 'material-ui/Form/FormControl';
import Button from 'material-ui/Button';
import Hidden from 'material-ui/Hidden';
import FormHelperText from 'material-ui/Form/FormHelperText';
import MaskedInput from 'react-text-mask';
import isLength from 'validator/lib/isLength';
import isAlphanumeric from 'validator/lib/isAlphanumeric';

const stepInfo = {
  description: 'Tell us a little bit about yourself as a staff member.'
}

// eslint-disable-next-line
const passwordRegExp = /^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z])(?=.*[!@#$&±%^()<>/|"\"]).{8}/;

export default class StaffStep extends Component {
  constructor(props){
  	super(props);
  	this.state = {
      passwordMismatch: false,
      password: null,
      confirmPassword: null,
      firstName: null,
      lastName: null,
      phone: null
    };
  }

  handleFirstNameChange(e) {
    const FirstName = e.target.value.trim();
    this.setState({firstName: FirstName, requireFirstName: false})
    if(!isLength(FirstName, {min: 0, max: 64})) {
      this.setState({firstNameError: 'The text entered exceeds the maximum length'})
    } else if(FirstName && !isAlphanumeric(FirstName)) {
      this.setState({firstNameError: 'The string must be entirely alphanumeric'})
    } else {
      this.setState({firstNameError: null})
    }
  }

  handleLastNameChange(e) {
    const LastName = e.target.value.trim();
    this.setState({lastName: LastName, requireLastName: false})
    if(!isLength(LastName, {min: 0, max: 64})) {
      this.setState({lastNameError: 'The text entered exceeds the maximum length'})
    } else if(LastName && !isAlphanumeric(LastName)) {
      this.setState({lastNameError: 'The string must be entirely alphanumeric'})
    } else {
      this.setState({lastNameError: null})
    }
  }
  
  handlePhoneChange(e) {
    if(/\d/.test(e.target.value)) {
      this.setState({phone: e.target.value, requirePhone: false})
    } else {
      this.setState({phone: null, requirePhone: true})
    }
  }

  onPhoneBlur() {
    this.setState({phoneFocused: false})
    if(this.state.phone && this.state.phone.replace(/\D/g, '').length !== 10) {
      this.setState({requirePhone: true})
    }
  }

  handlePasswordChange(e) {
    if(this.state.confirmPassword && e.target.value !== this.state.confirmPassword) {
      this.setState({password: e.target.value, requirePassword: false, passwordMismatch: true})
    } else {
      this.setState({password: e.target.value, requirePassword: false, passwordMismatch: false})
    }
    if(passwordRegExp.test(e.target.value)) {
      this.setState({passwordError: false})
    }
  }

  handlePasswordBlur() {
    if(!passwordRegExp.test(this.state.password)) {
      this.setState({passwordError: true})
    }
  }

  handleConfirmPasswordChange(e) {
    if(e.target.value !== this.state.password) {
      this.setState({confirmPassword: e.target.value, requireConfirmPassword: false, passwordMismatch: true})
    } else {
      this.setState({confirmPassword: e.target.value, requireConfirmPassword: false, passwordMismatch: false})
    }
  }

  handleFormSubmit() {
    let requiredFields = this.checkRequiredFields();
    if(
      !requiredFields.password &&
      !requiredFields.confirmPassword &&
      !this.state.passwordMismatch &&
      !requiredFields.firstName &&
      !requiredFields.lastName &&
      !requiredFields.phone
    ) {
      const { password, firstName, lastName, phone } = this.state;
      const formData = { password, firstName, lastName, phone }
      this.props.handleStaffFormSubmit(formData);
    }
  }

  checkRequiredFields() {
    let requiredFields = {
      password: true,
      confirmPassword: true,
      firstName: true,
      lastName: true,
      phone: true
    }
    if(this.state.password && !this.state.passwordError) {
      requiredFields.password = false;
    } else {
       this.setState({requirePassword: true})
    }

    if(this.state.confirmPassword) {
      requiredFields.confirmPassword = false;
    } else {
       this.setState({requireConfirmPassword: true})
    }

    if(this.state.firstName && !this.state.firstNameError) {
      requiredFields.firstName = false;
    } else {
       this.setState({requireFirstName: true})
    }

    if(this.state.lastName && !this.state.lastNameError) {
      requiredFields.lastName = false;
    } else {
       this.setState({requireLastName: true})
    }


    if(this.state.phone) {
      requiredFields.phone = false;
    } else {
       this.setState({requirePhone: true})
    }

    return requiredFields;
  }

  render () {
    return (
      <Grid container className="staff-step-content" justify="center">
        <Grid item xs={12}>
          <SetupHeader stepInfo={stepInfo} push={this.props.push}/>
        </Grid>
        <Grid item md={6} sm={12} xs={12}>
          <div className="form-view">
            <FormControl fullWidth className="form-control">
              <InputLabel htmlFor="email" className="label">
                Your email
              </InputLabel>
              <Input
                id="email"
                className="input disabled"
                placeholder="Enter your email"
                value={this.props.profile ? this.props.profile.email : ''}
                disabled
              />
            </FormControl>
            <FormControl fullWidth className="form-control">
              <InputLabel htmlFor="company" className="label">
                Company
              </InputLabel>
              <Input
                id="company"
                className="input disabled"
                placeholder="Enter company name"
                value={this.props.profile ? this.props.profile.company : ''}
                disabled
              />
            </FormControl>
            <div className="two-inputs-inline">
              <FormControl fullWidth className="form-control">
                <InputLabel htmlFor="firstName" className="label">
                  First Name
                </InputLabel>
                <Input
                  id="firstName"
                  placeholder="Enter your First Name"
                  onChange={(e) => this.handleFirstNameChange(e)}
                />
                {this.state.requireFirstName && <FormHelperText error>First Name are Required</FormHelperText>}
                {this.state.firstNameError && <FormHelperText error>{this.state.firstNameError}</FormHelperText>}
              </FormControl>
              <FormControl fullWidth className="form-control">
                <InputLabel htmlFor="lastName" className="label">
                  Last Name
                </InputLabel>
                <Input
                  id="lastName"
                  placeholder="Enter your Last Name"
                  onChange={(e) => this.handleLastNameChange(e)}
                />
                {this.state.requireLastName && <FormHelperText error>Last Name are Required</FormHelperText>}
                {this.state.lastNameError && <FormHelperText error>{this.state.lastNameError}</FormHelperText>}
              </FormControl>
            </div>
            <FormControl fullWidth className="form-control phone">
              <InputLabel htmlFor="phone" className="label" shrink={this.state.phoneFocused || !!this.state.phone}>
                Phone Number
              </InputLabel>
              <MaskedInput
                id="phone"
                className="masked-input"
                placeholder="Enter a phone number"
                guide={true} mask={['(', /[0-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
                onChange={(e) => this.handlePhoneChange(e)}
                onFocus={() => this.setState({phoneFocused: true})}
                onBlur={() => this.onPhoneBlur()}
              />
              {this.state.requirePhone && <FormHelperText error>Phone Number is Required</FormHelperText>}
            </FormControl>
            <FormControl fullWidth className="form-control">
              <InputLabel htmlFor="password" className="label">
                Create Password
              </InputLabel>
              <Input
                id="password"
                placeholder="Enter your password"
                type="password"
                onChange={(e) => this.handlePasswordChange(e)}
                onBlur={() => this.handlePasswordBlur()}
              />
              {this.state.requirePassword && <FormHelperText error>Password is Required</FormHelperText>}
              {this.state.passwordError && <FormHelperText error>The password should be alphanumeric, contain at least one upper case character and special symbol (%, #, !, etc.). Min length 8 characters.</FormHelperText>}
            </FormControl>
            <FormControl fullWidth className="form-control">
              <InputLabel htmlFor="confirmPassword" className="label">
                Confirm Password
              </InputLabel>
              <Input
                id="confirmPassword"
                placeholder="Confirm your password"
                type="password"
                onChange={(e) => this.handleConfirmPasswordChange(e)}
              />
              {
                (this.state.requireConfirmPassword && <FormHelperText error>Confirm Password is Required</FormHelperText>) ||
                (this.state.passwordMismatch && <FormHelperText error>The password doesn't match</FormHelperText>)
              }
            </FormControl>
            <Hidden mdUp>
              <Button raised className="green-btn next-step-btn" onClick={() => this.handleFormSubmit()}>
                 Next Step
              </Button>
            </Hidden>
          </div>
        </Grid>
        <Grid item xs={12} className="next-btn-view">
          <Hidden smDown>
            <Button raised className="green-btn next-step-btn" onClick={() => this.handleFormSubmit()}>
               Next Step
            </Button>
          </Hidden>
        </Grid>
      </Grid>
    )
  }
}
