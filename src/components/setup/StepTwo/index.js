import React, { Component } from 'react';
import '../index.css';
import SetupHeader from '../Header';
import Grid from 'material-ui/Grid';
import Input from 'material-ui/Input/Input';
import InputLabel from 'material-ui/Input/InputLabel';
import FormControl from 'material-ui/Form/FormControl';
import Button from 'material-ui/Button';
import Hidden from 'material-ui/Hidden';
import FormHelperText from 'material-ui/Form/FormHelperText';
import MaskedInput from 'react-text-mask';
import isLength from 'validator/lib/isLength';
import isPostalCode from 'validator/lib/isPostalCode';
import Autocomplete from '../../autocomplete';
import countries from '../../../assets/countries.json'
import states from '../../../assets/states.json'
import _ from 'lodash'
import { validateZipCodeApi } from '../../../services/api'

const stepInfo = {
  number: 2,
  description: 'Tell us a little bit about practice'
}

export default class StepTwo extends Component {
  constructor(props){
  	super(props);
    const { formData } = this.props;
  	this.state = {
      openImageEditor: false,
      companyName: formData.companyName || '',
      companyPhone: formData.companyPhone || '',
      companyAddress: formData.companyAddress || '',
      companyCity: formData.companyCity || '',
      companyCountry: formData.companyCountry || "United States",
      companyState: formData.companyState || '',
      numberOfEmployees: formData.numberOfEmployees || '',
      zipCode: formData.zipCode || '',
    };

  }

  handleCompanyNameChange(e) {
    const companyName = e.target.value;
    this.setState({companyName: companyName, requireCompanyName: false})
    if(isLength(companyName, {min: 0, max: 64})) {
      this.setState({companyNameError: null})
    } else {
      this.setState({companyNameError: 'The text entered exceeds the maximum length'})
    }
  }

  handleCompanyAddressChange(e) {
    const companyAddress = e.target.value;
    this.setState({companyAddress: companyAddress, requireCompanyAddress: false})
    if(isLength(companyAddress, {min: 0, max: 250})) {
      this.setState({companyAddressError: null})
    } else {
      this.setState({companyAddressError: 'The text entered exceeds the maximum length'})
    }
  }

  handleCompanyCityChange(e) {
    const companyCity = e.target.value;
    this.setState({companyCity: companyCity, requireCompanyCity: false})
    if(isLength(companyCity, {min: 0, max: 64})) {
      this.setState({companyCityError: null})
    } else {
      this.setState({companyCityError: 'The text entered exceeds the maximum length'})
    }
  }

  handleCompanyCountryChange(value) {
    const companyCountry = value ? value.trim() : value;
    this.setState({companyCountry: companyCountry, requireCompanyCountry: false})
    if(isLength(companyCountry, {min: 0, max: 64})) {
      this.setState({companyCountryError: null})
    } else {
      this.setState({companyCountryError: 'The text entered exceeds the maximum length'})
    }
  }

  handleCompanyStateChange(value) {
    const companyState = value ? value.trim() : value;
    this.setState({companyState: companyState, requireCompanyState: false})
    if(isLength(companyState, {min: 0, max: 64})) {
      this.setState({companyStateError: null})
    } else {
      this.setState({companyStateError: 'The text entered exceeds the maximum length'})
    }
  }

  handleCompanyPhoneChange(e) {
    if(/\d/.test(e.target.value)) {
      this.setState({companyPhone: e.target.value, requireCompanyPhone: false})
    } else {
      this.setState({companyPhone: null, requireCompanyPhone: true})
    }
  }

  onCompanyPhoneBlur() {
    this.setState({phoneFocused: false})
    if(this.state.companyPhone && this.state.companyPhone.replace(/\D/g, '').length !== 10) {
      this.setState({requireCompanyPhone: true})
    }
  }

  handleNumberOfEmployeesChange(e) {
    const numberOfEmployees = e.target.value;
    this.setState({numberOfEmployees: numberOfEmployees, requireNumberOfEmployees: false})
    if(isLength(numberOfEmployees, {min: 0, max: 64})) {
      this.setState({numberOfEmployeesError: null})
    } else {
      this.setState({numberOfEmployeesError: 'The text entered exceeds the maximum length'})
    }
  }

  handleZipCodeChange(e) {
    const ZipCode = e.target.value.trim();
    this.setState({zipCode: ZipCode, requireZipCode: false})
    if(ZipCode && isPostalCode(ZipCode, ['US'])) {
      this.setState({zipCodeError: null})
    }
  }

  onZipCodeBlur() {
    this.setState({zipCodeError: 'Checking zip code...', zipCodeStatus: ''})
    if(this.state.zipCode && isPostalCode(this.state.zipCode, ['US'])) {
      if(_.includes(["united states", "us", "usa"], this.state.companyCountry.toLowerCase())) {
        validateZipCodeApi(this.state.zipCode).then((res) => {
          this.setState({zipCodeStatus: 'Zip code is valid', requireZipCode: false, zipCodeError: null})
        }, (err) => {
          this.setState({zipCodeError: err.data.error, zipCodeStatus: ''})
        })
      } else {
        this.setState({zipCodeError: null})
      }
    } else {
      this.setState({zipCodeError: this.state.zipCode ? 'The zip code is not valid' : null})
    }
  }

  handleFormSubmit() {
    let requiredFields = this.checkRequiredFields();
    if(
      !requiredFields.companyName &&
      !requiredFields.companyPhone &&
      !requiredFields.companyAddress &&
      !requiredFields.companyCity &&
      !requiredFields.companyCountry &&
      !requiredFields.numberOfEmployees &&
      !requiredFields.zipCode &&
      !requiredFields.companyState
    ) {
      const { companyName, companyPhone, companyAddress, companyCity, companyCountry, numberOfEmployees, zipCode, companyState } = this.state;
      const companyForm = {
        companyName, companyPhone, companyAddress, companyCity, companyCountry, numberOfEmployees, zipCode, companyState
      }
      this.props.handleCpaFormSubmit(companyForm);
    }
  }

  checkRequiredFields() {
    let requiredFields = {
      companyName: true,
      companyPhone: true,
      companyAddress: false,
      companyCity: true,
      companyCountry: true,
      numberOfEmployees: true,
      zipCode: true,
      companyState: true
    }

    if(this.state.companyName && !this.state.companyNameError) {
      requiredFields.companyName = false;
    } else if(requiredFields.companyName) {
       this.setState({requireCompanyName: true})
    }

    if(this.state.companyPhone && !this.state.requireCompanyPhone) {
      requiredFields.companyPhone = false;
    } else if(requiredFields.companyPhone) {
       this.setState({requireCompanyPhone: true})
    }

    if(this.state.companyAddress && !this.state.companyAddressError) {
      requiredFields.companyAddress = false;
    } else if(requiredFields.companyAddress) {
       this.setState({requireCompanyAddress: true})
    }

    if(this.state.companyCity && !this.state.companyCityError) {
      requiredFields.companyCity = false;
    } else if(requiredFields.companyCity) {
       this.setState({requireCompanyCity: true})
    }

    if(this.state.companyCountry && !this.state.companyCountryError) {
      requiredFields.companyCountry = false;
    } else if(requiredFields.companyCountry) {
       this.setState({requireCompanyCountry: true})
    }

    if(this.state.companyState && !this.state.companyStateError) {
      requiredFields.companyState = false;
    } else if(requiredFields.companyState) {
       this.setState({requireCompanyState: true})
    }

    if(this.state.numberOfEmployees && !this.state.numberOfEmployeesError) {
      requiredFields.numberOfEmployees = false;
    } else if(requiredFields.numberOfEmployees) {
      this.setState({requireNumberOfEmployees: true})
    }

    if(this.state.zipCode && !this.state.zipCodeError) {
      requiredFields.zipCode = false;
    } else if(requiredFields.zipCode) {
       this.setState({requireZipCode: true})
    }

    return requiredFields;
  }

  handleRequestCancel = () => {
    this.setState({ openImageEditor: false });
  };

  handleRequestAgree = () => {
    if (this.editor) {
      const canvasScaledUrl = this.editor.getImageScaledToCanvas().toDataURL();
      this.setState({ openImageEditor: false, companyAvatar: canvasScaledUrl});
    }
  };

  handleGoPreviusStep() {
    const { companyName, companyPhone, companyAddress, companyCity, companyCountry, numberOfEmployees, zipCode, companyState } = this.state;
    const companyForm = {
      companyName, companyPhone, companyAddress, companyCity, companyCountry, numberOfEmployees, zipCode, companyState
    }
    this.props.handleGoToStep(1, companyForm);
  }

  render () {
    return (
      <Grid container className="step-two-content" justify="center">
        <Grid item xs={12}>
          <SetupHeader stepInfo={stepInfo} push={this.props.push}/>
        </Grid>
        <Grid item md={6} sm={12} xs={12}>
          <div className="form-view">
            <FormControl fullWidth className="form-control">
              <InputLabel htmlFor="companyName" className="label">
                Business Name <span className="require-asterisk">*</span>
              </InputLabel>
              <Input
                id="companyName"
                placeholder="Enter Company Name"
                onChange={(e) => this.handleCompanyNameChange(e)}
                disabled={this.props.pending}
                value={this.state.companyName}
              />
              {this.state.requireCompanyName && <FormHelperText error>Company Name is Required</FormHelperText>}
              {this.state.companyNameError && <FormHelperText error>{this.state.companyNameError}</FormHelperText>}
            </FormControl>
            <FormControl fullWidth className="form-control phone">
              <InputLabel htmlFor="companyPhone" className="label" shrink={this.state.phoneFocused || !!this.state.companyPhone}>
                Business Phone Number <span className="require-asterisk">*</span>
              </InputLabel>
              <MaskedInput
                className="masked-input"
                placeholder={(this.state.phoneFocused || !!this.state.companyPhone) ? "Enter a company phone number" : ""}
                guide={true} mask={['(', /[0-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
                onChange={(e) => this.handleCompanyPhoneChange(e)}
                onFocus={() => this.setState({phoneFocused: true})}
                onBlur={() => this.onCompanyPhoneBlur()}
                value={this.state.companyPhone}
                disabled={this.props.pending}
              />
              {this.state.requireCompanyPhone && <FormHelperText error>Company Phone is Required</FormHelperText>}
            </FormControl>
            <FormControl fullWidth className="form-control country">
              <Autocomplete
                suggestions={countries}
                handleChange={this.handleCompanyCountryChange.bind(this)}
                error={this.state.companyCountryError}
                label={"Country"}
                placeholder="Enter your country"
                reguiredError={this.state.requireCompanyCountry}
                isRequired
                defaultValue={this.state.companyCountry}
                disabled={this.props.pending}
              />
            </FormControl>
            <FormControl className="form-control" fullWidth>
              <Autocomplete
                suggestions={ _.includes(["united states", "us", "usa"], this.state.companyCountry.toLowerCase())  ? states : []}
                handleChange={this.handleCompanyStateChange.bind(this)}
                error={this.state.companyStateError}
                label={"State/Province/Region"}
                placeholder="Enter your region"
                reguiredError={this.state.requireCompanyState}
                isRequired
                defaultValue={this.state.companyState}
                disabled={this.props.pending}
              />
            </FormControl>
          </div>
        </Grid>
        <Grid item md={6} sm={12} xs={12}>
          <div className="form-view">
            <div className="two-inputs-inline">
              <FormControl className="form-control">
                <InputLabel htmlFor="city" className="label">
                  City <span className="require-asterisk">*</span>
                </InputLabel>
                <Input
                  id="city"
                  placeholder="Enter your city"
                  onChange={(e) => this.handleCompanyCityChange(e)}
                  disabled={this.props.pending}
                  value={this.state.companyCity}
                />
                {this.state.requireCompanyCity && <FormHelperText error>Company City is Required</FormHelperText>}
                {this.state.companyCityError && <FormHelperText error>{this.state.companyCityError}</FormHelperText>}
              </FormControl>
              <FormControl fullWidth className="form-control">
                <InputLabel htmlFor="address" className="label">
                  Address
                </InputLabel>
                <Input
                  id="address"
                  placeholder="Enter your address"
                  onChange={(e) => this.handleCompanyAddressChange(e)}
                  disabled={this.props.pending}
                  value={this.state.companyAddress}
                />
                {this.state.requireCompanyAddress && <FormHelperText error>Company Address is Required</FormHelperText>}
                {this.state.companyAddressError && <FormHelperText error>{this.state.companyAddressError}</FormHelperText>}
              </FormControl>
            </div>
            <FormControl fullWidth className="form-control">
              <InputLabel htmlFor="zip" className="label">
                Zip code <span className="require-asterisk">*</span>
              </InputLabel>
              <Input
                id="zip"
                placeholder="Enter zip code"
                onChange={(e) => this.handleZipCodeChange(e)}
                onBlur={() => this.onZipCodeBlur()}
                type="number"
                value={this.state.zipCode}
                disabled={this.props.pending}
              />
              {this.state.requireZipCode && <FormHelperText error>Zip Code is Required</FormHelperText>}
              {this.state.zipCodeError && <FormHelperText error>{this.state.zipCodeError}</FormHelperText>}
              {this.state.zipCodeStatus && <FormHelperText>{this.state.zipCodeStatus}</FormHelperText>}
            </FormControl>
            <FormControl fullWidth className="form-control">
              <InputLabel htmlFor="employees" className="label">
                Number of Employees <span className="require-asterisk">*</span>
              </InputLabel>
              <Input
                id="employees"
                type="number"
                placeholder="Enter Number of Employees"
                onChange={(e) => this.handleNumberOfEmployeesChange(e)}
                disabled={this.props.pending}
                value={this.state.numberOfEmployees}
              />
              {this.state.requireNumberOfEmployees && <FormHelperText error>Number of Employees is Required</FormHelperText>}
              {this.state.numberOfEmployeesError && <FormHelperText error>{this.state.numberOfEmployeesError}</FormHelperText>}
            </FormControl>
            <Hidden mdUp>
              <Button raised className="green-btn next-step-btn" onClick={() => this.handleFormSubmit()} disabled={this.props.pending}>
                 Complete
              </Button>
            </Hidden>
          </div>
        </Grid>
        <Grid item xs={12} className="next-btn-view">
          <Hidden smDown>
            <div>
              <Button className="btn previous-step-btn" onClick={() => this.handleGoPreviusStep()} disabled={this.props.pending}>
                 Previous step
              </Button>
              <Button raised className="green-btn next-step-btn" onClick={() => this.handleFormSubmit()} disabled={this.props.pending}>
                 Complete
              </Button>
            </div>
          </Hidden>
        </Grid>
      </Grid>
    )
  }
}
