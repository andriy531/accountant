import React from 'react'
import FaCheckCircle from 'react-icons/lib/fa/check-circle'
import './passwordBulletPoints.css'

const PasswordBulletPoints = ({rules}) => {
  return (
    <div id="password-rules-container">
      <p className={rules.lowerCase ? "point" : "point error"}><FaCheckCircle className="check-icon"/> include a lower case letter</p>
      <p className={rules.upperCase ? "point" : "point error"}><FaCheckCircle className="check-icon"/> include an upper case letter</p>
      <p className={rules.number ? "point" : "point error"}><FaCheckCircle className="check-icon"/> include a number</p>
      <p className={rules.special ? "point" : "point error"}><FaCheckCircle className="check-icon"/> include a special symbol</p>
      <p className={rules.length ? "point" : "point error"}><FaCheckCircle className="check-icon"/> have 8 characters or more</p>
    </div>
  )
}

export default PasswordBulletPoints
