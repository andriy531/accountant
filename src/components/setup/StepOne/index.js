import React, { Component } from 'react';
import '../index.css';
import SetupHeader from '../Header';
import Grid from 'material-ui/Grid';
import Input from 'material-ui/Input/Input';
import InputLabel from 'material-ui/Input/InputLabel';
import FormControl from 'material-ui/Form/FormControl';
import Button from 'material-ui/Button';
import Hidden from 'material-ui/Hidden';
import FormHelperText from 'material-ui/Form/FormHelperText';
import MaskedInput from 'react-text-mask';
import isLength from 'validator/lib/isLength';
import isAlphanumeric from 'validator/lib/isAlphanumeric';
import isURL from 'validator/lib/isURL';
import PasswordBulletPoints from './passwordBulletPoints'
import { validateBusinessDomainApi } from '../../../services/api'

const stepInfo = {
  number: 1,
  description: 'Tell us a little bit about yourself.'
}

function checkPasswordRules(password) {
  let rules = {
    lowerCase: false,
    upperCase: false,
    number: false,
    length: false,
    special: false
  }
  if(!password) {
    return rules
  }
  if(/^(?=.*[A-Z])/.test(password)) {
    rules.upperCase = true;
  } else {
    rules.upperCase = false;
  }
  if(/^(?=.*[0-9])/.test(password)) {
    rules.number = true;
  } else {
    rules.number = false;
  }
  if(/^(?=.*[a-z])/.test(password)) {
    rules.lowerCase = true;
  } else {
    rules.lowerCase = false;
  }
  // eslint-disable-next-line
  if(/^(?=.*[!@#$&±%^()<>/|"\"])/.test(password)) {
    rules.special = true;
  } else {
    rules.special = false;
  }
  if(/^.{8}/.test(password)) {
    rules.length = true;
  } else {
    rules.length = false;
  }

  return rules;
}

export default class StepOne extends Component {
  constructor(props){
  	super(props);
    const { formData } = this.props;
  	this.state = {
      openImageEditor: false,
      passwordMismatch: false,
      firstName: formData ? formData.firstName : '',
      lastName: formData ? formData.lastName : '',
      phone: formData ? formData.phone : '',
      businessDomain: formData ? formData.businessDomain : '',
      password: formData ? formData.password : '',
      confirmPassword: formData ? formData.password : ''
    };
  }

  handleFirstNameChange(e) {
    const FirstName = e.target.value.trim();
    this.setState({firstName: FirstName, requireFirstName: false})
    if(!isLength(FirstName, {min: 0, max: 64})) {
      this.setState({firstNameError: 'The text entered exceeds the maximum length'})
    } else if(FirstName && !isAlphanumeric(FirstName)) {
      this.setState({firstNameError: 'The string must be entirely alphanumeric'})
    } else {
      this.setState({firstNameError: null})
    }
  }

  handleLastNameChange(e) {
    const LastName = e.target.value.trim();
    this.setState({lastName: LastName, requireLastName: false})
    if(!isLength(LastName, {min: 0, max: 64})) {
      this.setState({lastNameError: 'The text entered exceeds the maximum length'})
    } else if(LastName && !isAlphanumeric(LastName)) {
      this.setState({lastNameError: 'The string must be entirely alphanumeric'})
    } else {
      this.setState({lastNameError: null})
    }
  }

  handleBusinessDomainChange(e) {
    const businessDomain = e.target.value.trim() + ".countup.io";
    this.setState({businessDomain: businessDomain, requireBusinessDomain: false})
    if(!isLength(businessDomain, {min: 4, max: 64})) {
      this.setState({businessDomainError: 'The text entered exceeds the maximum length'})
    } else if(!isURL(businessDomain)) {
      this.setState({businessDomainError: 'The business domain URL is not valid'})
    } else {
      this.setState({businessDomainError: null})
    }
  }

  handleBusinessDomainBlur(e) {
    this.setState({businessDomainError: 'Checking domain...', businessDomainStatus: ''})
    if(e.target.value.trim().length >= 4) {
      const businessDomain = e.target.value.trim() + ".countup.io";
      validateBusinessDomainApi(businessDomain).then((res => {
        this.setState({requireBusinessDomain: false, businessDomainError: null, businessDomainStatus: 'Domain is free'})
      }), (err => {
        this.setState({requireBusinessDomain: false, businessDomainError: err.data.error, businessDomainStatus: ''})
      }))
    } else {
      this.setState({businessDomainError: 'The text should be 4 characters min'})
    }
  }

  handlePhoneChange(e) {
    if(/\d/.test(e.target.value)) {
      this.setState({phone: e.target.value, requirePhone: false})
    } else {
      this.setState({phone: null, requirePhone: true})
    }
  }

  onPhoneBlur() {
    this.setState({phoneFocused: false})
    if(this.state.phone && this.state.phone.replace(/\D/g, '').length !== 10) {
      this.setState({requirePhone: true})
    }
  }

  handlePasswordChange(e) {
    const rules = checkPasswordRules(e.target.value);
    const { lowerCase, upperCase, number, length, special } = rules;
    if(this.state.confirmPassword && e.target.value !== this.state.confirmPassword) {
      this.setState({password: e.target.value, requirePassword: false, passwordMismatch: true, rules})
    } else {
      this.setState({password: e.target.value, requirePassword: false, passwordMismatch: false, rules})
    }

    if(lowerCase && upperCase && number && length && special) {
      this.setState({passwordError: false})
    }
  }

  handlePasswordBlur() {
    const rules = checkPasswordRules(this.state.password);
    const { lowerCase, upperCase, number, length, special } = rules;
    if(!lowerCase || !upperCase || !number || !length || !special) {
      this.setState({passwordError: true, rules })
    }
  }

  handleConfirmPasswordChange(e) {
    if(e.target.value !== this.state.password) {
      this.setState({confirmPassword: e.target.value, requireConfirmPassword: false, passwordMismatch: true})
    } else {
      this.setState({confirmPassword: e.target.value, requireConfirmPassword: false, passwordMismatch: false})
    }
  }

  handleFormSubmit() {
    let requiredFields = this.checkRequiredFields();
    if(
      !requiredFields.password &&
      !requiredFields.confirmPassword &&
      !this.state.passwordMismatch &&
      !requiredFields.firstName &&
      !requiredFields.lastName &&
      !requiredFields.phone &&
      !requiredFields.businessDomain
    ) {
      const { password, firstName, lastName, phone, businessDomain } = this.state;
      const formData = { password, firstName, lastName, phone, businessDomain }
      this.props.handleGoToStep(2, formData);
    }
  }

  checkRequiredFields() {
    let requiredFields = {
      password: true,
      confirmPassword: true,
      firstName: true,
      lastName: true,
      phone: true,
      businessDomain: true,
    }
    if(this.state.password && !this.state.passwordError) {
      requiredFields.password = false;
    } else {
       this.setState({requirePassword: true})
    }

    if(this.state.confirmPassword) {
      requiredFields.confirmPassword = false;
    } else {
       this.setState({requireConfirmPassword: true})
    }

    if(this.state.firstName && !this.state.firstNameError) {
      requiredFields.firstName = false;
    } else {
       this.setState({requireFirstName: true})
    }

    if(this.state.lastName && !this.state.lastNameError) {
      requiredFields.lastName = false;
    } else {
       this.setState({requireLastName: true})
    }

    if(this.state.businessDomain && !this.state.businessDomainError) {
      requiredFields.businessDomain = false;
    } else {
       this.setState({requireBusinessDomain: true})
    }

    if(this.state.phone && !this.state.requirePhone) {
      requiredFields.phone = false;
    } else {
       this.setState({requirePhone: true})
    }

    return requiredFields;
  }

  render () {
    return (
      <Grid container className="step-one-content" justify="center">
        <Grid item xs={12}>
          <SetupHeader stepInfo={stepInfo} push={this.props.push}/>
        </Grid>
        <Grid item md={6} sm={12} xs={12}>
          <div className="form-view">
            <FormControl fullWidth className="form-control">
              <InputLabel htmlFor="email" className="label">
                Your email
              </InputLabel>
              <Input
                id="email"
                className="input disabled"
                placeholder="Enter your email"
                value={this.props.profile ? this.props.profile.email : ''}
                disabled
              />
            </FormControl>
            <FormControl fullWidth className="form-control">
              <InputLabel htmlFor="password" className="label">
                Create Password <span className="require-asterisk">*</span>
              </InputLabel>
              <Input
                id="password"
                placeholder="Enter your password"
                type="password"
                onChange={(e) => this.handlePasswordChange(e)}
                onBlur={() => this.handlePasswordBlur()}
                value={this.state.password}
              />
              {this.state.requirePassword && <FormHelperText error>Password is Required</FormHelperText>}
              {this.state.passwordError && <PasswordBulletPoints rules={this.state.rules}/>}
            </FormControl>
            <FormControl fullWidth className="form-control">
              <InputLabel htmlFor="confirmPassword" className="label">
                Confirm Password <span className="require-asterisk">*</span>
              </InputLabel>
              <Input
                id="confirmPassword"
                placeholder="Confirm your password"
                type="password"
                onChange={(e) => this.handleConfirmPasswordChange(e)}
                value={this.state.confirmPassword}
              />
              {
                (this.state.requireConfirmPassword && <FormHelperText error>Confirm Password is Required</FormHelperText>) ||
                (this.state.passwordMismatch && <FormHelperText error>The password doesn't match</FormHelperText>)
              }
            </FormControl>
          </div>
        </Grid>
        <Grid item md={6} sm={12} xs={12}>
          <div className="form-view">
            <div className="two-inputs-inline">
              <FormControl fullWidth className="form-control">
                <InputLabel htmlFor="firstName" className="label">
                  First Name <span className="require-asterisk">*</span>
                </InputLabel>
                <Input
                  value={this.state.firstName}
                  id="firstName"
                  placeholder="Enter your First Name"
                  onChange={(e) => this.handleFirstNameChange(e)}
                />
                {this.state.requireFirstName && <FormHelperText error>First Name are Required</FormHelperText>}
                {this.state.firstNameError && <FormHelperText error>{this.state.firstNameError}</FormHelperText>}
              </FormControl>
              <FormControl fullWidth className="form-control">
                <InputLabel htmlFor="lastName" className="label">
                  Last Name <span className="require-asterisk">*</span>
                </InputLabel>
                <Input
                  value={this.state.lastName}
                  id="lastName"
                  placeholder="Enter your Last Name"
                  onChange={(e) => this.handleLastNameChange(e)}
                />
                {this.state.requireLastName && <FormHelperText error>Last Name are Required</FormHelperText>}
                {this.state.lastNameError && <FormHelperText error>{this.state.lastNameError}</FormHelperText>}
              </FormControl>
            </div>
            <FormControl fullWidth className="form-control phone">
              <InputLabel htmlFor="phone" className="label" shrink={this.state.phoneFocused || !!this.state.phone}>
                Phone Number <span className="require-asterisk">*</span>
              </InputLabel>
              <MaskedInput
                id="phone"
                className="masked-input"
                placeholder={(this.state.phoneFocused || !!this.state.phone) ? "Enter a phone number" : ""}
                guide={true} mask={['(', /[0-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
                onChange={(e) => this.handlePhoneChange(e)}
                onFocus={() => this.setState({phoneFocused: true})}
                onBlur={() => this.onPhoneBlur()}
                value={this.state.phone}
              />
              {this.state.requirePhone && <FormHelperText error>Phone Number is Required</FormHelperText>}
            </FormControl>
            <FormControl fullWidth className="form-control">
              <InputLabel htmlFor="domain" className="label">
                <Hidden mdDown><span>Create your</span></Hidden> business domain <span className="require-asterisk">*</span>
              </InputLabel>
              <Input
                id="name"
                placeholder="Enter business domain"
                className="domain-input"
                onChange={(e) => this.handleBusinessDomainChange(e)}
                onBlur={(e) => this.handleBusinessDomainBlur(e)}
                value={this.state.businessDomain.replace(".countup.io", "")}
              />
              <span className="domain">.countup.io</span>
              {this.state.requireBusinessDomain && <FormHelperText error>Business Domain is Required</FormHelperText>}
              {this.state.businessDomainError && <FormHelperText error>{this.state.businessDomainError}</FormHelperText>}
              {this.state.businessDomainStatus && <FormHelperText>{this.state.businessDomainStatus}</FormHelperText>}
            </FormControl>

            <Hidden mdUp>
              <Button raised className="green-btn next-step-btn" onClick={() => this.handleFormSubmit()}>
                 Next Step
              </Button>
            </Hidden>
          </div>
        </Grid>
        <Grid item xs={12} className="next-btn-view">
          <Hidden smDown>
            <Button raised className="green-btn next-step-btn" onClick={() => this.handleFormSubmit()}>
               Next Step
            </Button>
          </Hidden>
        </Grid>
      </Grid>
    )
  }
}
