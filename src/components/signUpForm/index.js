import React, { Component } from 'react'
import Grid from 'material-ui/Grid';
import Input from 'material-ui/Input/Input';
import InputLabel from 'material-ui/Input/InputLabel';
import FormControl from 'material-ui/Form/FormControl';
import Button from 'material-ui/Button';
import FormHelperText from 'material-ui/Form/FormHelperText';
import isEmail from 'validator/lib/isEmail';


export default class SignUpForm extends Component {
  constructor(props) {
  	super(props);
  	this.state = {
      invalidEmail: false,
      email: null,
      password: null
    };
  }

  handleEmailChange(e) {
    const email = e.target.value.trim();
    if(email && isEmail(email)) {
      this.setState({invalidEmail: false})
    }
    this.setState({email: email, requireEmail: false})
  }

  handleFormSubmit() {
    let requiredFields = this.checkRequiredFields();
    if(!requiredFields.email) {
      this.props.postSignUpRequest({email: this.state.email})
    }
  }

  checkRequiredFields() {
    let requiredFields = {
      email: true
    }
    if (this.state.email && !this.state.invalidEmail) {
      requiredFields.email = false;
    } else {
      this.setState({requireEmail: true})
    }
    return requiredFields;
  }

  validateEmail() {
    if(this.state.email && isEmail(this.state.email)) {
      this.setState({invalidEmail: false})
    } else {
      this.setState({invalidEmail: this.state.email ? true : false})
    }
  }

  render() {
    return (
      <Grid item xs={10} sm={5} className="form-content">
        <h1>Grow your practice online!</h1>
        <p className="tip">Get started by filling your info</p>
        <FormControl fullWidth className="form-control">
          <InputLabel htmlFor="email">
            Your e-mail
          </InputLabel>
          <Input
            id="email"
            placeholder="Enter your email"
            onChange={(e) => this.handleEmailChange(e)}
            onBlur={() => this.validateEmail()}
            error={this.state.invalidEmail}
            disabled={this.props.pending}
          />
          {
            (this.state.invalidEmail && <FormHelperText error>Invalid Email Address</FormHelperText>) ||
            (this.state.requireEmail && <FormHelperText error>Email Address is Required</FormHelperText>)
          }
        </FormControl>
        <Button disabled={this.props.pending} raised className="green-btn sign-up-btn" onClick={() => this.handleFormSubmit()}>
           Sign Up
        </Button>
        <br/>
        <br/>
        <p className="policy">By clicking 'Sign Up' you agree to Count Up`s <br/>
          <a>Terms of service</a> and <a>Privacy Policy</a>
        </p>
      </Grid>
    )
  }
}
