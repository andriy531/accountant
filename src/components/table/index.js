import React, { Component } from 'react'
import './index.css'
import Table, {
  TableBody,
  TableCell,
  TableRow,
} from 'material-ui/Table';
import CustomTableHead from './tableHead'
import _ from 'lodash'
import EditIcon from 'material-ui-icons/Edit'
import ContentCopyIcon from 'material-ui-icons/ContentCopy'
import DeleteIcon from 'material-ui-icons/Delete'
import moment from 'moment'
import ExpandMoreIcon from 'material-ui-icons/ExpandMore'
import Menu, { MenuItem } from 'material-ui/Menu'
import Tooltip from 'material-ui/Tooltip'


//How to use
//id -> string or array of string
//we can set multiple props to show in one column (instead id: 'test' -> id: ['test1', 'test2'])
//label -> string
//width -> string
//showTooltip -> string that identifies property for tooltip
//tooltip -> function that accept table row as parametr and should return tooltip content
//date -> bool
//numeric -> bool
//onClick -> function
//EXAMPLE
// const columnData = [
//   { id: ['firstName', 'lastName'], label: 'Name', width: '25%', showTooltip: "tags", tooltip: (row) => <p>{row.tags && row.tags.join(", ")}</p> },
//   { id: 'type', label: 'Type', width: '10%' },
//   { id: 'registrationDate', label: 'Registered', date: true, width: '10%' },
//   { id: 'engagementsNumber', label: 'Engagements', numeric: true, width: '15%' },
// ];

export default class CustomTable extends Component {
  state = {
    order: 'asc',
    //orderBy: 'name',
    data: this.props.data,
    anchorEl: null,
    openMenu: false,
  };

  handleOpenMenu(event, index) {
    this.setState({ openMenu: index, anchorEl: event.currentTarget });
  };

  handleCloseMenu() {
    this.setState({ openMenu: null });
  };

  handleRequestSort = (event, prop) => {
    let property = Array.isArray(prop) ?  prop[0] : prop;
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    const data = this.state.data.sort(
      (a, b) => (order === 'desc' ? b[orderBy] > a[orderBy] : a[orderBy] > b[orderBy]),
    );

    this.setState({ data, order, orderBy });
  };

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.data, nextProps.data)) {
      this.setState({data: nextProps.data})
    }
  }

  render() {
    const { data, order, orderBy } = this.state;
    return (
      <Table id="custom-table">
        <CustomTableHead
          order={order}
          orderBy={orderBy}
          onRequestSort={this.handleRequestSort}
          columnData={this.props.columnData}
        />
        <TableBody>
          {data.map((n, i) => {
            return (
              <TableRow
                hover
                tabIndex={-1}
                key={i}
              >
                {this.props.columnData.map((c, i) => {
                  return c.render ?
                    <TableCell
                      key={i}
                      numeric={c.numeric}
                      className={c.numeric ? "numeric": ""}
                      >
                    <span>{c.render(n)}</span>
                  </TableCell> : <TableCell
                    key={i}
                    numeric={c.numeric}
                    className={c.numeric ? "numeric": ""}
                    >
                    <Tooltip
                      disableTriggerHover={!c.showTooltip || !n[c.showTooltip] || !n[c.showTooltip].length}
                      title={c.tooltip && c.tooltip(n)}
                      className="table-tooltip"
                    >
                      <span onClick={() => c.onClick && c.onClick(n)}>
                        {
                          // we can set multiple props to show in one column (instead id: 'test' -> id: ['test1', 'test2'])
                          Array.isArray(c.id) ? _.map(c.id, (o) => ' ' + n[o]) : (c.date && n[c.id]) ? moment(n[c.id]).format('DD/MM/YYYY') : n[c.id]
                        }
                      </span>
                    </Tooltip>
                  </TableCell>
                })}
                <TableCell className="actions-cell">
                  <ExpandMoreIcon onClick={(e) => this.handleOpenMenu(e, i)}/>
                  <Menu
                    id="table-action-menu"
                    anchorEl={this.state.anchorEl}
                    open={this.state.openMenu === i}
                    onRequestClose={this.handleCloseMenu.bind(this)}
                  >
                    {
                      this.props.showEdit &&
                      <MenuItem onClick={() => {this.props.onEditClick(n); this.handleCloseMenu()}} className="menu-item">
                        <EditIcon className="action-icon"/>
                        Edit
                      </MenuItem>
                    }
                    {
                      this.props.showCopy &&
                      <MenuItem onClick={() => {this.props.onCopyClick(n); this.handleCloseMenu()}} className="menu-item">
                        <ContentCopyIcon className="action-icon"/>
                        Duplicate
                      </MenuItem>
                    }
                    {
                      this.props.showDelete &&
                      <MenuItem
                        disabled={this.props.disableDelete && this.props.disableDelete(n)}
                        onClick={() => {this.props.onDeleteClick(n); this.handleCloseMenu()}} className="menu-item">
                        <DeleteIcon className="action-icon"/>
                        Delete
                      </MenuItem>
                    }
                    {
                      this.props.customActions && this.props.customActions.map((a, i) =>
                      (a.props.visibility(n) &&
                        <MenuItem
                          key={i}
                          onClick={() => { a.props.action(n); this.handleCloseMenu()}}
                          className="menu-item"
                        >
                          {a}
                        </MenuItem>
                      ))
                    }
                  </Menu>
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    );
  }
}
