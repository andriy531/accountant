import React, { Component } from 'react'
import './index.css'
import {
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel,
} from 'material-ui/Table';


export default class CustomTableHead extends Component {

  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };

  render() {
    const { order, orderBy } = this.props;
    return (
      <TableHead id="table-head">
        <TableRow className="row">
          {this.props.columnData.map(column => {
            return (
              <TableCell key={column.id} className={column.numeric ? "col numeric": "col"} style={{width: (column.width ?  column.width :  'auto')}}>
                <TableSortLabel
                  active={(orderBy === column.id) || (orderBy === column.id[0])}
                  direction={order}
                  onClick={this.createSortHandler(column.id)}
                >
                  <span className={column.numeric ? "label numeric": "label"}>{column.label}</span>
                </TableSortLabel>
              </TableCell>
            );
          }, this)}
          <TableCell className="actions-col"/>
        </TableRow>
      </TableHead>
    )
  }
}
