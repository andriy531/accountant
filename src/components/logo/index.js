import React from 'react'
import './index.css'
import SquareLogo from '../../static/images/square-logo.svg'
import FullLogo from '../../static/images/logo-white.svg'
import Hidden from 'material-ui/Hidden'

const Logo = () => {
  return (
    <div>
      <Hidden smDown>
        <img className="logo" src={FullLogo} alt=""/>
      </Hidden>
      <Hidden mdUp>
        <img className="logo" src={SquareLogo} alt=""/>
      </Hidden>
    </div>
  )
}

export default Logo
