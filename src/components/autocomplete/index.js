import React, { Component } from 'react'
import Autosuggest from 'react-autosuggest'
import { MenuItem } from 'material-ui/Menu'
import match from 'autosuggest-highlight/match'
import parse from 'autosuggest-highlight/parse'
import TextField from 'material-ui/TextField'
import Paper from 'material-ui/Paper'
import FormHelperText from 'material-ui/Form/FormHelperText'
import { withStyles } from 'material-ui/styles'

// https://developer.mozilla.org/en/docs/Web/JavaScript/Guide/Regular_Expressions#Using_Special_Characters
function escapeRegexCharacters(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

function getSuggestions(value, suggestions) {
  const escapedValue = escapeRegexCharacters(value.trim());
  const regex = new RegExp('^' + escapedValue, 'i');

  return suggestions.filter(language => regex.test(language.name));
}

function getSuggestionValue(suggestion) {
  return suggestion.name;
}

function renderSuggestion(suggestion, { query, isHighlighted }) {
  const matches = match(suggestion.name, query);
  const parts = parse(suggestion.name, matches);

  return (
    <MenuItem selected={isHighlighted} component="div">
      <div style={{overflow: 'hidden', textOverflow: 'ellipsis'}}>
        {parts.map((part, index) => {
          return part.highlight ? (
            <span key={index} style={{fontFamily: 'SF-Display-Thin, sans-serif'}}>
              {part.text}
            </span>
          ) : (
            <strong key={index} style={{ fontFamily: 'SF-Display-Regular, sans-serif' }}>
              {part.text}
            </strong>
          );
        })}
      </div>
    </MenuItem>
  );
}

function renderInput(inputProps) {
  const { autoFocus, value, ref, label, isRequired, ...other } = inputProps;

  const fieldLabel = <span>{label} {isRequired && <span className="require-asterisk">*</span>}</span>

  return (
    <TextField
      fullWidth
      autoFocus={autoFocus}
      value={value}
      inputRef={ref}
      label={fieldLabel}
      InputProps={{
        ...other,
      }}
    />
  );
}

function renderSuggestionsContainer(options) {
  const { containerProps, children } = options;

  return (
    <Paper {...containerProps} square style={styles.suggestionsContainer}>
      {children}
    </Paper>
  );
}

class Autocomplete extends Component {
  constructor(props){
  	super(props);
  	this.state = {
      value: this.props.defaultValue || '',
      suggestions: []
    };
  }

  onChange = (event, { newValue, method }) => {
    this.props.handleChange(newValue);
    this.setState({
      value: newValue
    });
  };

  onSuggestionsFetchRequested = ({ value }, suggestions) => {
    this.props.handleChange(value);
    this.setState({
      suggestions: getSuggestions(value, suggestions)
    });
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  render() {
    const { value, suggestions } = this.state;
    const inputProps = {
      placeholder: this.props.placeholder,
      value,
      onChange: this.onChange,
      label: this.props.label,
      isRequired: this.props.isRequired,
      disabled: this.props.disabled
    };
    const { classes } = this.props;
    return (
      <div>
        <Autosuggest
          theme={{
            container: classes.container,
            suggestionsContainerOpen: classes.suggestionsContainerOpen,
            suggestionsList: classes.suggestionsList,
            suggestion: classes.suggestion,
          }}
          suggestions={suggestions}
          onSuggestionsFetchRequested={(value) => this.onSuggestionsFetchRequested(value, this.props.suggestions)}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          getSuggestionValue={getSuggestionValue}
          renderSuggestion={renderSuggestion}
          inputProps={inputProps}
          renderInputComponent={renderInput}
          renderSuggestionsContainer={renderSuggestionsContainer}
        />
        {this.props.error && <FormHelperText error>{this.props.error}</FormHelperText>}
        {this.props.reguiredError && <FormHelperText error>{this.props.label} is required</FormHelperText>}
      </div>
    )
  }
}

const styles = theme => ({
  container: {
    flexGrow: 1,
    position: 'relative'
  },
  suggestionsContainerOpen: {
    position: 'absolute',
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit * 3,
    left: 0,
    right: 0,
    zIndex: 10
  },
  suggestion: {
    display: 'block'
  },
  suggestionsList: {
    margin: 0,
    padding: 0,
    listStyleType: 'none'
  },
  suggestionsContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    zIndex: 10
  }
});

export default withStyles(styles)(Autocomplete);
