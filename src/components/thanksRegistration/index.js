import React, { Component } from 'react'
import Grid from 'material-ui/Grid';
import './index.css'
import Done from 'material-ui-icons/Done'


export default class Thanks extends Component {
  render() {
    return (
      <Grid item xs={10} sm={7} className="form-content" id="thanks">
        <h1>Thanks for registration!</h1>
        <p className="tip">
          Your account has been created and a verification email has been sent to your registered email address.
          Please click on a verification link included in the email to activate your account.
        </p>
        <br/>
        <div className="done">
          <Done className="done-icon"/>
        </div>
      </Grid>
    )
  }
}
