import React, { Component } from 'react'
import './inviteStep.css'
import TextField from 'material-ui/TextField'
import isEmail from 'validator/lib/isEmail'
import isLength from 'validator/lib/isLength'
import _ from 'lodash'
import Input, { InputLabel } from 'material-ui/Input'
import { MenuItem } from 'material-ui/Menu'
import { FormControl } from 'material-ui/Form'
import Select from 'material-ui/Select'

export default class CreateClientInviteStep extends Component {

  constructor(props){
  	super(props);
  	this.state = {
      emailError: null,
      email: null,
      firstNameError: null,
      lastNameError: null,
      firstName: null,
      lastName: null
    };
  }

  handleEmailChange(e) {
    const email = e.target.value.trim();
    this.setState({email: email, emailError: null})
    if(email && isEmail(email)) {
      this.props.handleEmailChange(email);
    } else {
      this.props.handleEmailChange(null);
    }
  }

  validateEmail() {
    if(this.state.email && isEmail(this.state.email)) {
      this.setState({emailError: null})
    } else {
      this.setState({emailError: this.state.email ? 'Invalid Email Address' : 'E-mail address is required'})
    }
  }

  handleTextFieldChange(e, name) {
    const value = e.target.value.trim();
    this.setState({[name]: value, [`${name}Error`]: null})
    if(!isLength(value, {min: 0, max: 64})) {
      this.setState({[`${name}Error`]: 'The text entered exceeds the maximum length'})
      this.props.handleTextFieldChange({[name]: null})
    } else {
      this.setState({[`${name}Error`]: null})
      this.props.handleTextFieldChange({[name]: value})
    }
  }

  validateName(name) {
    if(!this.state[name]) {
      this.setState({[`${name}Error`]: `${_.startCase(name)} is Required!`})
    }
  }

  handleTypeChange(e) {
    const value = e.target.value;
    this.props.handleTypeChange(value);
  }

  render() {
    const { pending } = this.props;
    return (
      <div id="create-client-invite-step">
        <p className="header">Add information</p>
        <TextField
          id="firstName"
          label={<span>First Name <span className="require-asterisk">*</span></span>}
          className="input"
          onChange={(e) => this.handleTextFieldChange(e, 'firstName')}
          onBlur={() => this.validateName('firstName')}
          helperText={this.state.firstNameError}
          FormHelperTextProps={{error: true}}
          margin="normal"
          disabled={pending}
        />
        <TextField
          id="lastName"
          label={<span>Last Name <span className="require-asterisk">*</span></span>}
          className="input"
          onChange={(e) => this.handleTextFieldChange(e, 'lastName')}
          onBlur={() => this.validateName('lastName')}
          helperText={this.state.lastNameError}
          FormHelperTextProps={{error: true}}
          margin="normal"
          disabled={pending}
        />
        <br/>
        <TextField
          id="email"
          label={<span>E-mail <span className="require-asterisk">*</span></span>}
          className="input"
          onChange={(e) => this.handleEmailChange(e)}
          onBlur={() => this.validateEmail()}
          error={this.state.invalidEmail}
          margin="normal"
          helperText={this.state.emailError}
          FormHelperTextProps={{error: true}}
          disabled={pending}
        />
        <FormControl className="input">
          <InputLabel htmlFor="type">Type</InputLabel>
          <Select
            value={this.props.type}
            onChange={(e) => this.handleTypeChange(e)}
            input={<Input id="type"/>}
            disabled={pending}
          >
            {this.props.clientTypes.map((t, i) => {
              return <MenuItem key={i} value={t}>{t}</MenuItem>
            })}
          </Select>
        </FormControl>
      </div>
    )
  }
}
