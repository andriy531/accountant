import React, { Component } from 'react'
import './firstStep.css'
import Radio, { RadioGroup } from 'material-ui/Radio';
import { FormControl, FormControlLabel } from 'material-ui/Form';

export default class CreateClientFirstStep extends Component {

  constructor(props){
  	super(props);
  	this.state = {
    };
  }

  handleMethodChange(e, value) {
    this.props.handleMethodChange(value)
  }

  render() {
    return (
      <div id="create-client-first-step">
        <p className="header">Choose method</p>
        <FormControl component="fieldset" required>
          <RadioGroup
            aria-label="method"
            name="method"
            value={this.props.method}
            onChange={this.handleMethodChange.bind(this)}
          >
            <FormControlLabel value="invite" control={<Radio className="radio-btn"/>} label="Invite by e-mail" />
            <FormControlLabel value="manualy" control={<Radio className="radio-btn"/>} label="Create manualy" />
          </RadioGroup>
        </FormControl>
      </div>
    )
  }
}
