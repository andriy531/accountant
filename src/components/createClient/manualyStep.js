import React, { Component } from 'react'
import './manualyStep.css'
import TextField from 'material-ui/TextField'
import isEmail from 'validator/lib/isEmail'
import isLength from 'validator/lib/isLength'
import _ from 'lodash'
import Input, { InputLabel } from 'material-ui/Input'
import { MenuItem } from 'material-ui/Menu'
import { FormControl } from 'material-ui/Form'
import Select from 'material-ui/Select'
import ExpandMoreIcon from 'material-ui-icons/ExpandMore'
import IconButton from 'material-ui/IconButton'
import FormHelperText from 'material-ui/Form/FormHelperText'
import MaskedInput from 'react-text-mask'
import Autocomplete from '../autocomplete';
import countries from '../../assets/countries.json'
import states from '../../assets/states.json'
import isURL from 'validator/lib/isURL'
import isPostalCode from 'validator/lib/isPostalCode'
import moment from 'moment'

export default class CreateClientManualyStep extends Component {
  constructor(props){
  	super(props);
  	this.state = {
      emailError: null,
      email: this.props.clientToEdit && !this.props.duplicateClient ? this.props.clientToEdit.email : '',
      firstNameError: null,
      lastNameError: null,
      firstName: this.props.clientToEdit ? this.props.clientToEdit.firstName : '',
      lastName: this.props.clientToEdit ? this.props.clientToEdit.lastName : '',
      expanded: false,
      phone: this.props.clientToEdit ? this.props.clientToEdit.phone : ''
    };
  }

  handleEmailChange(e) {
    const email = e.target.value.trim();
    this.setState({email: email, emailError: null})
    if(email && isEmail(email)) {
      this.props.handleEmailChange(email);
    } else {
      this.props.handleEmailChange(null);
    }
  }

  validateEmail() {
    if(this.state.email && isEmail(this.state.email)) {
      this.setState({emailError: null})
    } else {
      this.setState({emailError: this.state.email ? 'Invalid Email Address' : 'E-mail address is required'})
    }
  }

  handleTextFieldChange(e, name) {
    const value = e.target.value.trim();
    this.setState({[name]: value, [`${name}Error`]: null})
    if(!isLength(value, {min: 0, max: 64})) {
      this.setState({[`${name}Error`]: 'The text entered exceeds the maximum length'})
      this.props.handleTextFieldChange({[name]: null})
      this.props.changeFormValid(false)
    } else {
      this.setState({[`${name}Error`]: null})
      this.props.handleTextFieldChange({[name]: value})
      this.props.changeFormValid(true)
    }
  }

  validateName(name) {
    if(!this.state[name]) {
      this.setState({[`${name}Error`]: `${_.startCase(name)} is Required!`})
    }
  }

  handleTypeChange(e) {
    const value = e.target.value;
    this.props.handleTypeChange(value);
  }

  handleToggleExpand() {
    this.setState({expanded: !this.state.expanded})
  }

  handlePhoneChange(e) {
    this.props.changeFormValid(true)
    if(/\d/.test(e.target.value)) {
      this.setState({phone: e.target.value, phoneError: null})
    } else {
      this.setState({phone: null, phoneError: null})
      this.props.handlePhoneChange(null);
    }

    if(e.target.value && e.target.value.replace(/\D/g, '').length === 10) {
      this.props.handlePhoneChange(e.target.value);
    } else {
      this.props.handlePhoneChange(null);
    }
  }

  onPhoneBlur() {
    this.setState({phoneFocused: false})
    if(this.state.phone && this.state.phone.replace(/\D/g, '').length !== 10) {
      this.setState({phoneError: 'Not full number entered'})
      this.props.changeFormValid(false)
    }
  }

  handleWebsiteChange(e) {
    const website = e.target.value.trim();
    this.setState({website: website})
    if(!isLength(website, {min: 0, max: 64})) {
      this.setState({websiteError: 'The text entered exceeds the maximum length'})
      this.props.handleWebsiteChange(null)
      this.props.changeFormValid(false)
    } else {
      this.setState({websiteError: null})
      this.props.handleWebsiteChange(website)
      this.props.changeFormValid(true)
    }
  }

  onWebsiteBlur() {
    if(!!this.state.website && !isURL(this.state.website)) {
      this.setState({websiteError: 'The website URL is not valid'})
      this.props.handleWebsiteChange(null)
      this.props.changeFormValid(false)
    }
  }

  handleAutocompleteChange(value, name) {
    const data = value ? value.trim() : value;
    if(isLength(data, {min: 0, max: 64})) {
      this.setState({[`${name}Error`]: null})
      this.props.handleTextFieldChange({[name]: data})
      this.props.changeFormValid(true)
    } else {
      this.setState({[`${name}Error`]: 'The text entered exceeds the maximum length'})
      this.props.handleTextFieldChange({[name]: null})
      this.props.changeFormValid(false)
    }
  }

  handleZipCodeChange(e) {
    const ZipCode = e.target.value.trim();
    this.setState({zipCode: ZipCode})
    this.props.changeFormValid(true)
    if(ZipCode && isPostalCode(ZipCode, ['US'])) {
      this.setState({zipCodeError: null})
      this.props.handleZipCodeChange(ZipCode)
    }
  }

  onZipCodeBlur() {
    if(this.state.zipCode && isPostalCode(this.state.zipCode, ['US'])) {
      this.setState({zipCodeError: null})
      this.props.changeFormValid(true)
    } else {
      this.setState({zipCodeError: this.state.zipCode ? 'The zip code is not valid' : null})
      this.props.handleZipCodeChange(null)
      if(this.state.zipCode) { this.props.changeFormValid(false) }
    }
  }

  render() {
    const { pending, clientToEdit, duplicateClient } = this.props;
    const { expanded } = this.state;
    return (
      <div id="create-client-manualy-step">
        <p className="header">Add information</p>
        <TextField
          id="firstName"
          label={<span>First Name <span className="require-asterisk">*</span></span>}
          className="input"
          onChange={(e) => this.handleTextFieldChange(e, 'firstName')}
          onBlur={() => this.validateName('firstName')}
          helperText={this.state.firstNameError}
          FormHelperTextProps={{error: true}}
          margin="normal"
          disabled={pending}
          defaultValue={clientToEdit ? clientToEdit.firstName : ''}
        />
        <TextField
          id="lastName"
          label={<span>Last Name <span className="require-asterisk">*</span></span>}
          className="input"
          onChange={(e) => this.handleTextFieldChange(e, 'lastName')}
          onBlur={() => this.validateName('lastName')}
          helperText={this.state.lastNameError}
          FormHelperTextProps={{error: true}}
          margin="normal"
          disabled={pending}
          defaultValue={clientToEdit ? clientToEdit.lastName : ''}
        />
        <br/>
        <TextField
          id="email"
          label={<span>E-mail <span className="require-asterisk">*</span></span>}
          className="input"
          onChange={(e) => this.handleEmailChange(e)}
          onBlur={() => this.validateEmail()}
          error={this.state.invalidEmail}
          margin="normal"
          helperText={this.state.emailError}
          FormHelperTextProps={{error: true}}
          disabled={pending}
          defaultValue={clientToEdit && !duplicateClient ? clientToEdit.email : ''}
        />
        <FormControl className="input">
          <InputLabel htmlFor="type">Type</InputLabel>
          <Select
            value={this.props.type}
            onChange={(e) => this.handleTypeChange(e)}
            input={<Input id="type" />}
            disabled={pending}
          >
            {this.props.clientTypes.map((t, i) => {
              return <MenuItem key={i} value={t}>{t}</MenuItem>
            })}
          </Select>
        </FormControl>
        <div className="add-more-info">
          <p onClick={() => this.handleToggleExpand()}>Add more information</p>
          <IconButton
            className={expanded ? 'expand expandOpen': 'expand'}
            onClick={() => this.handleToggleExpand()}
            aria-label="Show more"
          >
              <ExpandMoreIcon/>
          </IconButton>
        </div>
        <div style={{display: (expanded ? 'block' : 'none')}}>
          <FormControl className="input">
            <InputLabel htmlFor="phone" className="label" shrink={this.state.phoneFocused || !!this.state.phone}>
              Phone Number
            </InputLabel>
            <MaskedInput
              id="phone"
              className="masked-input"
              placeholder={(this.state.phoneFocused || !!this.state.phone) ? "Enter a phone number" : ""}
              guide={true} mask={['(', /[0-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
              onChange={(e) => this.handlePhoneChange(e)}
              onFocus={() => this.setState({phoneFocused: true})}
              onBlur={() => this.onPhoneBlur()}
              disabled={pending}
              value={this.state.phone}
            />
            {this.state.phoneError && <FormHelperText error>{this.state.phoneError}</FormHelperText>}
          </FormControl>
          <TextField
            id="site"
            label={<span>Website</span>}
            className="input"
            onChange={(e) => this.handleWebsiteChange(e)}
            error={!!this.state.websiteError}
            margin="normal"
            helperText={this.state.websiteError}
            FormHelperTextProps={{error: true}}
            disabled={pending}
            onBlur={() => this.onWebsiteBlur()}
            placeholder="Enter website"
            defaultValue={clientToEdit ? clientToEdit.website : ''}
          />
          <br/>
          <FormControl className="input">
            <Autocomplete
              suggestions={countries}
              handleChange={(value) => this.handleAutocompleteChange(value, 'clientCountry')}
              label={"Country"}
              placeholder="Enter client country"
              defaultValue={this.props.clientCountry}
              error={this.state.clientCountryError}
              disabled={pending}
            />
          </FormControl>
          <FormControl className="input state">
            <Autocomplete
              suggestions={ _.includes(["united states", "us", "usa"], (this.props.clientCountry || "").toLowerCase())  ? states : []}
              handleChange={(value) => this.handleAutocompleteChange(value, 'clientState')}
              label={"State/Province/Region"}
              placeholder="Enter client region"
              error={this.state.clientStateError}
              disabled={pending}
              defaultValue={clientToEdit ? clientToEdit.clientState : ''}
            />
          </FormControl>
          <br/>
          <TextField
            id="city"
            label={<span>City</span>}
            className="input"
            onChange={(e) => this.handleTextFieldChange(e, 'clientCity')}
            helperText={this.state.clientCityError}
            FormHelperTextProps={{error: true}}
            margin="normal"
            disabled={pending}
            defaultValue={clientToEdit ? clientToEdit.clientCity : ''}
            placeholder="Enter client city"
          />
          <TextField
            id="zip"
            label={<span>ZIP</span>}
            className="input"
            onChange={(e) => this.handleZipCodeChange(e)}
            onBlur={() => this.onZipCodeBlur()}
            helperText={this.state.zipCodeError}
            FormHelperTextProps={{error: true}}
            margin="normal"
            disabled={pending}
            type="number"
            defaultValue={clientToEdit ? clientToEdit.zipCode + '' : ''}
            placeholder="Enter zip code"
          />
          <br/>
          <TextField
            id="address"
            label={<span>Address</span>}
            className="input address"
            onChange={(e) => this.handleTextFieldChange(e, 'address')}
            helperText={this.state.addressError}
            FormHelperTextProps={{error: true}}
            margin="normal"
            disabled={pending}
            defaultValue={clientToEdit ? clientToEdit.address : ''}
            placeholder="Enter address"
          />
          <br/>
          <TextField
            id="tax"
            label={<span>Tax number</span>}
            className="input"
            onChange={(e) => this.handleTextFieldChange(e, 'taxNumber')}
            helperText={this.state.taxNumberError}
            FormHelperTextProps={{error: true}}
            margin="normal"
            disabled={pending}
            type="number"
            defaultValue={clientToEdit ? clientToEdit.taxNumber : ''}
            placeholder="Enter tax number"
          />
          <TextField
            id="fiscalYearEnd"
            label="Fiscal year end"
            type="date"
            className="input"
            onChange={(e) => this.handleTextFieldChange(e, 'fiscalEndYear')}
            InputLabelProps={{
              shrink: true,
            }}
            disabled={pending}
            defaultValue={clientToEdit ? moment(clientToEdit.fiscalEndYear).format("YYYY-MM-DD") : ''}
          />
        </div>
      </div>
    )
  }
}
