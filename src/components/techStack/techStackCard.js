import React, { Component } from 'react'
import Card, { CardContent, CardActions } from 'material-ui/Card'
import Collapse from 'material-ui/transitions/Collapse'
import IconButton from 'material-ui/IconButton'
import ExpandMoreIcon from 'material-ui-icons/ExpandMore'
import './techStackCard.css'
import DeleteIcon from 'material-ui-icons/Delete'


class ThechStackCard extends Component {
  constructor(props){
  	super(props);
  	this.state = {
      open: false
    };
  }

  handleExpandClick() {
    this.setState({open: !this.state.open})
  }
  render() {
    return (
      <Card className="tech-card">
        <CardContent>
            <p className="pale">{this.props.name}</p>
            <div onClick={() => window.open(this.props.url, '_blank')}
               className="tech-image" style={{
              backgroundImage: `url(${this.props.logo})`
            }} alt={this.props.name}/>
        </CardContent>
        {!this.props.disableActions && <CardActions disableActionSpacing className="card-actions">
          <IconButton
            className="icon-btn"
            onClick={() => this.handleExpandClick()}
            aria-expanded={this.state.open}
            aria-label="Show more"
          >
            <ExpandMoreIcon className={this.state.open ? 'expand expandOpen': 'expand'}/>
          </IconButton>
        </CardActions>}
        <Collapse in={this.state.open} transitionDuration="auto" unmountOnExit>
          <CardContent className="card-content">
            <div className="actions-content">
              <div className="action">
                <p>Delete: </p>
                <IconButton className="delete-btn">
                  <DeleteIcon className="delete-icon" onClick={() => this.props.handleRemoveTechStack(this.props.index)}/>
                </IconButton>
              </div>
            </div>
          </CardContent>
        </Collapse>
    </Card>
    )
  }
}

export default ThechStackCard
