import React, { Component } from 'react'
import './createTechStack.css'
import Button from 'material-ui/Button'
import Dialog from 'material-ui/Dialog'
import Dropzone from 'react-dropzone'
import CloudUpload from 'material-ui-icons/CloudUpload'
import TextField from 'material-ui/TextField'


class CreateTechStack extends Component {
  constructor(props){
  	super(props);
  	this.state = {
      //openTechStack: false,
      stackLogo: null,
      techStackName: null,
      techStackUrl: null
    };
  }

  handleCloseStackDialog() {
    this.setState({
      //openTechStack: false,
      stackLogo: null,
      techStackName: null,
      techStackUrl: null,
      requireTechStackName: false,
      requireTechStackUrl: false,
      requireTechStackLogo: false
    })
    this.props.handleClose();
  }

  onDrop(acceptedFiles, rejectedFiles) {
    if (rejectedFiles.length) {
      if(!rejectedFiles[0].type.includes('image')) {
        alert('You can upload only *.jpeg and *.png files');
      } else {
        alert('You can`t attach file more than 200 Mb. File size is ' + Math.round(rejectedFiles[0].size / 1048576 * 10) / 10 + ' Mb');
      }
    } else {
      this.setState({ stackLogo: acceptedFiles[0], requireTechStackLogo: false })
    }
  }

  handleAddStack(logo, name, url) {
    //let { stackLogo, techStackName, techStackUrl } = this.state;
    let requiredFields = this.checkRequiredFields();
    if(!requiredFields.techStackName && !requiredFields.techStackUrl && !requiredFields.stackLogo) {
      // this.props.handleAddTechStack(stackLogo, techStackName, techStackUrl);
      // this.handleCloseStackDialog();
    }
  }

  handleTechStackNameChange(e) {
    this.setState({techStackName: e.target.value, requireTechStackName: false})
  }

  handleTechStackUrlChange(e) {
    this.setState({techStackUrl: e.target.value, requireTechStackUrl: false})
  }

  checkRequiredFields() {
    let requiredFields = {
      techStackName: true,
      techStackUrl: true,
      stackLogo: true
    }
    if(this.state.techStackName) {
      requiredFields.techStackName = false;
    } else {
       this.setState({requireTechStackName: true})
    }
    if (this.state.techStackUrl) {
      requiredFields.techStackUrl = false;
    } else {
      this.setState({requireTechStackUrl: true})
    }
    if (this.state.stackLogo) {
      requiredFields.stackLogo = false;
    } else {
      this.setState({requireTechStackLogo: true})
    }
    return requiredFields;
  }

  render() {
    return (
      <Dialog open={this.props.open} onRequestClose={() => this.handleCloseStackDialog()}>
        <div className="new-stack-dialog">
          <div className="new-stack-header">
            <h3>Add a New Tech Stack</h3>
            <p>Download the logo of new tech stack and add a link to the website</p>
          </div>
          <div className="new-stack-form">
            <p className="label">Upload Company Logo</p>
            <Dropzone
              style={{ backgroundImage: `url(${this.state.stackLogo ? this.state.stackLogo.preview : undefined})` }}
              className="upload-view"
              onDrop={this.onDrop.bind(this)}
              maxSize={1048576 * 200}
              multiple={false}
              ref={node => { this.dropzoneRef = node }}
              accept="image/jpeg, image/png"
            >
              {!this.state.stackLogo && <CloudUpload className="camera-icon"/>}
            </Dropzone>
            {this.state.requireTechStackLogo && <p className="logo-required">Company Logo is required</p>}
            <TextField
              id="stack name"
              label="Tech stack name"
              margin="normal"
              fullWidth
              placeholder="Enter the tech stack name"
              onChange={(e) => this.handleTechStackNameChange(e)}
              helperText={this.state.requireTechStackName ? "Tech stack name is required" : ""}
              helperTextClassName="helper-text"
            />
            <TextField
              id="web-site"
              label="Past the website link"
              margin="normal"
              fullWidth
              placeholder="Enter the website link"
              onChange={(e) => this.handleTechStackUrlChange(e)}
              helperText={this.state.requireTechStackUrl ? "The website link is required" : ""}
              helperTextClassName="helper-text"
            />
            <Button
              raised
              className="btn green-btn new-stack-btn"
              onClick={() => this.handleAddStack()}
            >
              Add new stack
            </Button>
          </div>
        </div>
      </Dialog>
    )
  }
}

export default CreateTechStack
