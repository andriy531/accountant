import React, { Component } from 'react'
import './selectTechStack.css'
import Button from 'material-ui/Button'
import Dialog from 'material-ui/Dialog'
import Select from 'react-select'
import ThechStackCard from './techStackCard'

class GravatarOption extends Component {
  handleMouseDown (event) {
		event.preventDefault();
		event.stopPropagation();
		this.props.onSelect(this.props.option, event);
	}
	handleMouseEnter (event) {
		this.props.onFocus(this.props.option, event);
	}
	handleMouseMove (event) {
		if (this.props.isFocused) return;
		this.props.onFocus(this.props.option, event);
	}
  render () {
    const { option } = this.props;
    return (
      <div
        onMouseDown={this.handleMouseDown.bind(this)}
				onMouseEnter={this.handleMouseEnter.bind(this)}
				onMouseMove={this.handleMouseMove.bind(this)}
        className="option"
      >
        <div alt="logo" className="option-img" style={{backgroundImage: `url(${option.logo})`}}/>
        {option.name}
      </div>
    )
  }
}

const GravatarValue = () => ({
	render () {
		return (
			<div className="Select-value" title={this.props.value.name}>
				<span className="Select-value-label">
					{this.props.value.name}
				</span>
			</div>
		);
	}
});


class SelectTechStack extends Component {
  constructor(props){
  	super(props);
  	this.state = {
      techStack: null
    };
  }

  handleAddStack() {
    let requiredFields = this.checkRequiredFields();
    if(!requiredFields.techStack) {
      this.props.handleAddStack(this.state.techStack)
      this.handleCloseStackDialog()
    }
  }

  handleCloseStackDialog() {
    this.setState({techStack: null, requireTechStack: false})
    this.props.handleClose();
  }

  setValue (value) {
		this.setState({ techStack: value, requireTechStack: false});
	}

  checkRequiredFields() {
    let requiredFields = {
      techStack: true,
    }
    if(this.state.techStack) {
      requiredFields.techStack = false;
    } else {
       this.setState({requireTechStack: true})
    }
    return requiredFields;
  }

  render() {
    return (
      <Dialog open={this.props.open} onRequestClose={() => this.handleCloseStackDialog()}>
        <div className="select-stack-dialog">
          <div className="select-stack-header">
            <h3>Choose a Tech Stack</h3>
            <p>Choose one of existed tech stacks</p>
          </div>
          <div className="select-stack-form">
            <div className="select-wrapper">
              <p className="select-label">Choose tech stack</p>
              <Select
                name="techstack"
                className="select-field"
                placeholder="Select tech stack"
                options={this.props.options}
                optionComponent={GravatarOption}
                onChange={(value) => this.setValue(value)}
                value={this.state.techStack}
                valueComponent={GravatarValue}
              />
            </div>
            {this.state.requireTechStack && <p className="stack-required">Stack is required</p>}
            {this.state.techStack && <ThechStackCard
              logo={this.state.techStack.logo}
              name={this.state.techStack.name}
              url={this.state.techStack.url}
              disableActions={true}
            />}
            <Button
              raised
              className="btn green-btn select-stack-btn"
              onClick={() => this.handleAddStack()}
            >
              Confirm
            </Button>
          </div>
        </div>
      </Dialog>
    )
  }
}

export default SelectTechStack
