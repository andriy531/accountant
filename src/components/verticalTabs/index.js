import React, { Component } from 'react'
import './index.css'
import ButtonBase from 'material-ui/ButtonBase'
import Hidden from 'material-ui/Hidden'

export default class VerticalTabs extends Component {
  render() {
    const { tabValue } = this.props;
    return (
      <div className="vertical-tabs">
        {this.props.tabs.map((t, i) =>
          <ButtonBase
            focusRipple
            key={i}
            className={(tabValue === t.id) ? 'menu-item active' : 'menu-item'}
            onClick={() => {this.props.handleTabChange(t.id)}}
          >
            <Hidden xsDown><p>{t.label}</p></Hidden>
            <Hidden smUp>{t.icon}</Hidden>
          </ButtonBase>
        )}
      </div>
    )
  }
}
