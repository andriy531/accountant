import React, { Component } from 'react'
import './index.css'
import Button from 'material-ui/Button'
import AddIcon from 'material-ui-icons/Add'
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup'
import FaList from 'react-icons/lib/fa/list'
import FaComments from 'react-icons/lib/fa/comments'
import FaBriefcase from 'react-icons/lib/fa/briefcase'
import FaCreditCardAlt from 'react-icons/lib/fa/credit-card-alt'
import FaUserPlus from 'react-icons/lib/fa/user-plus'

export default class Fab extends Component {

  render() {
    return (
      <div id="fab-container" onClick={() => this.props.toggleFab()}>
        <div className="fabs-block">
          <CSSTransitionGroup
            transitionName="small-fabs"
            transitionAppear={true}
            transitionAppearTimeout={500}
            transitionEnter={false}
            transitionLeave={false}
            >
            <div className="small-fabs">
              <div className="small-fab-item">
                <Button fab aria-label="add" className="small-btn" onClick={() => this.props.openCreateClientModal()}>
                  <FaUserPlus />
                </Button>
                <p className="fab-text" onClick={() => this.props.openCreateClientModal()}>New Client</p>
              </div>
              <div className="small-fab-item">
                <Button fab aria-label="add" className="small-btn" onClick={() => this.props.openCreateServiceModal()}>
                  <FaList />
                </Button>
                <p className="fab-text" onClick={() => this.props.openCreateServiceModal()}>New Service</p>
              </div>
              <div className="small-fab-item">
                <Button fab aria-label="add" className="small-btn">
                  <FaComments />
                </Button>
                <p className="fab-text">New Message</p>
              </div>
              <div className="small-fab-item">
                <Button fab aria-label="add" className="small-btn" onClick={() => this.props.push('/engagements/create')}>
                  <FaBriefcase />
                </Button>
                <p className="fab-text" onClick={() => this.props.push('/engagements/create')}>New Engagement</p>
              </div>
              <div className="small-fab-item">
                <Button fab aria-label="add" className="small-btn">
                  <FaCreditCardAlt />
                </Button>
                <p className="fab-text">New Payment</p>
              </div>
            </div>
          </CSSTransitionGroup>
          <Button
            fab
            aria-label="add"
            className="main-btn-opened"
            onClick={() => this.props.toggleFab()}
            >
              <CSSTransitionGroup
                transitionName="main-btn-icon"
                transitionAppear={true}
                transitionAppearTimeout={500}
                transitionEnter={false}
                transitionLeave={false}
                >
                <AddIcon />
              </CSSTransitionGroup>
          </Button>
        </div>
      </div>
    )
  }
}
