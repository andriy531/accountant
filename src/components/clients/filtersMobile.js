import React, { Component } from 'react'
import './filtersMobile.css'
import Button from 'material-ui/Button'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import { FormControl } from 'material-ui/Form'
import Input, { InputLabel } from 'material-ui/Input'
import Select from 'material-ui/Select'
import TextField from 'material-ui/TextField'
import { MenuItem } from 'material-ui/Menu'
import Radio, { RadioGroup } from 'material-ui/Radio'
import { FormControlLabel } from 'material-ui/Form'
import moment from 'moment'
import Search from '../searchField'
import { getClientSearchAutosuggestApi } from '../../services/api'
import ExpandMoreIcon from 'material-ui-icons/ExpandMore'
import IconButton from 'material-ui/IconButton'
import Close from 'material-ui-icons/Close'
import _ from 'lodash'


class CustomInput extends Component {
  render() {
    return (
      <TextField
        placeholder={this.props.placeholder}
        onClick={this.props.onClick}
        onChange={this.props.onChange}
        value={this.props.value}
        error={this.props.error}
        InputProps={{
          readOnly: true
        }}
      />
    )
  }
}

export default class ClientsFiltersMobile extends Component {

  constructor(props) {
  	super(props);
  	this.state = {
      expandedId: null,
      dateFilterType: 'date-range',
      fiscalEndYearType: 'date-range',
      engagementsType: 'number-range',
      searchBy: this.props.filters && this.props.filters.searchBy ? this.props.filters.searchBy : 'name',
      filters: this.props.filters || {
        sortByRegistration: 'desc',
        sortByName: '',
        sortByFiscalEnd: '',
        filterByType: '',
        filterByStatus: '',
        fromRegistration: '',
        toRegistration: '',
        fromFiscalEnd: '',
        toFiscalEnd: '',
        fromEngagements: '',
        toEngagements: '',
        searchQuery: '',
        searchBy: ''
      },
      sortedBy: '',
      filteredBy: ''
    };
  }

  handleChangeFilterType(e, prop) {
    let updatedFilters = this.state.filters;
    if(prop === 'dateFilterType') {
      updatedFilters = {
        ...updatedFilters,
        fromRegistration: '',
        toRegistration: ''
      }
    }
    if(prop === 'fiscalEndYearType') {
      updatedFilters = {
        ...updatedFilters,
        fromFiscalEnd: '',
        toFiscalEnd: ''
      }
    }
    if(prop === 'engagementsType') {
      updatedFilters = {
        ...updatedFilters,
        fromEngagements: '',
        toEngagements: ''
      }
    }
    this.setState({[prop]: e.target.value, filters: updatedFilters})
  }

  onChangeSearchBy(e) {
    this.setState({searchBy: e.target.value, filters: {
      ...this.state.filters,
      searchBy: this.state.filters.searchQuery ? e.target.value : ''
    }})
  }

  onChangeFilter(data, notFilterImmediately) {
    let filters = { ...this.state.filters, ...data };
    if(Object.keys(data)[0] === 'sortByName') {
      filters.sortByRegistration = '';
      filters.sortByFiscalEnd = '';
    } else if(Object.keys(data)[0] === 'sortByRegistration') {
      filters.sortByName = '';
      filters.sortByFiscalEnd = '';
    } else if(Object.keys(data)[0] === 'sortByFiscalEnd') {
      filters.sortByName = '';
      filters.sortByRegistration = '';
    }

    this.setState({filters})
  }

  handleFilter(filters) {
    this.props.handleFilter(filters);
    this.props.toggleFiltersView();
    this.filteredByStringBuilder(filters);
  }

  resetFilter() {
    const filters = {
      sortByRegistration: 'desc',
      sortByName: '',
      sortByFiscalEnd: '',
      filterByType: '',
      filterByStatus: '',
      fromRegistration: '',
      toRegistration: '',
      fromFiscalEnd: '',
      toFiscalEnd: '',
      fromEngagements: '',
      toEngagements: '',
      searchQuery: '',
      searchBy: ''
    }
    this.setState({ filters })
    this.props.resetFilter();
    this.props.toggleFiltersView();
  }

  fetchSuggestion(value) {
    const { searchBy } = this.state;
    return getClientSearchAutosuggestApi(searchBy, value);
  }

  cancelOnlyFilters() {
    let filters = {
      ...this.state.filters,
      filterByType: '',
      filterByStatus: '',
      fromRegistration: '',
      toRegistration: '',
      fromFiscalEnd: '',
      toFiscalEnd: '',
      fromEngagements: '',
      toEngagements: ''
    }
    this.props.handleFilter(filters);
    this.props.toggleFiltersView();
  }

  handleToggleExpand(expandedId) {
    this.setState({expandedId: expandedId === this.state.expandedId ? null : expandedId})
  }

  filteredByStringBuilder(filters) {
    let sortedBy = '';
    let filteredBy = '';
    sortedBy += filters.sortByFiscalEnd && `fiscal year end (${filters.sortByFiscalEnd})`;
    sortedBy += filters.sortByName && `${sortedBy.length ? ', ' : ' '} name (${filters.sortByName})`;
    sortedBy += filters.sortByRegistration && `${sortedBy.length ? ', ' : ' '} registration (${filters.sortByRegistration})`;

    filteredBy += filters.filterByType && `type (${filters.filterByType})`;
    filteredBy += filters.filterByStatus && `${filteredBy.length ? ', ' : ' '} status (${filters.filterByStatus})`;
    if(filters.fromRegistration && filters.toRegistration) {
      if(filters.fromRegistration !== filters.toRegistration) {
        filteredBy += `${filteredBy.length ? ', ' : ' '} date range (${filters.fromRegistration} - ${filters.toRegistration})`;
      } else {
        filteredBy += `${filteredBy.length ? ', ' : ' '} date (${filters.fromRegistration})`;
      }
    }
    if(filters.fromEngagements && filters.toEngagements) {
      if(filters.fromEngagements !== filters.toEngagements) {
        filteredBy += `${filteredBy.length ? ', ' : ' '} engagements range (${filters.fromEngagements} - ${filters.toEngagements})`;
      } else {
        filteredBy += `${filteredBy.length ? ', ' : ' '} engagements (${filters.fromEngagements})`;
      }
    }
    if(filters.fromFiscalEnd && filters.toFiscalEnd) {
      if(filters.fromFiscalEnd !== filters.toFiscalEnd) {
        filteredBy += `${filteredBy.length ? ', ' : ' '} fiscal year end range (${filters.fromFiscalEnd} - ${filters.toFiscalEnd})`;
      } else {
        filteredBy += `${filteredBy.length ? ', ' : ' '} fiscal year end (${filters.fromFiscalEnd})`;
      }
    }
    this.setState({sortedBy, filteredBy})
  }

  componentDidMount() {
    this.filteredByStringBuilder(this.state.filters)
  }

  render() {
    const { sortByName, sortByRegistration, sortByFiscalEnd, filterByType, filterByStatus, fromRegistration, toRegistration, fromFiscalEnd, toFiscalEnd, fromEngagements, toEngagements } = this.state.filters;
    return (
      <div id="clients-filters-mobile">
        <div className="filter-view">
          <p
            className="filter-switch"
            onClick={() => this.props.toggleFiltersView()}
            >Filter services <Close className="filter-icon"/>
          </p>
        </div>
        <Button raised className="btn reset-btn" onClick={() => this.resetFilter()}>
          Reset filters
        </Button>
        <div className="filter-block">
          <FormControl className="form-control">
            <InputLabel htmlFor="searchBy">Search by</InputLabel>
            <Select
              value={this.state.searchBy}
              onChange={(e) => this.onChangeSearchBy(e)}
              input={<Input id="searchBy" />}
              className="select"
            >
              <MenuItem value={"name"}><span className="select-item">Client name</span></MenuItem>
              <MenuItem value={"company"}><span className="select-item">Company name</span></MenuItem>
              <MenuItem value={"tags"}><span className="select-item">Client tags</span></MenuItem>
            </Select>
          </FormControl>
          <FormControl className="form-control">
            <div className="search-view">
              <Search
                fetchSuggestion={this.fetchSuggestion.bind(this)}
                handleChange={(value) => this.onChangeFilter({searchBy: value ? this.state.searchBy : '', searchQuery: value}, true)}
                label={"Search"}
                placeholder={_.startCase(this.state.searchBy)}
                style={{fontSize: '16px', width: '100%'}}
                value={this.state.filters.searchQuery}
              />
            </div>
          </FormControl>
          <Button className="green-btn filter-btn" onClick={() => this.handleFilter(this.state.filters)}>
            Search
          </Button>
        </div>
        <div className="filter-line" onClick={() => this.handleToggleExpand('sort')}>
          <div className="filter-toggle">
            <p>Sort</p>
            <IconButton
              className={this.state.expandedId === 'sort' ? 'expand expandOpen': 'expand'}
              aria-label="Show more"
            >
                <ExpandMoreIcon/>
            </IconButton>
          </div>
          {this.state.expandedId !== 'sort' && <p className="filtered-by">{this.state.sortedBy && `Sorted by ${this.state.sortedBy}`}</p>}
        </div>
        {this.state.expandedId === 'sort' &&
          <div className="filter-block">
            <FormControl className="form-control">
              <InputLabel htmlFor="name">Service name</InputLabel>
              <Select
                value={sortByName}
                onChange={(e) => this.onChangeFilter({sortByName: e.target.value})}
                input={<Input id="name" />}
                className="select"
              >
                <MenuItem value=""><span className="select-item">None</span></MenuItem>
                <MenuItem value={"asc"}><span className="select-item">A...Z</span></MenuItem>
                <MenuItem value={"desc"}><span className="select-item">Z...A</span></MenuItem>
              </Select>
            </FormControl>
            <br/>
            <FormControl className="form-control">
              <InputLabel htmlFor="registration">Regitrations</InputLabel>
              <Select
                value={sortByRegistration}
                onChange={(e) => this.onChangeFilter({sortByRegistration: e.target.value})}
                input={<Input id="registration" />}
                className="select"
              >
                <MenuItem value=""><span className="select-item">None</span></MenuItem>
                <MenuItem value={"desc"}><span className="select-item">Recently</span></MenuItem>
                <MenuItem value={"asc"}><span className="select-item">Old</span></MenuItem>
              </Select>
            </FormControl>
            <br/>
            <FormControl className="form-control">
              <InputLabel htmlFor="date">Fiscal year end</InputLabel>
              <Select
                value={sortByFiscalEnd}
                onChange={(e) => this.onChangeFilter({sortByFiscalEnd: e.target.value})}
                input={<Input id="fiscalEndYear" />}
                className="select"
              >
                <MenuItem value=""><span className="select-item">None</span></MenuItem>
                <MenuItem value={"desc"}><span className="select-item">Recently</span></MenuItem>
                <MenuItem value={"asc"}><span className="select-item">Old</span></MenuItem>
              </Select>
            </FormControl>
            <Button className="green-btn filter-btn" onClick={() => {this.handleFilter(this.state.filters)}}>
              Sort
            </Button>
          </div>
        }
        <div className="filter-line" onClick={() => this.handleToggleExpand('filter')}>
          <div className="filter-toggle">
            <p>Filter</p>
            <IconButton
              className={this.state.expandedId === 'filter' ? 'expand expandOpen': 'expand'}
              aria-label="Show more"
            >
                <ExpandMoreIcon/>
            </IconButton>
          </div>
          {this.state.expandedId !== 'filter' && <p className="filtered-by">{this.state.filteredBy && `Filtered by ${this.state.filteredBy}`}</p>}
        </div>
        {this.state.expandedId === 'filter' &&
          <div className="filter-block">
            <FormControl className="form-control">
              <InputLabel htmlFor="type">Type</InputLabel>
              <Select
                value={filterByType}
                onChange={(e) => this.onChangeFilter({filterByType: e.target.value})}
                input={<Input id="type" />}
                className="select"
              >
                <MenuItem value=""><span className="select-item">None</span></MenuItem>
                <MenuItem value={"individual"}><span className="select-item">Individual</span></MenuItem>
                <MenuItem value={"corporate"}><span className="select-item">Corporate</span></MenuItem>
                <MenuItem value={"non-profit"}><span className="select-item">Non-profit</span></MenuItem>
              </Select>
            </FormControl>
            <br/>
            <FormControl className="form-control">
              <InputLabel htmlFor="status">Status</InputLabel>
              <Select
                value={filterByStatus}
                onChange={(e) => this.onChangeFilter({filterByStatus: e.target.value})}
                input={<Input id="status" />}
                className="select"
              >
                <MenuItem value=""><span className="select-item">None</span></MenuItem>
                <MenuItem value={"self-registered"}><span className="select-item">Self-registered</span></MenuItem>
                <MenuItem value={"invited"}><span className="select-item">Invited</span></MenuItem>
                <MenuItem value={"offline"}><span className="select-item">Offline</span></MenuItem>
              </Select>
            </FormControl>
            <FormControl className="date-range-form">
              <div className="controlled-label">
                <span>Registration: </span>
                <FormControl component="fieldset">
                  <RadioGroup
                    aria-label="date"
                    name="date"
                    className="radio-group"
                    value={this.state.dateFilterType}
                    onChange={(e) => this.handleChangeFilterType(e, 'dateFilterType')}
                  >
                    <FormControlLabel className="control-label" value="date" control={<Radio className="radio-btn"/>} label="Date" />
                    <FormControlLabel className="control-label" value="date-range" control={<Radio className="radio-btn"/>} label="Date range" />
                  </RadioGroup>
                </FormControl>
              </div>
              {this.state.dateFilterType === 'date-range' ? <div className="date-range-view">
                <DatePicker
                  selected={fromRegistration ? moment(fromRegistration, "MM-DD-YYYY") : ''}
                  selectsStart
                  startDate={fromRegistration ? moment(fromRegistration, "MM-DD-YYYY") : ''}
                  endDate={toRegistration ? moment(toRegistration, "MM-DD-YYYY") : ''}
                  onChange={(date) => this.onChangeFilter({fromRegistration: date ? moment(date).format('MM-DD-YYYY') : ''})}
                  placeholderText="Start date"
                  customInput={<CustomInput label="Start date"/>}
                />
                <DatePicker
                    selected={toRegistration ? moment(toRegistration, "MM-DD-YYYY") : ''}
                    selectsEnd
                    startDate={fromRegistration ? moment(fromRegistration, "MM-DD-YYYY") : ''}
                    endDate={toRegistration ? moment(toRegistration, "MM-DD-YYYY") : ''}
                    onChange={(date) => this.onChangeFilter({toRegistration: date ? moment(date).format('MM-DD-YYYY') : ''})}
                    placeholderText="End date"
                    customInput={<CustomInput label="End date"/>}
                />
              </div> : <div className="date-range-view"><DatePicker
                selected={fromRegistration ? moment(fromRegistration, "MM-DD-YYYY") : ''}
                onChange={(date) => {
                  let parsedDate = date ? moment(date).format('MM-DD-YYYY') : '';
                  this.onChangeFilter({fromRegistration: parsedDate, toRegistration: parsedDate})}
                }
                placeholderText="Date"
                customInput={<CustomInput label="Date"/>}
              /></div>}
            </FormControl>
            <FormControl className="date-range-form">
              <div className="controlled-label">
                <span>Fiscal year: </span>
                <FormControl component="fieldset">
                  <RadioGroup
                    aria-label="date"
                    name="fiscalEndYear"
                    className="radio-group"
                    value={this.state.fiscalEndYearType}
                    onChange={(e) => this.handleChangeFilterType(e, 'fiscalEndYearType')}
                  >
                    <FormControlLabel className="control-label" value="date" control={<Radio className="radio-btn"/>} label="Date" />
                    <FormControlLabel className="control-label" value="date-range" control={<Radio className="radio-btn"/>} label="Date range" />
                  </RadioGroup>
                </FormControl>
              </div>
              {this.state.fiscalEndYearType === 'date-range' ?
                <div className="date-range-view">
                  <DatePicker
                      selected={fromFiscalEnd ? moment(fromFiscalEnd, "MM-DD-YYYY") : ''}
                      selectsStart
                      startDate={fromFiscalEnd ? moment(fromFiscalEnd, "MM-DD-YYYY") : ''}
                      endDate={toFiscalEnd ? moment(toFiscalEnd, "MM-DD-YYYY") : ''}
                      onChange={(date) => this.onChangeFilter({fromFiscalEnd: date ? moment(date).format('MM-DD-YYYY') : ''})}
                      placeholderText="Start date"
                      customInput={<CustomInput label="Start date"/>}
                  />
                  <DatePicker
                      selected={toFiscalEnd ? moment(toFiscalEnd, "MM-DD-YYYY") : ''}
                      selectsEnd
                      startDate={fromFiscalEnd ? moment(fromFiscalEnd, "MM-DD-YYYY") : ''}
                      endDate={toFiscalEnd ? moment(toFiscalEnd, "MM-DD-YYYY") : ''}
                      onChange={(date) => this.onChangeFilter({toFiscalEnd: date ? moment(date).format('MM-DD-YYYY') : ''})}
                      placeholderText="End date"
                      customInput={<CustomInput label="End date"/>}
                  />
                </div> : <div className="date-range-view">
                  <DatePicker
                      selected={fromFiscalEnd ? moment(fromFiscalEnd, "MM-DD-YYYY") : ''}
                      onChange={(date) => {
                        let parsedDate = date ? moment(date).format('MM-DD-YYYY') : '';
                        this.onChangeFilter({fromFiscalEnd: parsedDate, toFiscalEnd: parsedDate})}
                      }
                      placeholderText="Date"
                      customInput={<CustomInput label="Date"/>}
                  />
                </div>
              }
            </FormControl>
            <FormControl className="form-control engagements-control">
              <div className="controlled-label">
                <span>Engagements: </span>
                <FormControl component="fieldset">
                  <RadioGroup
                    aria-label="engagements"
                    name="engagements"
                    className="radio-group"
                    value={this.state.engagementsType}
                    onChange={(e) => this.handleChangeFilterType(e, 'engagementsType')}
                  >
                    <FormControlLabel className="control-label" value="number" control={<Radio className="radio-btn"/>} label="Number" />
                    <FormControlLabel className="control-label" value="number-range" control={<Radio className="radio-btn"/>} label="Range" />
                  </RadioGroup>
                </FormControl>
              </div>
              {this.state.engagementsType === 'number-range' ?
                <div className="number-range">
                  <TextField
                    value={fromEngagements}
                    type="number"
                    placeholder="From"
                    className="number"
                    onChange={(e) => this.onChangeFilter({fromEngagements: e.target.value})}
                  />
                  <TextField
                    value={toEngagements}
                    type="number"
                    placeholder="To"
                    className="number"
                    onChange={(e) => this.onChangeFilter({toEngagements: e.target.value})}
                  />
                </div> : <div className="number-range">
                  <TextField
                    value={fromEngagements}
                    type="number"
                    placeholder="Number"
                    className="number"
                    onChange={(e) => this.onChangeFilter({fromEngagements: e.target.value, toEngagements: e.target.value})}
                  />
                </div>
              }
            </FormControl>
            <Button className="green-btn filter-btn" onClick={() => this.handleFilter(this.state.filters)}>
              Apply filters
            </Button>
            <Button raised className="btn cancel-btn" onClick={() => this.cancelOnlyFilters()}>
              Cancel
            </Button>
            <br/><br/>
          </div>
        }
      </div>
    )
  }
}
