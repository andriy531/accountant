import React, { Component } from 'react'
import './mobileList.css'
import FilterListIcon from 'material-ui-icons/FilterList'
import Button from 'material-ui/Button'
import ExpandMoreIcon from 'material-ui-icons/ExpandMore'
import IconButton from 'material-ui/IconButton'
import Collapse from 'material-ui/transitions/Collapse'
import Divider from 'material-ui/Divider'
import moment from 'moment'
import ReactPaginate from 'react-paginate'
import KeyboardArrowLeftIcon from 'material-ui-icons/KeyboardArrowLeft'
import KeyboardArrowRightIcon from 'material-ui-icons/KeyboardArrowRight'
import Avatar from 'react-avatar'
import ClientsFiltersMobile from './filtersMobile'


export default class ClientMobile extends Component {
  constructor(props){
  	super(props);
  	this.state = {
      expandedId: null
    };
  }

  handleToggleExpand(expandedId) {
    this.setState({expandedId: expandedId === this.state.expandedId ? null : expandedId})
  }

  toggleFiltersView() {
    this.setState({openFilters: !this.state.openFilters})
  }

  render() {
    const { openFilters } = this.state;
    return (
      <div id="clients-list-mobile">
        <div className="filter-view">
          <p className="filtered-by">{this.props.filteredBy && !openFilters && 'Filtered'}</p>
          <p
            className="filter-switch"
            onClick={() => this.toggleFiltersView()}
            >Filter clients <FilterListIcon className="filter-icon"/>
          </p>
        </div>
        { openFilters ? <div>
          <ClientsFiltersMobile
            handleFilter={this.props.handleFilter}
            resetFilter={this.props.resetFilter}
            toggleFiltersView={this.toggleFiltersView.bind(this)}
            filters={this.props.filters}
          />
        </div> : <div>
          {
            this.props.clients.map((c, i) => {
              return  (<div className="client-card" key={i}>
                <div className="info-view">
                  <div className="avatar-view">
                    <Avatar
                      size={50}
                      round
                      name={c.firstName + ' ' + c.lastName}
                      className="avatar"
                      colors={['#9da5b9', '#5bc2a8', '#50a993', '#7d68a3']}
                    />
                  </div>
                  <div className="client-info">
                    <div className="name-block">
                      <p>{c.firstName} {c.lastName}</p>
                      <IconButton
                        className={this.state.expandedId === c.client_id ? 'expand expandOpen': 'expand'}
                        onClick={() => this.handleToggleExpand(c.client_id)}
                        aria-label="Show more"
                      >
                          <ExpandMoreIcon/>
                      </IconButton>
                    </div>
                    <span className="client-date">{c.companyName || moment(c.registrationDate).format('DD/MM/YYYY')}</span>
                  </div>
                </div>
                <Collapse in={this.state.expandedId === c.client_id} transitionDuration="auto" unmountOnExit>
                  <Divider/>
                  <div className="collapsed-view">
                    <table className="info-table">
                      <tbody>
                        <tr>
                          <td>Type</td>
                          <td>{c.type}</td>
                        </tr>
                        <tr>
                          <td>Fiscal year end</td>
                          <td>{moment(c.fiscalEndYear).format('DD/MM/YYYY')}</td>
                        </tr>
                        <tr>
                          <td>Status</td>
                          <td>{c.status}</td>
                        </tr>
                        <tr>
                          <td>Registration date</td>
                          <td>{moment(c.registrationDate).format('DD/MM/YYYY')}</td>
                        </tr>
                        <tr>
                          <td>Engagements</td>
                          <td>{c.engagementsNumber}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div className="actions">
                    <Button
                      onClick={() => this.props.onEditClick(c)}
                    >
                        edit
                    </Button>
                    <Button
                      onClick={() => this.props.onCopyClick(c)}
                    >
                        copy
                    </Button>
                    {c.status === 'offline' &&
                      <Button
                        onClick={() => this.props.onInviteClick(c)}
                      >
                          invite
                      </Button>
                    }
                    <Button
                      onClick={() => this.props.onDeleteClick(c)}
                      disabled={!!c.engagementsNumber}
                    >
                        delete
                    </Button>
                  </div>
                  <Divider/>
                  <div className="actions view-more">
                    <Button
                      onClick={() => this.props.push(`clients/${c.client_id}`)}
                    >
                        View more
                    </Button>
                  </div>
                </Collapse>
              </div>)
            })
          }
          {
            !!this.props.clients.length && <ReactPaginate previousLabel={<IconButton aria-label="previeus"><KeyboardArrowLeftIcon/></IconButton>}
                       nextLabel={<IconButton aria-label="next"><KeyboardArrowRightIcon/></IconButton>}
                       breakLabel={<a href="">...</a>}
                       breakClassName={"break-me"}
                       pageCount={this.props.pageCount}
                       marginPagesDisplayed={1}
                       pageRangeDisplayed={2}
                       onPageChange={(page) => this.props.handlePageClick(page)}
                       containerClassName={"pagination"}
                       activeClassName={"active"}
                       disabledClassName={"disabled"}
                       forcePage={this.props.page_number - 1}
                     />
           }
         </div>
       }
      </div>
    )
  }
}
