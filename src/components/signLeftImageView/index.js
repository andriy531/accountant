import React, { Component } from 'react'
import Grid from 'material-ui/Grid';
import './index.css'
import Logo from '../logo';


export default class SignLeftImageView extends Component {

  render() {
    return (
      <Grid item md={4} hidden={{ smDown: true }} className="sign-left-side">
        <div className="dark-mask">
          <Logo/>
          { this.props.showText &&
            <div>
              <p className="quote">CountUp helps me build deeper relationships with my clients because I provide them with all the necessary financial information through my Web App.</p>
              <p className="quote">They can access this info, communicate, and pay me anytime from any device! This is truly the best and the easiest way to modernize your practice.</p>
              <p className="quote">Highly recommend!</p>
              <p className="quote-by-cpa">Charles Browning CPA</p>
            </div>
          }
        </div>
      </Grid>
    )
  }
}
