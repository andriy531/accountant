import React, { Component } from 'react';
import './index.css';
import AvatarEditor from 'react-avatar-editor'
import Button from 'material-ui/Button';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogTitle,
} from 'material-ui/Dialog';


export default class ImageEditor extends Component {

  constructor(props){
  	super(props);
  	this.state = {
      scale: 1
    };
  }

  handleScale = e => {
    const scale = parseFloat(e.target.value)
    this.setState({ scale })
  }

  render() {
    return (
      <Dialog open={this.props.open} onRequestClose={this.props.handleRequestCancel} className="avatar-editor-dialog">
        <DialogTitle className="title">{"CountUp Avatar Editor"}</DialogTitle>
        <DialogContent>
          <div className="avatar-editor">
            <AvatarEditor
              ref={this.props.setEditorRef}
              disableDrop={true}
              image={this.props.tmpFile}
              width={140}
              height={140}
              border={20}
              color={[0, 0, 0, 0.54]}
              scale={parseFloat(this.state.scale)}
              rotate={0}
            />
            <div className="zoom-control">
              <p>Zoom:</p>
              <input
                name='scale'
                type='range'
                onChange={this.handleScale}
                min='1'
                max='3'
                step='0.01'
                defaultValue='1'
              />
            </div>
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.handleRequestCancel}>
            Cancel
          </Button>
          <Button onClick={this.props.handleRequestAgree} className="crop-btn">
            Crop
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}
