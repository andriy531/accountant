import React, { Component } from 'react'
import Autosuggest from 'react-autosuggest'
import { MenuItem } from 'material-ui/Menu'
import match from 'autosuggest-highlight/match'
import parse from 'autosuggest-highlight/parse'
import TextField from 'material-ui/TextField'
import Paper from 'material-ui/Paper'
import FormHelperText from 'material-ui/Form/FormHelperText'
import { withStyles } from 'material-ui/styles'
import SearchIcon from 'material-ui-icons/Search'
import './index.css'

// https://developer.mozilla.org/en/docs/Web/JavaScript/Guide/Regular_Expressions#Using_Special_Characters
function escapeRegexCharacters(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

function getSuggestions(value, suggestions) {
  const escapedValue = escapeRegexCharacters(value.trim());
  const regex = new RegExp('^' + escapedValue, 'i');

  return suggestions.filter(data => regex.test(data));
}

function getSuggestionValue(suggestion) {
  return suggestion;
}

function renderSuggestion(suggestion, { query, isHighlighted }) {
  const matches = match(suggestion, query);
  const parts = parse(suggestion, matches);

  return (
    <MenuItem selected={isHighlighted} component="div" style={{height: '10px'}}>
      <div style={{overflow: 'hidden', textOverflow: 'ellipsis', fontSize: '14px'}}>
        {parts.map((part, index) => {
          return part.highlight ? (
            <span key={index} style={{fontFamily: 'SF-Display-Thin, sans-serif'}}>
              {part.text}
            </span>
          ) : (
            <strong key={index} style={{ fontFamily: 'SF-Display-Regular, sans-serif' }}>
              {part.text}
            </strong>
          );
        })}
      </div>
    </MenuItem>
  );
}

function renderInput(inputProps) {
  const { autoFocus, value, ref, label, isRequired, ...other } = inputProps;

  const fieldLabel = <span>{label} {isRequired && <span className="require-asterisk">*</span>}</span>

  return (
    <div className="search-view">
      <SearchIcon className="search-icon"/>
      <TextField
        className="seach-field"
        fullWidth
        autoFocus={autoFocus}
        value={value}
        inputRef={ref}
        label={fieldLabel}
        type="search"
        InputLabelProps={{
          shrink: true,
        }}
        InputProps={{
          ...other,
          id: 'search',
          type: 'search'
        }}
      />
    </div>
  );
}

function renderSuggestionsContainer(options) {
  const { containerProps, children } = options;

  return (
    <Paper {...containerProps} square style={styles.suggestionsContainer}>
      {children}
    </Paper>
  );
}

class Search extends Component {
  constructor(props){
  	super(props);
  	this.state = {
      suggestions: []
    };
  }

  onChange = (event, { newValue, method }) => {
    this.props.handleChange(newValue);
    this.setState({
      value: newValue
    });
  };

  onSuggestionsFetchRequested = ({ value }) => {
    this.props.handleChange(value);
    this.props.fetchSuggestion(value).then(res => {
      this.setState({
        suggestions: getSuggestions(value, res.data)
      });
    }, err => {
      this.setState({
        suggestions: getSuggestions(value, [])
      });
    });
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  render() {
    const { suggestions } = this.state;
    const inputProps = {
      placeholder: this.props.placeholder,
      value: this.props.value,
      onChange: this.onChange,
      label: this.props.label,
      isRequired: this.props.isRequired,
      disabled: this.props.disabled,
      style: this.props.style
    };
    const { classes } = this.props;
    return (
      <div>
        <Autosuggest
          theme={{
            container: classes.container,
            suggestionsContainerOpen: classes.suggestionsContainerOpen,
            suggestionsList: classes.suggestionsList,
            suggestion: classes.suggestion,
          }}
          suggestions={suggestions}
          onSuggestionsFetchRequested={(value) => this.onSuggestionsFetchRequested(value)}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          getSuggestionValue={getSuggestionValue}
          renderSuggestion={renderSuggestion}
          inputProps={inputProps}
          renderInputComponent={renderInput}
          renderSuggestionsContainer={renderSuggestionsContainer}
          alwaysRenderSuggestions={this.props.alwaysRenderSuggestions}
        />
        {this.props.error && <FormHelperText error>{this.props.error}</FormHelperText>}
        {this.props.reguiredError && <FormHelperText error>{this.props.label} is required</FormHelperText>}
      </div>
    )
  }
}

const styles = theme => ({
  container: {
    flexGrow: 1,
    position: 'relative'
  },
  suggestionsContainerOpen: {
    position: 'absolute',
    marginTop: 0,
    marginBottom: theme.spacing.unit * 3,
    left: 0,
    right: 0,
    zIndex: 10
  },
  suggestion: {
    display: 'block'
  },
  suggestionsList: {
    margin: 0,
    padding: 0,
    listStyleType: 'none'
  },
  suggestionsContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    zIndex: 10
  }
});

export default withStyles(styles)(Search);
