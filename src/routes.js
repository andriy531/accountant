import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { ConnectedRouter } from 'react-router-redux'
import { requireAuthentication } from './hocs/requireAuth'
import SignIn from './containers/SignIn'
import SignUp from './containers/SignUp'
import ForgotPassword from './containers/ForgotPassword'
import RestorePassword from './containers/RestorePassword'
import NotFound from './containers/NotFound'
import Setup from './containers/Setup'
import Engagements from './containers/Engagements'
import Dashboard from './containers/Dashboard'
import CreateEngagement from './containers/CreateEngagement'
import Clients from './containers/Clients'
import ClientDetails from './containers/ClientDetails'
import Services from './containers/Services'


export function routes (browserHistory) {
  return (
    <ConnectedRouter history={browserHistory}>
      <Switch>
        <Route exact path='/' component={requireAuthentication(Dashboard)} />
        <Route path='/signin' component={SignIn} />
        <Route path='/signup' component={SignUp} />
        <Route path='/forgotpswd' component={ForgotPassword} />
        <Route path='/restorepswd' component={RestorePassword} />
        <Route path='/setup' component={Setup} />
        <Route path='/setupstaff' component={Setup} />
        <Route exact path="/engagements" component={requireAuthentication(Engagements)}/>
        <Route exact path="/engagements/create" component={requireAuthentication(CreateEngagement)}/>
        <Route exact path="/clients" component={requireAuthentication(Clients)}/>
        <Route exact path="/clients/:id" component={requireAuthentication(ClientDetails)}/>
        <Route exact path="/services" component={requireAuthentication(Services)}/>
        <Route path='/notfound' component={NotFound}/>
        <Route component={NotFound}/>
      </Switch>
    </ConnectedRouter>
  )
}
