import React from 'react'
import FaFilePdfO from 'react-icons/lib/fa/file-pdf-o'
import FaFileImageO from 'react-icons/lib/fa/file-image-o'
import FaFileTextO from 'react-icons/lib/fa/file-text-o'
import FaFileExcelO from 'react-icons/lib/fa/file-excel-o'
import FaFileWordO from 'react-icons/lib/fa/file-word-o'
import FaFilePowerpointO from 'react-icons/lib/fa/file-powerpoint-o'


export const extensionIcons = {
  'pdf': <FaFilePdfO/>,
  'png': <FaFileImageO/>,
  'jpg': <FaFileImageO/>,
  'jpeg': <FaFileImageO/>,
  'txt': <FaFileTextO/>,
  'csv': <FaFileTextO/>,
  'xlsx': <FaFileExcelO/>,
  'xlsm': <FaFileExcelO/>,
  'xlsb': <FaFileExcelO/>,
  'xml': <FaFileExcelO/>,
  'doc': <FaFileWordO/>,
  'docx': <FaFileWordO/>,
  'dot': <FaFileWordO/>,
  'docm': <FaFileWordO/>,
  'ppt': <FaFilePowerpointO/>,
  'pot': <FaFilePowerpointO/>,
  'pps': <FaFilePowerpointO/>
}
