import axios from 'axios'

export const postSignUpApi = (body) => {
  return axios.post(`/invite/make_invitation`, body)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({message: err.response.data.error});
  })
}

export const postSignInApi = (body) => {
  return axios.post(`/auth/login`, body)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({message: err.response.data.error});
  })
}

export const postSetupApi = (body) => {
  return axios.post(`/register`, body)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({message: err.response.data.error});
  })
}

export const getAccountFromInviteApi = (params) => {
  return axios.get(`invite/check_invitation${params}`)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({message: 'Something went wrong'});
  })
}

export const getClientNotesApi = (client_id, numberOfPage, filterQuery) => {
  filterQuery = filterQuery.replace(/\s+/g, '-');
  return axios.get(`manage_clients/notes/get_notes?page=${numberOfPage}&client=${client_id}${filterQuery}`)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({message: 'Something went wrong'});
  })
}

export const getNotesAuthorsApi = (client_id) => {
  return axios.get(`manage_clients/notes/get_authors/${client_id}`)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({message: 'Something went wrong'});
  })
}

export const postClientNoteApi = (client_id, body) => {
  return axios.post(`manage_clients/notes/create/${client_id}`, body)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({message: err.response.data});
  })
}

export const getBillingsApi = () => {
  return axios.get(`service/get_billing_types`)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({message: err.response.data});
  })
}

export const createServiceApi = (body) => {
  return axios.post(`service/create`, body)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({data: err.response.data});
  })
}

export const updateServiceApi = (body, service_id) => {
  return axios.put(`service/modify/${service_id}`, body)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({data: err.response.data});
  })
}

export const getServicesApi = (page_number, filterQuery, limit = 10) => {
  filterQuery = filterQuery.replace(/\s+/g, '-');
  return axios.get(`service/get_services?page=${page_number}&limit=${limit}${filterQuery}`)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({data: err.response.data});
  })
}

export const getOneServiceApi = (service_id) => {
  return axios.get(`service/get_one/${service_id}`)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({data: err.response.data});
  })
}

export const deleteServiceApi = (service_id) => {
  return axios.delete(`service/delete/${service_id}`)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({data: err.response.data});
  })
}

export const createClientApi = (body) => {
  return axios.post(`manage_clients/create_client`, body)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({data: err.response.data});
  })
}

export const getClientsApi = (page_number, filterQuery, limit = 10) => {
  filterQuery = filterQuery.replace(/\s+/g, '-');
  return axios.get(`manage_clients/get_clients?page=${page_number}&limit=${limit}${filterQuery}`)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({data: err.response.data});
  })
}

export const deleteClientApi = (client_id) => {
  return axios.delete(`manage_clients/delete_client/${client_id}`)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({data: err.response.data});
  })
}

export const sendClientInvitationApi = (client_id) => {
  return axios.get(`manage_clients/make_client_invitation/${client_id}`)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({data: err.response.data});
  })
}

export const getOneClientApi = (client_id) => {
  return axios.get(`manage_clients/get_client/${client_id}`)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({data: err.response.data});
  })
}

export const updateClientApi = (body, client_id) => {
  return axios.put(`manage_clients/update_client/${client_id}`, body)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({data: err.response.data});
  })
}

export const getClientSearchAutosuggestApi = (searchType, searchWord) => {
  searchWord = searchWord.replace(/\s+/g, '-');
  return axios.get(`manage_clients/search/suggest?searchBy=${searchType}&searchQuery=${searchWord}`)
  .then(res => {
    return res.data;
  }, err => {
    return {data: []}
  })
}

export const getServicesSearchAutosuggestApi = (searchWord) => {
  searchWord = searchWord.replace(/\s+/g, '-');
  return axios.get(`service/search/suggest?searchQuery=${searchWord}`)
  .then(res => {
    return res.data;
  }, err => {
    return {data: []}
  })
}

export const deleteClientNoteApi = (noteId) => {
  return axios.delete(`manage_clients/notes/delete/${noteId}`)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({data: err.response.data});
  })
}

export const updateClientNoteApi = (noteId, body) => {
  return axios.put(`manage_clients/notes/update/${noteId}`, body)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({data: err.response.data});
  })
}

export const validateBusinessDomainApi = (businessDomain) => {
  return axios.get(`validation/business_domain/${businessDomain}`)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({data: err.response.data});
  })
}

export const validateZipCodeApi = (zipCode) => {
  return axios.get(`validation/zip_code/${zipCode}`)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({data: err.response.data});
  })
}

export const getDocumentUploadLink = (name) => {
  return axios.get(`manage_clients/documents/get_signed_url?fileName=${name}`)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({data: err.response.data});
  })
}

export const uploadDocumentApi = (signedRequestUrl, file, cancelToken, onUploadProgress) => {
  const headers = {
    'Content-Type': file.type
  }
  return axios.put(signedRequestUrl, file, {
    headers,
    onUploadProgress: (e) => onUploadProgress(e),
    cancelToken: cancelToken
  })
  .then(res => {
    return res;
  }, err => {
    if (axios.isCancel(err)) {
      return Promise.reject({data: {error: err.message}});
    } else {
      return Promise.reject({data: {error: 'Failed'}});
    }
  })
}

export const deleteDocumentApi = (body) => {
  return axios.delete('manage_clients/documents/delete', {data: body}).then(res => {
    return res.data;
  }, err => {
    return Promise.reject({data: {error: 'Something went wrong'}});
  })
}

export const getDocumentTypeAutosuggestApi = (searchWord, searchBy) => {
  searchWord = searchWord.replace(/\s+/g, '-');
  return axios.get(`manage_clients/documents/add/suggest?searchBy=${searchBy}&searchQuery=${searchWord}`)
  .then(res => {
    return res.data;
  }, err => {
    return {data: []}
  })
}

export const postClientDocumentApi = (client_id, body) => {
  return axios.post(`manage_clients/documents/add/${client_id}`, body)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({data: err.response.data});
  })
}

export const getClientDocumentsApi = (client_id, page_number, filterQuery, limit = 10) => {
  filterQuery = filterQuery.replace(/\s+/g, '-');
  return axios.get(`manage_clients/documents/get_documents/${client_id}?page=${page_number}&limit=${limit}${filterQuery}`)
  .then(res => {
    return res.data;
  }, err => {
    return Promise.reject({data: err.response.data});
  })
}
