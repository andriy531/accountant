import React, { Component } from 'react';
import { Provider } from 'react-redux'
import createHistory from 'history/createBrowserHistory'
import { routes } from './routes'
import createStore from './models/store'
import rootSaga from './models/saga'
import { MuiThemeProvider } from 'material-ui/styles';
import './styles/fonts.css'
import './styles/globals.css'
import { theme } from './styles/theme.js'
import axios from 'axios'
import { logoutRequest } from './models/auth/actions'
import { hideMobileHeader } from './models/viewport/actions'


const browserHistory = createHistory()

browserHistory.listen(location => store.dispatch(hideMobileHeader()));

export const store = createStore(browserHistory)

store.runSaga(rootSaga)

const API_URL = process.env.REACT_APP_API_URL;
axios.defaults.baseURL = API_URL;

// Add a request interceptor
axios.interceptors.request.use(config => {
  const cpa_data = JSON.parse(localStorage.getItem('cpa_data'));
  if (cpa_data) {
    config.headers['Auth'] = cpa_data['token']
  }

  return config
});

//Add a response interceptor
axios.interceptors.response.use(
  response => response,
  error => {
    if (error.response && (error.response.status === 401)) {
      store.dispatch(logoutRequest())
    }
    return Promise.reject(error)
  }
)

class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <Provider store={store}>
          {routes(browserHistory)}
        </Provider>
      </MuiThemeProvider>
    );
  }
}

export default App;
