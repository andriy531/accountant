// Effects
import { spawn, all } from 'redux-saga/effects'

// Watchers
import auth from './auth/saga'
import client from './client/saga'
import billings from './billings/saga'
import services from './services/saga'
import clients from './clients/saga'

// Subroutines
export default function* root() {
  yield all([
    spawn(auth),
    spawn(client),
    spawn(billings),
    spawn(services),
    spawn(clients)
  ])
}
