import { client_a as actions } from '../_actions.register'

const initialState = {
  basicInfo: null,
  basicInfoError: null,
  notes: {
    data: [],
    lengthOfNotes: 0
  },
  documents: {
    data: [],
    lengthOfDocuments: 0
  }
}

export default function reducer (state = initialState, action) {
  switch (action.type) {
    case actions.GET_CLIENT_BASIC_INFO_REQUEST:
      return {
        ...state,
        pending: true,
        basicInfoError: null
      }
    case actions.GET_CLIENT_BASIC_INFO_SUCCSESS:
      return {
        ...state,
        ...action.payload,
        pending: false,
        basicInfoError: null
      }
    case actions.GET_CLIENT_BASIC_INFO_FAILURE:
      return {
        ...state,
        ...action.payload,
        pending: false,
        basicInfo: null
      }
    case actions.GET_CLIENT_NOTES_REQUEST:
      return {
        ...state,
        pending: true,
      }
    case actions.GET_CLIENT_NOTES_SUCCSESS:
      return {
        ...state,
        notes: {...action.payload},
        pending: false,
      }
    case actions.GET_CLIENT_NOTES_FAILURE:
      return {
        ...state,
        ...action.payload,
        pending: false,
        notes: {
          data: [],
          lengthOfNotes: 0
        }
      }
    case actions.POST_CLIENT_NOTE_REQUEST:
      return {
        ...state,
        pending: true,
      }
    case actions.POST_CLIENT_NOTE_SUCCSESS:
      return {
        ...state,
        notes: {...action.payload},
        pending: false,
      }
    case actions.POST_CLIENT_NOTE_FAILURE:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    case actions.DELETE_CLIENT_NOTE_REQUEST:
      return {
        ...state,
        pending: true,
      }
    case actions.DELETE_CLIENT_NOTE_SUCCSESS:
      return {
        ...state,
        notes: {...action.payload},
        pending: false,
      }
    case actions.DELETE_CLIENT_NOTE_FAILURE:
      return {
        ...state,
        notes: {...action.payload},
        pending: false
      }
    case actions.EDIT_CLIENT_NOTE_REQUEST:
      return {
        ...state,
        pending: true,
      }
    case actions.EDIT_CLIENT_NOTE_SUCCSESS:
      return {
        ...state,
        notes: {...action.payload},
        pending: false,
      }
    case actions.EDIT_CLIENT_NOTE_FAILURE:
      return {
        ...state,
        notes: {...action.payload},
        pending: false
      }
    case actions.UPDATE_CLIENT_BASIC_INFO_REQUEST:
      return {
        ...state,
        pending: true,
        basicInfoError: null
      }
    case actions.UPDATE_CLIENT_BASIC_INFO_SUCCSESS:
      return {
        ...state,
        ...action.payload,
        pending: false,
        basicInfoError: null
      }
    case actions.UPDATE_CLIENT_BASIC_INFO_FAILURE:
      return {
        ...state,
        ...action.payload,
        pending: false,
        basicInfo: null
      }
    case actions.POST_CLIENT_DOCUMENT_REQUEST:
      return {
        ...state,
        pending: true,
      }
    case actions.POST_CLIENT_DOCUMENT_SUCCSESS:
      return {
        ...state,
        documents: {...action.payload},
        pending: false,
      }
    case actions.POST_CLIENT_DOCUMENT_FAILURE:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    case actions.GET_CLIENT_DOCUMENTS_REQUEST:
      return {
        ...state,
        pending: true,
      }
    case actions.GET_CLIENT_DOCUMENTS_SUCCSESS:
      return {
        ...state,
        documents: {...action.payload},
        pending: false,
      }
    case actions.GET_CLIENT_DOCUMENTS_FAILURE:
      return {
        ...state,
        ...action.payload,
        pending: false,
        documents: {
          data: [],
          lengthOfDocuments: 0
        }
      }
    default:
      return state
  }
}
