import { all, takeLatest, put, call, select } from 'redux-saga/effects'
import { client_a as actions, viewport_a } from '../_actions.register'
import { client_s as selectors } from '../_selectors.register'
import { getOneClientApi, getClientNotesApi, updateClientApi, postClientNoteApi, deleteClientNoteApi, updateClientNoteApi, postClientDocumentApi, getClientDocumentsApi } from '../../services/api'
import { showLoading, hideLoading } from 'react-redux-loading-bar'
import Notifications from 'react-notification-system-redux'
// import { push } from 'react-router-redux'
import _ from 'lodash'



function* getClientBasicInfoFlow(action) {
  yield put(showLoading());
  try {
    const { id } = action.payload;
    const res = yield call(getOneClientApi, id);
    yield put(actions.getClientBasicInfoSuccess({basicInfo: res.data}))
    yield put(viewport_a.showMobileHeader(`${res.data.firstName} ${res.data.lastName}`))
  } catch (e) {
    yield put(actions.getClientBasicInfoFailure({basicInfoError: e.message}))
    const notificationOpts = {
      title: 'Something went wrong!',
      message: e.message,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.error(notificationOpts));
  } finally {
    yield put(hideLoading());
  }
}

function* getClientNotesFlow(action) {
  yield put(showLoading());
  try {
    const { id, page, filters } = action.payload;
    let res;
    if(filters) {
      let filterQuery = '';
      _.forIn(filters, (value, key) => {
        if(value) {
          filterQuery += `&${key}=${value}`
        }
      })
      res = yield call(getClientNotesApi, id, page, filterQuery);
    } else {
      res = yield call(getClientNotesApi, id, page, '');
    }
    //const res = yield call(getClientNotesApi, id, page);
    const oldNotes = yield select(selectors.notes);
    let notes;
    if(page === 1) {
      notes = res.notes;
    } else {
      notes = [...oldNotes, ...res.notes];
    }
    yield put(actions.getClientNotesSuccess({data: notes, lengthOfNotes: res.lengthOfNotes}))
  } catch (e) {
    yield put(actions.getClientNotesFailure())
    const notificationOpts = {
      title: 'Something went wrong!',
      message: e.message,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.error(notificationOpts));
  } finally {
    yield put(hideLoading());
  }
}

function* postClientNoteFlow(action) {
  yield put(showLoading());
  try {
    const { client_id, note } = action.payload;
    const res = yield call(postClientNoteApi, client_id, note);
    const oldNotes = yield select(selectors.notes);
    const lengthOfNotes = yield select(selectors.lengthOfNotes);
    const notes = [res.data, ...oldNotes]
    yield put(actions.postClientNoteSuccess({data: notes, lengthOfNotes: lengthOfNotes + 1}))
  } catch (e) {
    yield put(actions.postClientNoteFailure())
    const notificationOpts = {
      title: 'Something went wrong!',
      message: e.message,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.error(notificationOpts));
  } finally {
    yield put(hideLoading());
  }
}

function* deleteClientNoteFlow(action) {
  yield put(showLoading());
  try {
    const { noteToDelete } = action.payload;
    const res = yield call(deleteClientNoteApi, noteToDelete.note_id)
    const notes = yield select(selectors.notes);
    let newNotes = [...notes];
    _.remove(newNotes, (n) => n.note_id === noteToDelete.note_id);
    const notificationOpts = {
      title: 'Success!',
      message: res.data,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.success(notificationOpts));
    const lengthOfNotes = yield select(selectors.lengthOfNotes);
    yield put(actions.deleteClientNoteSuccess({data: newNotes, lengthOfNotes: lengthOfNotes - 1}))
  } catch (e) {
    yield put(actions.deleteClientNoteFailure())
    const notificationOpts = {
      title: 'Something went wrong!',
      message: e.message,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.error(notificationOpts));
  } finally {
    yield put(hideLoading());
  }
}

function* editClientNoteFlow(action) {
  yield put(showLoading());
  try {
    const { noteToEdit } = action.payload;
    const res = yield call(updateClientNoteApi, noteToEdit.note_id, noteToEdit)
    const notes = yield select(selectors.notes);
    const lengthOfNotes = yield select(selectors.lengthOfNotes);
    let newNotes = _.map([...notes], (n) => {
      if(n.note_id === noteToEdit.note_id) {
        n = res.data;
      }
      return n;
    })
    yield put(actions.editClientNoteSuccess({data: newNotes, lengthOfNotes}))
  } catch (e) {

  } finally {
    yield put(hideLoading());
  }
}

function* updateClientBasicInfoFlow(action) {
  yield put(showLoading());
  try {
    const { body, client_id } = action.payload;
    delete body.client_id;
    delete body.clid;
    const res = yield call(updateClientApi, body, client_id);
    yield put(actions.updateClientBasicInfoSuccess({basicInfo: res.data}))
  } catch (e) {
    yield put(actions.updateClientBasicInfoFailure({basicInfoError: e.message}))
    const notificationOpts = {
      title: 'Something went wrong!',
      message: e.message,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.error(notificationOpts));
  } finally {
    yield put(hideLoading());
  }
}

function* postClientDocumentFlow(action) {
  yield put(showLoading());
  try {
    const { client_id, body } = action.payload;
    const res = yield call(postClientDocumentApi, client_id, body);
    const oldDocuments = yield select(selectors.documents);
    const lengthOfDocuments = yield select(selectors.lengthOfDocuments);
    const documents = [res.data, ...oldDocuments];
    yield put(actions.postClientDocumentSuccess({data: documents, lengthOfDocuments: lengthOfDocuments + 1}));
  } catch (e) {
    yield put(actions.postClientDocumentFailure())
    const notificationOpts = {
      title: 'Something went wrong!',
      message: e.message,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.error(notificationOpts));
  } finally {
    yield put(hideLoading());
  }
}

function* getClientDocumentsFlow(action) {
  yield put(showLoading());
  try {
    const { client_id, page_number, filters, limit } = action.payload;
    let res;
    if(filters) {
      let filterQuery = '';
      _.forIn(filters, (value, key) => {
        if(value) {
          filterQuery += `&${key}=${value}`
        }
      })
      res = yield call(getClientDocumentsApi, client_id, page_number, filterQuery, limit);
    } else {
      res = yield call(getClientDocumentsApi, client_id, page_number, '', limit);
    }
    yield put(actions.getClientDocumentsSuccess({data: res.documents, lengthOfDocuments: res.lengthOfTable}))
  } catch (e) {
    yield put(actions.getClientDocumentsFailure())
    const notificationOpts = {
      title: 'Something went wrong!',
      message: e.data.error,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.error(notificationOpts));
  } finally {
    yield put(hideLoading());
  }
}

export default function* watcher() {
  yield all([
    takeLatest(actions.GET_CLIENT_BASIC_INFO_REQUEST, getClientBasicInfoFlow),
    takeLatest(actions.GET_CLIENT_NOTES_REQUEST, getClientNotesFlow),
    takeLatest(actions.POST_CLIENT_NOTE_REQUEST, postClientNoteFlow),
    takeLatest(actions.DELETE_CLIENT_NOTE_REQUEST, deleteClientNoteFlow),
    takeLatest(actions.EDIT_CLIENT_NOTE_REQUEST, editClientNoteFlow),
    takeLatest(actions.UPDATE_CLIENT_BASIC_INFO_REQUEST, updateClientBasicInfoFlow),
    takeLatest(actions.POST_CLIENT_DOCUMENT_REQUEST, postClientDocumentFlow),
    takeLatest(actions.GET_CLIENT_DOCUMENTS_REQUEST, getClientDocumentsFlow),
  ]);
}
