export const GET_CLIENT_BASIC_INFO_REQUEST = 'CPA/GET_CLIENT_BASIC_INFO_REQUEST'
export const GET_CLIENT_BASIC_INFO_SUCCSESS = 'CPA/GET_CLIENT_BASIC_INFO_SUCCSESS'
export const GET_CLIENT_BASIC_INFO_FAILURE = 'CPA/GET_CLIENT_BASIC_INFO_FAILURE'
export const GET_CLIENT_NOTES_REQUEST = 'CPA/GET_CLIENT_NOTES_REQUEST'
export const GET_CLIENT_NOTES_SUCCSESS = 'CPA/GET_CLIENT_NOTES_SUCCSESS'
export const GET_CLIENT_NOTES_FAILURE = 'CPA/GET_CLIENT_NOTES_FAILURE'
export const POST_CLIENT_NOTE_REQUEST = 'CPA/POST_CLIENT_NOTE_REQUEST'
export const POST_CLIENT_NOTE_SUCCSESS = 'CPA/POST_CLIENT_NOTE_SUCCSESS'
export const POST_CLIENT_NOTE_FAILURE = 'CPA/POST_CLIENT_NOTE_FAILURE'
export const DELETE_CLIENT_NOTE_REQUEST = 'CPA/DELETE_CLIENT_NOTE_REQUEST'
export const DELETE_CLIENT_NOTE_SUCCSESS = 'CPA/DELETE_CLIENT_NOTE_SUCCSESS'
export const DELETE_CLIENT_NOTE_FAILURE = 'CPA/DELETE_CLIENT_NOTE_FAILURE'
export const EDIT_CLIENT_NOTE_REQUEST = 'CPA/EDIT_CLIENT_NOTE_REQUEST'
export const EDIT_CLIENT_NOTE_SUCCSESS = 'CPA/EDIT_CLIENT_NOTE_SUCCSESS'
export const EDIT_CLIENT_NOTE_FAILURE = 'CPA/EDIT_CLIENT_NOTE_FAILURE'
export const UPDATE_CLIENT_BASIC_INFO_REQUEST = 'CPA/UPDATE_CLIENT_BASIC_INFO_REQUEST'
export const UPDATE_CLIENT_BASIC_INFO_SUCCSESS = 'CPA/UPDATE_CLIENT_BASIC_INFO_SUCCSESS'
export const UPDATE_CLIENT_BASIC_INFO_FAILURE = 'CPA/UPDATE_CLIENT_BASIC_INFO_FAILURE'
export const POST_CLIENT_DOCUMENT_REQUEST = 'CPA/POST_CLIENT_DOCUMENT_REQUEST'
export const POST_CLIENT_DOCUMENT_SUCCSESS = 'CPA/POST_CLIENT_DOCUMENT_SUCCSESS'
export const POST_CLIENT_DOCUMENT_FAILURE = 'CPA/POST_CLIENT_DOCUMENT_FAILURE'
export const GET_CLIENT_DOCUMENTS_REQUEST = 'CPA/GET_CLIENT_DOCUMENTS_REQUEST'
export const GET_CLIENT_DOCUMENTS_SUCCSESS = 'CPA/GET_CLIENT_DOCUMENTS_SUCCSESS'
export const GET_CLIENT_DOCUMENTS_FAILURE = 'CPA/GET_CLIENT_DOCUMENTS_FAILURE'


export function getClientBasicInfoRequest (data) {
  return {
    type: GET_CLIENT_BASIC_INFO_REQUEST,
    payload: data
  }
}

export function getClientBasicInfoSuccess (data) {
  return {
    type: GET_CLIENT_BASIC_INFO_SUCCSESS,
    payload: data
  }
}

export function getClientBasicInfoFailure (data) {
  return {
    type: GET_CLIENT_BASIC_INFO_FAILURE,
    payload: data
  }
}

export function getClientNotesRequest (data) {
  return {
    type: GET_CLIENT_NOTES_REQUEST,
    payload: data
  }
}

export function getClientNotesSuccess (data) {
  return {
    type: GET_CLIENT_NOTES_SUCCSESS,
    payload: data
  }
}

export function getClientNotesFailure (data) {
  return {
    type: GET_CLIENT_NOTES_FAILURE,
    payload: data
  }
}

export function postClientNoteRequest (data) {
  return {
    type: POST_CLIENT_NOTE_REQUEST,
    payload: data
  }
}

export function postClientNoteSuccess (data) {
  return {
    type: POST_CLIENT_NOTE_SUCCSESS,
    payload: data
  }
}

export function postClientNoteFailure (data) {
  return {
    type: POST_CLIENT_NOTE_FAILURE,
    payload: data
  }
}

export function deleteClientNoteRequest (data) {
  return {
    type: DELETE_CLIENT_NOTE_REQUEST,
    payload: data
  }
}

export function deleteClientNoteSuccess (data) {
  return {
    type: DELETE_CLIENT_NOTE_SUCCSESS,
    payload: data
  }
}

export function deleteClientNoteFailure (data) {
  return {
    type: DELETE_CLIENT_NOTE_FAILURE,
    payload: data
  }
}

export function editClientNoteRequest (data) {
  return {
    type: EDIT_CLIENT_NOTE_REQUEST,
    payload: data
  }
}

export function editClientNoteSuccess (data) {
  return {
    type: EDIT_CLIENT_NOTE_SUCCSESS,
    payload: data
  }
}

export function editClientNoteFailure (data) {
  return {
    type: EDIT_CLIENT_NOTE_FAILURE,
    payload: data
  }
}

export function updateClientBasicInfoRequest (data) {
  return {
    type: UPDATE_CLIENT_BASIC_INFO_REQUEST,
    payload: data
  }
}

export function updateClientBasicInfoSuccess (data) {
  return {
    type: UPDATE_CLIENT_BASIC_INFO_SUCCSESS,
    payload: data
  }
}

export function updateClientBasicInfoFailure (data) {
  return {
    type: UPDATE_CLIENT_BASIC_INFO_FAILURE,
    payload: data
  }
}

export function postClientDocumentRequest (data) {
  return {
    type: POST_CLIENT_DOCUMENT_REQUEST,
    payload: data
  }
}

export function postClientDocumentSuccess (data) {
  return {
    type: POST_CLIENT_DOCUMENT_SUCCSESS,
    payload: data
  }
}

export function postClientDocumentFailure (data) {
  return {
    type: POST_CLIENT_DOCUMENT_FAILURE,
    payload: data
  }
}

export function getClientDocumentsRequest (data) {
  return {
    type: GET_CLIENT_DOCUMENTS_REQUEST,
    payload: data
  }
}

export function getClientDocumentsSuccess (data) {
  return {
    type: GET_CLIENT_DOCUMENTS_SUCCSESS,
    payload: data
  }
}

export function getClientDocumentsFailure (data) {
  return {
    type: GET_CLIENT_DOCUMENTS_FAILURE,
    payload: data
  }
}
