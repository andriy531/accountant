import get from 'lodash/get';

/**
 * Selectors are useful to compute derived data from the Redux store.
 * Selectors are functions that accept the Redux store as an argument, and return
 * specific keys from it.
 */

/**
 * Derives a profile from the Redux store.
 *
 * @param  {Object} state The Redux store of the application
 * @return {Any}          Profile information
 */

export const servicesData = (state) => get(state, ['services', 'data'])
export const lengthOfServices = (state) => get(state, ['services', 'lengthOfServices'])
export const serviceToEdit = (state) => get(state, ['services', 'serviceToEdit'])
export const limit = (state) => get(state, ['services', 'limit'])
