import { all, takeLatest, put, call, select } from 'redux-saga/effects'
import { services_a as actions, viewport_a } from '../_actions.register'
import { services_s as selectors } from '../_selectors.register'
import { createServiceApi, getServicesApi, getOneServiceApi, updateServiceApi, deleteServiceApi } from '../../services/api'
import { showLoading, hideLoading } from 'react-redux-loading-bar'
import Notifications from 'react-notification-system-redux'
import _ from 'lodash'
import uuidv1 from 'uuid/v1'

function* postServiceFlow(action) {
  yield put(showLoading());
  try {
    const body = {
      ...action.payload,
      clientTasks: _.map(action.payload.clientTasks, (o) => {
        delete o.uuid
        delete o.task_id
        delete o.taskType
        delete o.service_id
        return o;
      }),
      teamTasks: _.map(action.payload.teamTasks, (o) => {
        delete o.uuid
        delete o.task_id
        delete o.taskType
        delete o.service_id
        return o;
      })
    }
    const res = yield call(createServiceApi, body);
    const servicesData = yield select(selectors.servicesData);
    let lengthOfServices = yield select(selectors.lengthOfServices);
    const limit = yield select(selectors.limit);
    let newServicesData = [res.data, ...servicesData];
    if(newServicesData.length > limit) { newServicesData.length = limit };
    yield put(actions.postServiceSuccess({data: newServicesData, lengthOfServices: lengthOfServices + 1}))
    const notificationOpts = {
      title: 'Success!',
      message: `Service ${res.data.name} successfully created`,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.success(notificationOpts));
    yield put(viewport_a.closeCreateServiceModal())
  } catch (e) {
    yield put(actions.postServiceFailure())
    const notificationOpts = {
      title: 'Something went wrong!',
      message: e.data.error,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.error(notificationOpts));
  } finally {
    yield put(hideLoading());
  }
}

function* getServicesFlow(action) {
  yield put(showLoading());
  try {
    const { page_number, filters, limit } = action.payload;
    let res;
    if(filters) {
      let filterQuery = '';
      _.forIn(filters, (value, key) => {
        if(value) {
          filterQuery += `&${key}=${value}`
        }
      })
      res = yield call(getServicesApi, page_number, filterQuery, limit);
    } else {
      res = yield call(getServicesApi, page_number, '', limit);
    }
    yield put(actions.getServicesSuccess({data: res.services, lengthOfServices: res.lengthOfServices}))
  } catch (e) {
    yield put(actions.getServicesFailure())
    const notificationOpts = {
      title: 'Something went wrong!',
      message: e.data.error,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.error(notificationOpts));
  } finally {
    yield put(hideLoading());
  }
}

function* getOneServiceFlow(action) {
  try {
    yield put(showLoading());
    const { service_id, duplicate } = action.payload;
    const res = yield call(getOneServiceApi, service_id);
    let serviceToEdit = res.data;
    const clientTasks = _.map(serviceToEdit.clientTasks, (t) => {
      t.uuid = uuidv1();
      return t;
    })
    const teamTasks = _.map(serviceToEdit.teamTasks, (t) => {
      t.uuid = uuidv1();
      return t;
    })
    serviceToEdit.clientTasks = clientTasks;
    serviceToEdit.teamTasks = teamTasks;
    yield put(actions.getOneServiceSuccess({serviceToEdit, duplicateService: duplicate}))
    yield put(viewport_a.openCreateServiceModal());
  } catch (e) {
    yield put(actions.getOneServiceFailure({serviceToEdit: null, duplicateService: false}))
    const notificationOpts = {
      title: 'Something went wrong!',
      message: e.data.error,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.error(notificationOpts));
  } finally {
    yield put(hideLoading());
  }
}

function* updateServiceFlow(action) {
  yield put(showLoading());
  try {
    const { body, service_id } = action.payload;
    const updatedBody = {
      ...body,
      clientTasks: _.map(body.clientTasks, (o) => {
        delete o.uuid
        return o;
      }),
      teamTasks: _.map(body.teamTasks, (o) => {
        delete o.uuid
        return o;
      })
    }

    const res = yield call(updateServiceApi, updatedBody, service_id);
    const servicesData = yield select(selectors.servicesData);
    let newServicesData = [...servicesData];
    const serviceIndex = _.findIndex(newServicesData, (o) => o.service_id === res.data.service_id );
    newServicesData[serviceIndex] = res.data;
    yield put(actions.updateServiceSuccess({data: newServicesData}))
    const notificationOpts = {
      title: 'Success!',
      message: `Service ${res.data.name} successfully updated`,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.success(notificationOpts));
    yield put(viewport_a.closeCreateServiceModal())
    yield put(actions.cancelEditService())
  } catch (e) {
    yield put(actions.updateServiceFailure())
    const notificationOpts = {
      title: 'Something went wrong!',
      message: e.data.error,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.error(notificationOpts));
  } finally {
    yield put(hideLoading());
  }
}

function* deleteServiceFlow(action) {
  yield put(showLoading());
  try {
    const { service_id, page_number, filters } = action.payload;
    const res = yield call(deleteServiceApi, service_id);
    const limit = yield select(selectors.limit);
    yield put(actions.getServicesRequest({page_number, filters, limit}))
    yield put(actions.deleteServiceSuccess())
    const notificationOpts = {
      title: 'Success!',
      message: res.data,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.success(notificationOpts));
  } catch (e) {
    const notificationOpts = {
      title: 'Something went wrong!',
      message: e.data.error,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.error(notificationOpts));
    yield put(actions.deleteServiceFailure())
  } finally {
    yield put(hideLoading());
  }
}

export default function* watcher() {
  yield all([
    takeLatest(actions.POST_SERVICE_REQUEST, postServiceFlow),
    takeLatest(actions.GET_SERVICES_REQUEST, getServicesFlow),
    takeLatest(actions.GET_ONE_SERVICE_REQUEST, getOneServiceFlow),
    takeLatest(actions.UPDATE_SERVICE_REQUEST, updateServiceFlow),
    takeLatest(actions.DELETE_SERVICE_REQUEST, deleteServiceFlow)
  ]);
}
