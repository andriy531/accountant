import { services_a as actions } from '../_actions.register'

const initialState = {
  pending: false,
  data: [],
  lengthOfServices: 0,
  serviceToEdit: null,
  duplicateService: false,
  limit: 10
}

export default function reducer (state = initialState, action) {
  switch (action.type) {
    case actions.POST_SERVICE_REQUEST:
      return {
        ...state,
        pending: true
      }
    case actions.POST_SERVICE_SUCCSESS:
      return {
        ...state,
        ...action.payload,
        pending: false,
        duplicateService: false,
        serviceToEdit: null
      }
    case actions.POST_SERVICE_FAILURE:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    case actions.UPDATE_SERVICE_REQUEST:
      return {
        ...state,
        pending: true
      }
    case actions.UPDATE_SERVICE_SUCCSESS:
      return {
        ...state,
        ...action.payload,
        pending: false,
        duplicateService: false,
        serviceToEdit: null
      }
    case actions.UPDATE_SERVICE_FAILURE:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    case actions.GET_SERVICES_REQUEST:
      return {
        ...state,
        limit: action.payload.limit,
        pending: true
      }
    case actions.GET_SERVICES_SUCCSESS:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    case actions.GET_SERVICES_FAILURE:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    case actions.GET_ONE_SERVICE_REQUEST:
      return {
        ...state,
        pending: true
      }
    case actions.GET_ONE_SERVICE_SUCCSESS:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    case actions.GET_ONE_SERVICE_FAILURE:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    case actions.CANCEL_EDIT_SERVICE:
      return {
        ...state,
        serviceToEdit: null,
        duplicateService: false
      }
    case actions.DELETE_SERVICE_REQUEST:
      return {
        ...state,
        pending: true
      }
    case actions.DELETE_SERVICE_SUCCSESS:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    case actions.DELETE_SERVICE_FAILURE:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    default:
      return state
  }
}
