export const POST_SERVICE_REQUEST = 'CPA/POST_SERVICE_REQUEST'
export const POST_SERVICE_SUCCSESS = 'CPA/POST_SERVICE_SUCCSESS'
export const POST_SERVICE_FAILURE = 'CPA/POST_SERVICE_FAILURE'
export const GET_SERVICES_REQUEST = 'CPA/GET_SERVICES_REQUEST'
export const GET_SERVICES_SUCCSESS = 'CPA/GET_SERVICES_SUCCSESS'
export const GET_SERVICES_FAILURE = 'CPA/GET_SERVICES_FAILURE'
export const GET_ONE_SERVICE_REQUEST = 'CPA/GET_ONE_SERVICE_REQUEST'
export const GET_ONE_SERVICE_SUCCSESS = 'CPA/GET_ONE_SERVICE_SUCCSESS'
export const GET_ONE_SERVICE_FAILURE = 'CPA/GET_ONE_SERVICE_FAILURE'
export const CANCEL_EDIT_SERVICE = 'CPA/CANCEL_EDIT_SERVICE'
export const UPDATE_SERVICE_REQUEST = 'CPA/UPDATE_SERVICE_REQUEST'
export const UPDATE_SERVICE_SUCCSESS = 'CPA/UPDATE_SERVICE_SUCCSESS'
export const UPDATE_SERVICE_FAILURE = 'CPA/UPDATE_SERVICE_FAILURE'
export const DELETE_SERVICE_REQUEST = 'CPA/DELETE_SERVICE_REQUEST'
export const DELETE_SERVICE_SUCCSESS = 'CPA/DELETE_SERVICE_SUCCSESS'
export const DELETE_SERVICE_FAILURE = 'CPA/DELETE_SERVICE_FAILURE'


export function postServiceRequest (data) {
  return {
    type: POST_SERVICE_REQUEST,
    payload: data
  }
}

export function postServiceSuccess (data) {
  return {
    type: POST_SERVICE_SUCCSESS,
    payload: data
  }
}

export function postServiceFailure (data) {
  return {
    type: POST_SERVICE_FAILURE,
    payload: data
  }
}

export function getServicesRequest (data) {
  return {
    type: GET_SERVICES_REQUEST,
    payload: data
  }
}

export function getServicesSuccess (data) {
  return {
    type: GET_SERVICES_SUCCSESS,
    payload: data
  }
}

export function getServicesFailure (data) {
  return {
    type: GET_SERVICES_FAILURE,
    payload: data
  }
}

export function getOneServiceRequest (data) {
  return {
    type: GET_ONE_SERVICE_REQUEST,
    payload: data
  }
}

export function getOneServiceSuccess (data) {
  return {
    type: GET_ONE_SERVICE_SUCCSESS,
    payload: data
  }
}

export function getOneServiceFailure (data) {
  return {
    type: GET_ONE_SERVICE_FAILURE,
    payload: data
  }
}

export function cancelEditService () {
  return {
    type: CANCEL_EDIT_SERVICE
  }
}

export function updateServiceRequest (data) {
  return {
    type: UPDATE_SERVICE_REQUEST,
    payload: data
  }
}

export function updateServiceSuccess (data) {
  return {
    type: UPDATE_SERVICE_SUCCSESS,
    payload: data
  }
}

export function updateServiceFailure (data) {
  return {
    type: UPDATE_SERVICE_FAILURE,
    payload: data
  }
}

export function deleteServiceRequest (data) {
  return {
    type: DELETE_SERVICE_REQUEST,
    payload: data
  }
}

export function deleteServiceSuccess (data) {
  return {
    type: DELETE_SERVICE_SUCCSESS,
    payload: data
  }
}

export function deleteServiceFailure (data) {
  return {
    type: DELETE_SERVICE_FAILURE,
    payload: data
  }
}
