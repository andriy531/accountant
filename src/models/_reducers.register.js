import auth from './auth/reducer'
import client from './client/reducer'
import viewport from './viewport/reducer'
import billings from './billings/reducer'
import services from './services/reducer'
import clients from './clients/reducer'


export {
  auth,
  client,
  viewport,
  billings,
  services,
  clients
}
