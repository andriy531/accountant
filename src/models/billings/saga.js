import { all, takeLatest, put, call } from 'redux-saga/effects'
import { billings_a as actions } from '../_actions.register'
//import { billings_s as selectors } from '../_selectors.register'
import { getBillingsApi } from '../../services/api'
import { showLoading, hideLoading } from 'react-redux-loading-bar'
//import _ from 'lodash'

function* getBillingsFlow(action) {
  yield put(showLoading());
  try {
    const res = yield call(getBillingsApi);
    yield put(actions.getBillingsSuccess({data: res.data}))
  } catch (e) {
    yield put(actions.getBillingsFailure({error: 'Oops - something went wrong with getting billing types...'}))
  } finally {
    yield put(hideLoading());
  }
}

export default function* watcher() {
  yield all([
    takeLatest(actions.GET_BILLINGS_REQUEST, getBillingsFlow)
  ]);
}
