export const GET_BILLINGS_REQUEST = 'CPA/GET_BILLINGS_REQUEST'
export const GET_BILLINGS_SUCCSESS = 'CPA/GET_BILLINGS_SUCCSESS'
export const GET_BILLINGS_FAILURE = 'CPA/GET_BILLINGS_FAILURE'


export function getBillingsRequest (data) {
  return {
    type: GET_BILLINGS_REQUEST,
    payload: data
  }
}

export function getBillingsSuccess (data) {
  return {
    type: GET_BILLINGS_SUCCSESS,
    payload: data
  }
}

export function getBillingsFailure (data) {
  return {
    type: GET_BILLINGS_FAILURE,
    payload: data
  }
}
