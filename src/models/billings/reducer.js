import { billings_a as actions } from '../_actions.register'

const initialState = {
  data: null,
  error: null,
  pending: false
}

export default function reducer (state = initialState, action) {
  switch (action.type) {
    case actions.GET_BILLINGS_REQUEST:
      return {
        ...state,
        pending: true,
        error: null
      }
    case actions.GET_BILLINGS_SUCCSESS:
      return {
        ...state,
        ...action.payload,
        pending: false,
        error: null
      }
    case actions.GET_BILLINGS_FAILURE:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    default:
      return state
  }
}
