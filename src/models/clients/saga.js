import { all, takeLatest, put, call, select } from 'redux-saga/effects'
import { clients_a as actions, viewport_a } from '../_actions.register'
import { clients_s as selectors } from '../_selectors.register'
import { createClientApi, getClientsApi, deleteClientApi, sendClientInvitationApi, getOneClientApi, updateClientApi } from '../../services/api'
import { showLoading, hideLoading } from 'react-redux-loading-bar'
import Notifications from 'react-notification-system-redux'
import _ from 'lodash'

function* postClientFlow(action) {
  yield put(showLoading());
  try {
    const { body } = action.payload;
    const res = yield call(createClientApi, body);
    const clientsData = yield select(selectors.clientsData);
    let lengthOfClients = yield select(selectors.lengthOfClients);
    const limit = yield select(selectors.limit);
    let newClientsData = [res.data, ...clientsData];
    if(newClientsData.length > limit) { newClientsData.length = limit };
    yield put(actions.postClientSuccess({data: newClientsData, lengthOfClients: lengthOfClients + 1}))
    const notificationOpts = {
      title: 'Success!',
      message: `${res.data.firstName} ${res.data.lastName} successfully created`,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.success(notificationOpts));
    yield put(viewport_a.closeCreateClientModal())
  } catch (e) {
    yield put(actions.postClientFailure())
    const notificationOpts = {
      title: 'Something went wrong!',
      message: e.data.error,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.error(notificationOpts));
  } finally {
    yield put(hideLoading());
  }
}

function* getClientsFlow(action) {
  yield put(showLoading());
  try {
    const { page_number, filters, limit } = action.payload;
    let res;
    if(filters) {
      let filterQuery = '';
      _.forIn(filters, (value, key) => {
        if(value) {
          filterQuery += `&${key}=${value}`
        }
      })
      res = yield call(getClientsApi, page_number, filterQuery, limit);
    } else {
      res = yield call(getClientsApi, page_number, '', limit);
    }
    yield put(actions.getClientsSuccess({data: res.clients, lengthOfClients: res.lengthOfClients}))
  } catch (e) {
    yield put(actions.getClientsFailure())
    const notificationOpts = {
      title: 'Something went wrong!',
      message: e.data.error,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.error(notificationOpts));
  } finally {
    yield put(hideLoading());
  }
}

function* deleteClientFlow(action) {
  yield put(showLoading());
  try {
    const { client_id, page_number, filters } = action.payload;
    const res = yield call(deleteClientApi, client_id)
    const limit = yield select(selectors.limit);
    yield put(actions.getClientsRequest({page_number, filters, limit}))
    yield put(actions.deleteClientSuccess())
    const notificationOpts = {
      title: 'Success!',
      message: res.data,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.success(notificationOpts));
  } catch (e) {
    const notificationOpts = {
      title: 'Something went wrong!',
      message: e.data.error,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.error(notificationOpts));
    yield put(actions.deleteClientFailure())
  } finally {
    yield put(hideLoading());
  }
}

function* sendClientInvitationFlow(action) {
  yield put(showLoading());
  try {
    const { client_id } = action.payload;
    const res = yield call(sendClientInvitationApi, client_id);
    const clientsData = yield select(selectors.clientsData);
    let newClientsData = [...clientsData];
    const clientIndex = _.findIndex(newClientsData, (o) => o.client_id === client_id );
    newClientsData[clientIndex].status = "invited"
    yield put(actions.sendClientInvitationSuccess({data: newClientsData}))
    const notificationOpts = {
      title: 'Success!',
      message: res.data,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.success(notificationOpts));
  } catch (e) {
    yield put(actions.sendClientInvitationFailure())
    const notificationOpts = {
      title: 'Something went wrong!',
      message: e.data.error,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.error(notificationOpts));
  } finally {
    yield put(hideLoading());
  }
}

function* getOneClientFlow(action) {
  try {
    yield put(showLoading());
    const { client_id, duplicate } = action.payload;
    const res = yield call(getOneClientApi, client_id);
    let clientToEdit = res.data;

    yield put(actions.getOneClientSuccess({clientToEdit, duplicateClient: duplicate}))
    yield put(viewport_a.openCreateClientModal());
  } catch (e) {
    yield put(actions.getOneClientFailure({clientToEdit: null, duplicateClient: false}))
    const notificationOpts = {
      title: 'Something went wrong!',
      message: e.data.error,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.error(notificationOpts));
  } finally {
    yield put(hideLoading());
  }
}

function* updateClientFlow(action) {
  yield put(showLoading());
  try {
    const { body, client_id } = action.payload;

    delete body.client_id;
    delete body.clid;

    const res = yield call(updateClientApi, body, client_id);
    const clientsData = yield select(selectors.clientsData);
    let newClientsData = [...clientsData];
    const clientIndex = _.findIndex(newClientsData, (o) => o.client_id === res.data.client_id );
    newClientsData[clientIndex] = res.data;
    yield put(actions.updateClientSuccess({data: newClientsData}))
    const notificationOpts = {
      title: 'Success!',
      message: `${res.data.firstName} ${res.data.lastName} successfully updated`,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.success(notificationOpts));
    yield put(viewport_a.closeCreateClientModal())
  } catch (e) {
    yield put(actions.updateClientFailure())
    const notificationOpts = {
      title: 'Something went wrong!',
      message: e.data.error,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.error(notificationOpts));
  } finally {
    yield put(hideLoading());
  }
}


export default function* watcher() {
  yield all([
    takeLatest(actions.POST_CLIENT_REQUEST, postClientFlow),
    takeLatest(actions.GET_CLIENTS_REQUEST, getClientsFlow),
    takeLatest(actions.DELETE_CLIENT_REQUEST, deleteClientFlow),
    takeLatest(actions.SEND_CLIENT_INVITATION_REQUEST, sendClientInvitationFlow),
    takeLatest(actions.GET_ONE_CLIENT_REQUEST, getOneClientFlow),
    takeLatest(actions.UPDATE_CLIENT_REQUEST, updateClientFlow),
  ]);
}
