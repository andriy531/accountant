import { clients_a as actions } from '../_actions.register'

const initialState = {
  pending: false,
  data: [],
  lengthOfClients: 0,
  clientToEdit: null,
  duplicateClient: false,
  limit: 10
}

export default function reducer (state = initialState, action) {
  switch (action.type) {
    case actions.POST_CLIENT_REQUEST:
      return {
        ...state,
        pending: true
      }
    case actions.POST_CLIENT_SUCCSESS:
      return {
        ...state,
        ...action.payload,
        pending: false,
        duplicateClient: false,
        clientToEdit: null
      }
    case actions.POST_CLIENT_FAILURE:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    case actions.GET_CLIENTS_REQUEST:
      return {
        ...state,
        limit: action.payload.limit,
        pending: true
      }
    case actions.GET_CLIENTS_SUCCSESS:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    case actions.GET_CLIENTS_FAILURE:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    case actions.DELETE_CLIENT_REQUEST:
      return {
        ...state,
        pending: true
      }
    case actions.DELETE_CLIENT_SUCCSESS:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    case actions.DELETE_CLIENT_FAILURE:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    case actions.SEND_CLIENT_INVITATION_REQUEST:
      return {
        ...state,
        pending: true
      }
    case actions.SEND_CLIENT_INVITATION_SUCCSESS:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    case actions.SEND_CLIENT_INVITATION_FAILURE:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    case actions.GET_ONE_CLIENT_REQUEST:
      return {
        ...state,
        pending: true
      }
    case actions.GET_ONE_CLIENT_SUCCSESS:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    case actions.GET_ONE_CLIENT_FAILURE:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    case actions.CANCEL_EDIT_CLIENT:
      return {
        ...state,
        clientToEdit: null,
        duplicateClient: false
      }
    case actions.UPDATE_CLIENT_REQUEST:
      return {
        ...state,
        pending: true
      }
    case actions.UPDATE_CLIENT_SUCCSESS:
      return {
        ...state,
        ...action.payload,
        pending: false,
        duplicateClient: false,
        clientToEdit: null
      }
    case actions.UPDATE_CLIENT_FAILURE:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    default:
      return state
  }
}
