export const POST_CLIENT_REQUEST = 'CPA/POST_CLIENT_REQUEST'
export const POST_CLIENT_SUCCSESS = 'CPA/POST_CLIENT_SUCCSESS'
export const POST_CLIENT_FAILURE = 'CPA/POST_CLIENT_FAILURE'
export const GET_CLIENTS_REQUEST = 'CPA/GET_CLIENTS_REQUEST'
export const GET_CLIENTS_SUCCSESS = 'CPA/GET_CLIENTS_SUCCSESS'
export const GET_CLIENTS_FAILURE = 'CPA/GET_CLIENTS_FAILURE'
export const DELETE_CLIENT_REQUEST = 'CPA/DELETE_CLIENT_REQUEST'
export const DELETE_CLIENT_SUCCSESS = 'CPA/DELETE_CLIENT_SUCCSESS'
export const DELETE_CLIENT_FAILURE = 'CPA/DELETE_CLIENT_FAILURE'
export const SEND_CLIENT_INVITATION_REQUEST = 'CPA/SEND_CLIENT_INVITATION_REQUEST'
export const SEND_CLIENT_INVITATION_SUCCSESS = 'CPA/SEND_CLIENT_INVITATION_SUCCSESS'
export const SEND_CLIENT_INVITATION_FAILURE = 'CPA/SEND_CLIENT_INVITATION_FAILURE'
export const GET_ONE_CLIENT_REQUEST = 'CPA/GET_ONE_CLIENT_REQUEST'
export const GET_ONE_CLIENT_SUCCSESS = 'CPA/GET_ONE_CLIENT_SUCCSESS'
export const GET_ONE_CLIENT_FAILURE = 'CPA/GET_ONE_CLIENT_FAILURE'
export const CANCEL_EDIT_CLIENT = 'CPA/CANCEL_EDIT_CLIENT'
export const UPDATE_CLIENT_REQUEST = 'CPA/UPDATE_CLIENT_REQUEST'
export const UPDATE_CLIENT_SUCCSESS = 'CPA/UPDATE_CLIENT_SUCCSESS'
export const UPDATE_CLIENT_FAILURE = 'CPA/UPDATE_CLIENT_FAILURE'


export function postClientRequest (data) {
  return {
    type: POST_CLIENT_REQUEST,
    payload: data
  }
}

export function postClientSuccess (data) {
  return {
    type: POST_CLIENT_SUCCSESS,
    payload: data
  }
}

export function postClientFailure (data) {
  return {
    type: POST_CLIENT_FAILURE,
    payload: data
  }
}

export function getClientsRequest (data) {
  return {
    type: GET_CLIENTS_REQUEST,
    payload: data
  }
}

export function getClientsSuccess (data) {
  return {
    type: GET_CLIENTS_SUCCSESS,
    payload: data
  }
}

export function getClientsFailure (data) {
  return {
    type: GET_CLIENTS_FAILURE,
    payload: data
  }
}

export function deleteClientRequest (data) {
  return {
    type: DELETE_CLIENT_REQUEST,
    payload: data
  }
}

export function deleteClientSuccess (data) {
  return {
    type: DELETE_CLIENT_SUCCSESS,
    payload: data
  }
}

export function deleteClientFailure (data) {
  return {
    type: DELETE_CLIENT_FAILURE,
    payload: data
  }
}

export function sendClientInvitationRequest (data) {
  return {
    type: SEND_CLIENT_INVITATION_REQUEST,
    payload: data
  }
}

export function sendClientInvitationSuccess (data) {
  return {
    type: SEND_CLIENT_INVITATION_SUCCSESS,
    payload: data
  }
}

export function sendClientInvitationFailure (data) {
  return {
    type: SEND_CLIENT_INVITATION_FAILURE,
    payload: data
  }
}

export function getOneClientRequest (data) {
  return {
    type: GET_ONE_CLIENT_REQUEST,
    payload: data
  }
}

export function getOneClientSuccess (data) {
  return {
    type: GET_ONE_CLIENT_SUCCSESS,
    payload: data
  }
}

export function getOneClientFailure (data) {
  return {
    type: GET_ONE_CLIENT_FAILURE,
    payload: data
  }
}

export function cancelEditClient () {
  return {
    type: CANCEL_EDIT_CLIENT
  }
}

export function updateClientRequest (data) {
  return {
    type: UPDATE_CLIENT_REQUEST,
    payload: data
  }
}

export function updateClientSuccess (data) {
  return {
    type: UPDATE_CLIENT_SUCCSESS,
    payload: data
  }
}

export function updateClientFailure (data) {
  return {
    type: UPDATE_CLIENT_FAILURE,
    payload: data
  }
}
