export const OPEN_CREATE_SERVICE_MODAL = 'CPA/OPEN_CREATE_SERVICE_MODAL'
export const CLOSE_CREATE_SERVICE_MODAL = 'CPA/CLOSE_CREATE_SERVICE_MODAL'
export const OPEN_CREATE_CLIENT_MODAL = 'CPA/OPEN_CREATE_CLIENT_MODAL'
export const CLOSE_CREATE_CLIENT_MODAL = 'CPA/CLOSE_CREATE_CLIENT_MODAL'
export const SHOW_MOBILE_HEADER = 'CPA/SHOW_MOBILE_HEADER'
export const HIDE_MOBILE_HEADER = 'CPA/HIDE_MOBILE_HEADER'

export function openCreateServiceModal () {
  return {
    type: OPEN_CREATE_SERVICE_MODAL
  }
}

export function closeCreateServiceModal (data) {
  return {
    type: CLOSE_CREATE_SERVICE_MODAL
  }
}

export function openCreateClientModal () {
  return {
    type: OPEN_CREATE_CLIENT_MODAL
  }
}

export function closeCreateClientModal (data) {
  return {
    type: CLOSE_CREATE_CLIENT_MODAL
  }
}

export function showMobileHeader (data) {
  return {
    type: SHOW_MOBILE_HEADER,
    payload: data
  }
}

export function hideMobileHeader (data) {
  return {
    type: HIDE_MOBILE_HEADER,
  }
}
