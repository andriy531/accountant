import { viewport_a as actions } from '../_actions.register'

const initialState = {
  showCreateServiceModal: false,
  showCreateClientModal: false,
  mobileHeader: ''
}

export default function reducer (state = initialState, action) {
  switch (action.type) {
    case actions.OPEN_CREATE_SERVICE_MODAL:
      return {
        ...state,
        showCreateServiceModal: true
      }
    case actions.CLOSE_CREATE_SERVICE_MODAL:
      return {
        ...state,
        showCreateServiceModal: false
      }
    case actions.OPEN_CREATE_CLIENT_MODAL:
      return {
        ...state,
        showCreateClientModal: true
      }
    case actions.CLOSE_CREATE_CLIENT_MODAL:
      return {
        ...state,
        showCreateClientModal: false
      }
    case actions.SHOW_MOBILE_HEADER:
      return {
        ...state,
        mobileHeader: action.payload
      }
    case actions.HIDE_MOBILE_HEADER:
      return {
        ...state,
        mobileHeader: ''
      }
    default:
      return state
  }
}
