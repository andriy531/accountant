export const POST_SIGNUP_REQUEST = 'CPA/POST_SIGNUP_REQUEST'
export const POST_SIGNUP_SUCCESS = 'CPA/POST_SIGNUP_SUCCESS'
export const POST_SIGNUP_FAILURE = 'CPA/POST_SIGNUP_FAILURE'
export const POST_SIGNIN_REQUEST = 'CPA/POST_SIGNIN_REQUEST'
export const POST_SIGNIN_SUCCESS = 'CPA/POST_SIGNIN_SUCCESS'
export const POST_SIGNIN_FAILURE = 'CPA/POST_SIGNIN_FAILURE'
export const GET_ACCOUNT_REQUEST = 'CPA/GET_ACCOUNT_REQUEST'
export const GET_ACCOUNT_SUCCSESS = 'CPA/GET_ACCOUNT_SUCCSESS'
export const GET_ACCOUNT_FAILURE = 'CPA/GET_ACCOUNT_FAILURE'
export const POST_SETUP_REQUEST = 'CPA/POST_SETUP_REQUEST'
export const POST_SETUP_SUCCESS = 'CPA/POST_SETUP_SUCCESS'
export const POST_SETUP_FAILURE = 'CPA/POST_SETUP_FAILURE'
export const LOGOUT_REQUEST = 'CPA/LOGOUT_REQUEST'
export const LOGOUT_SUCCESS = 'CPA/LOGOUT_SUCCESS'

export function postSignUpRequest (data) {
  return {
    type: POST_SIGNUP_REQUEST,
    payload: data
  }
}

export function postSignUpSuccess (data) {
  return {
    type: POST_SIGNUP_SUCCESS,
    payload: data
  }
}

export function postSignUpFailure (data) {
  return {
    type: POST_SIGNUP_FAILURE,
    payload: data
  }
}

export function postSignInRequest (data) {
  return {
    type: POST_SIGNIN_REQUEST,
    payload: data
  }
}

export function postSignInSuccess (data) {
  return {
    type: POST_SIGNIN_SUCCESS,
    payload: data
  }
}

export function postSignInFailure () {
  return {
    type: POST_SIGNIN_FAILURE,
  }
}

export function getAccountRequest (data) {
  return {
    type: GET_ACCOUNT_REQUEST,
    payload: data
  }
}

export function getAccountSuccess (data) {
  return {
    type: GET_ACCOUNT_SUCCSESS,
    payload: data
  }
}

export function getAccountFailure () {
  return {
    type: GET_ACCOUNT_FAILURE
  }
}

export function postSetupRequest (data) {
  return {
    type: POST_SETUP_REQUEST,
    payload: data
  }
}

export function postSetupSuccess (data) {
  return {
    type: POST_SETUP_SUCCESS,
    payload: data
  }
}

export function postSetupFailure () {
  return {
    type: POST_SETUP_FAILURE
  }
}

export function logoutRequest () {
  return {
    type: LOGOUT_REQUEST
  }
}

export function logoutSuccess () {
  return {
    type: LOGOUT_SUCCESS
  }
}
