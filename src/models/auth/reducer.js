import { auth_a as actions } from '../_actions.register'

const cpa_data = JSON.parse(localStorage.getItem('cpa_data'));

const initialState = {
  profile: cpa_data ? cpa_data.data : null,
  signUpSucceed: false,
  isLoggedIn: cpa_data ? true : false
}

export default function reducer (state = initialState, action) {
  switch (action.type) {
    case actions.POST_SIGNUP_REQUEST:
      return {
        ...state,
        pending: true
      }
    case actions.POST_SIGNUP_SUCCESS:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    case actions.POST_SIGNUP_FAILURE:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    case actions.POST_SIGNIN_REQUEST:
      return {
        ...state,
        pending: true
      }
    case actions.POST_SIGNIN_SUCCESS:
      return {
        ...state,
        ...action.payload,
        pending: false,
        isLoggedIn: true
      }
    case actions.POST_SIGNIN_FAILURE:
      return {
        ...state,
        pending: false,
        isLoggedIn: false
      }
    case actions.POST_SETUP_REQUEST:
      return {
        ...state,
        pending: true
      }
    case actions.POST_SETUP_SUCCESS:
      return {
        ...state,
        ...action.payload,
        pending: false,
        isLoggedIn: true
      }
    case actions.POST_SETUP_FAILURE:
      return {
        ...state,
        ...action.payload,
        pending: false,
        isLoggedIn: false
      }
    case actions.GET_ACCOUNT_REQUEST:
      return {
        ...state,
        pending: true
      }
    case actions.GET_ACCOUNT_SUCCSESS:
      return {
        ...state,
        ...action.payload,
        pending: false
      }
    case actions.GET_ACCOUNT_FAILURE:
      return {
        ...state,
        pending: false
      }
    case actions.LOGOUT_SUCCESS:
      return {
        ...initialState,
        profile: null,
        isLoggedIn: false
      }
    default:
      return state
  }
}
