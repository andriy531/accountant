import { all, takeLatest, put, call } from 'redux-saga/effects'
import { auth_a as actions } from '../_actions.register'
import { postSignUpApi, postSignInApi, postSetupApi, getAccountFromInviteApi } from '../../services/api'
import { showLoading, hideLoading } from 'react-redux-loading-bar'
import Notifications from 'react-notification-system-redux'
import { push } from 'react-router-redux'

function* postSignUpFlow(action) {
  yield put(showLoading())
  try {
    const res = yield call(postSignUpApi, {
      "email": action.payload.email
    })
    yield put(actions.postSignUpSuccess({signUpSucceed: true}))
    const notificationOpts = {
      title: 'Success!',
      message: res.result,
      position: 'tr',
      autoDismiss: 5
    };
    yield put(Notifications.success(notificationOpts));
  } catch (e) {
    console.log(e);
    yield put(actions.postSignUpFailure({signUpSucceed: false}))
    const notificationOpts = {
      title: 'Something went wrong!',
      message: e.message,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.error(notificationOpts));
  } finally {
    yield put(hideLoading())
  }
}

function* postSignInFlow(action) {
  yield put(showLoading());
  try {
    const { email, password } = action.payload;
    const body = { email, password };
    const res = yield call(postSignInApi, body);
    localStorage.setItem("cpa_data", JSON.stringify(res));
    yield put(actions.postSignInSuccess({profile: res.data}));
    yield put(push('/'));
  } catch (e) {
    const notificationOpts = {
      title: 'Something went wrong!',
      message: e.message,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.error(notificationOpts));
    yield put(actions.postSignInFailure())
  } finally {
    yield put(hideLoading());
  }
}

function* postSetupFlow(action) {
  yield put(showLoading());
  try {
    const body = action.payload;
    const res = yield call(postSetupApi, body);
    localStorage.setItem("cpa_data", JSON.stringify(res));
    yield put(actions.postSetupSuccess({profile: res.data}))
    yield put(push('/'))
  } catch (e) {
    const notificationOpts = {
      title: 'Something went wrong!',
      message: e.message,
      position: 'tr',
      autoDismiss: 3
    };
    yield put(Notifications.error(notificationOpts));
    yield put(actions.postSetupFailure())
  } finally {
    yield put(hideLoading());
  }
}

function* getAccountFlow(action) {
  yield put(showLoading());
  try {
    const { invitationParams } = action.payload;
    const res = yield call(getAccountFromInviteApi, invitationParams)
    yield put(actions.getAccountSuccess({profile: res}))
  } catch (e) {
    yield put(actions.getAccountFailure())
    yield put(push('/notfound'))
  } finally {
    yield put(hideLoading());
  }
}

function* logoutFlow() {
  localStorage.removeItem("cpa_data");
  localStorage.clear();
  yield put(actions.logoutSuccess());
  yield put(push('/signin'));
}

export default function* watcher() {
  yield all([
    takeLatest(actions.POST_SIGNUP_REQUEST, postSignUpFlow),
    takeLatest(actions.POST_SIGNIN_REQUEST, postSignInFlow),
    takeLatest(actions.POST_SETUP_REQUEST, postSetupFlow),
    takeLatest(actions.GET_ACCOUNT_REQUEST, getAccountFlow),
    takeLatest(actions.LOGOUT_REQUEST, logoutFlow)
  ]);
}
