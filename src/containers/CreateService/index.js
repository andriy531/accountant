import CreateService from './CreateService'
import connector from './connector'

export default connector(CreateService)
