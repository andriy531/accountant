import { connect } from 'react-redux'
import { viewport_a, billings_a, services_a } from '../../models/_actions.register'
import { push } from 'react-router-redux'
import Notifications from 'react-notification-system-redux'

const mapActionToProps = {
  push: push,
  openCreateServiceModal: viewport_a.openCreateServiceModal,
  closeCreateServiceModal: viewport_a.closeCreateServiceModal,
  getBillingsRequest: billings_a.getBillingsRequest,
  postServiceRequest: services_a.postServiceRequest,
  updateServiceRequest: services_a.updateServiceRequest,
  notificationError: Notifications.error,
  cancelEditService: services_a.cancelEditService
}

const mapStateToProps = (state) => ({
  showCreateServiceModal: state.viewport.showCreateServiceModal,
  billings: state.billings.data,
  billingsPending: state.billings.pending,
  billingsError: state.billings.error,
  servicePending: state.services.pending,
  serviceToEdit: state.services.serviceToEdit,
  duplicateService: state.services.duplicateService
})

export default connect(mapStateToProps, mapActionToProps)
