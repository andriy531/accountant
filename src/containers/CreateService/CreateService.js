import React, { Component } from 'react'
import './index.css'
import Button from 'material-ui/Button'
import Dialog, { DialogContent } from 'material-ui/Dialog'
import Slide from 'material-ui/transitions/Slide'
import Divider from 'material-ui/Divider'
import Tabs, { Tab } from 'material-ui/Tabs'
import ScopeTab from '../../components/createService/scope'
import BillingTab from '../../components/createService/billing'
import TermsTab from '../../components/createService/terms'
import TasksTab from '../../components/createService/tasks'
import Hidden from 'material-ui/Hidden'
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar'
import IconButton from 'material-ui/IconButton'
import CloseIcon from 'material-ui-icons/Close'
import LoadingBar from 'react-redux-loading-bar'

export default class CreateService extends Component {

  constructor(props){
  	super(props);
  	this.state = {
      tabValue: 0,
      serviceName: 'My New Service',
      scope: null,
      clientTasksData: null,
      teamTasksData: null,
      billingInfo: null,
      terms: null
    };
  }

  handleTabChange = (event, tabValue) => {
    this.setState({ tabValue });
  };

  handleServiceNameChange(e) {
    const serviceName = e.target.value;
    this.setState({serviceName})
  };

  handleScopeChange(e) {
    const description = e.target.value;
    this.setState({scope: description})
  };

  handleTermsChange(e) {
    const terms = e.target.value;
    this.setState({terms})
  };

  handleTasksChange(data) {
    this.setState(data);
  }

  handleBillingInfoChange(data) {
    this.setState({billingInfo: data});
  }

  componentWillReceiveProps(nextProps) {
    //get billing types when open modal
    if(nextProps.showCreateServiceModal !== this.props.showCreateServiceModal && nextProps.showCreateServiceModal) {
      this.props.getBillingsRequest();
      if(nextProps.serviceToEdit) {
        this.setState({
          serviceName: nextProps.duplicateService ? '' : nextProps.serviceToEdit.name,
          scope: nextProps.serviceToEdit.scope || null,
          clientTasksData: nextProps.serviceToEdit.clientTasks || null,
          teamTasksData: nextProps.serviceToEdit.teamTasks || null,
          billingInfo: nextProps.serviceToEdit.billingInfo || null,
          terms: nextProps.serviceToEdit.terms || null
        })
      }
    }

    //reset state before close modal
    if(nextProps.showCreateServiceModal !== this.props.showCreateServiceModal && !nextProps.showCreateServiceModal) {
      this.setState({
        serviceName: 'My New Service',
        tabValue: 0,
        scope: null,
        clientTasksData: null,
        teamTasksData: null,
        billingInfo: null,
        terms: null
      })
    }
  }

  handleSubmitService() {
    let billingInfo = this.state.billingInfo;
    if(!billingInfo) {
      billingInfo = {
        price: 0,
        quantity: 1,
        totalValue: 0,
        billingType: this.props.billings && this.props.billings.length ? this.props.billings[0].billingType : 'regular basis'
      }
    }
    let body = {
      name: this.state.serviceName,
      terms: this.state.terms || '',
      scope: this.state.scope || '',
      billingInfo: billingInfo,
      clientTasks: this.state.clientTasksData || [],
      teamTasks: this.state.teamTasksData || []
    }

    if(this.props.serviceToEdit && !this.props.duplicateService) {
      this.props.updateServiceRequest({body, service_id: this.props.serviceToEdit.service_id})
    } else {
      this.props.postServiceRequest(body)
    }
  }

  closeCreateServiceModal() {
    this.props.closeCreateServiceModal();
    this.props.cancelEditService();
  }

  render() {
    return (
      <div>
        <Hidden smDown>
          <Dialog
            open={this.props.showCreateServiceModal}
            transition={Slide}
            id="create-service-dialog"
            >
            <p className="service-name">{this.state.serviceName}</p>
            <Divider/>
            <Tabs value={this.state.tabValue} onChange={this.handleTabChange.bind(this)} className="horizontal-tabs">
              <Tab label="Scope" className="tab"/>
              <Tab label="Billing" className="tab" disabled={!this.state.serviceName}/>
              <Tab label="Terms" className="tab" disabled={!this.state.serviceName}/>
              <Tab label="Tasks" className="tab" disabled={!this.state.serviceName}/>
            </Tabs>
            <Divider/>
            <DialogContent className="dialog-content">
              <div className="content-card">
                {this.state.tabValue === 0 && <ScopeTab
                  handleServiceNameChange={this.handleServiceNameChange.bind(this)}
                  handleScopeChange={this.handleScopeChange.bind(this)}
                  serviceName={this.state.serviceName}
                  scope={this.state.scope}
                  servicePending={this.props.servicePending}
                />}
                {this.state.tabValue === 1 && <BillingTab
                  billings={this.props.billings}
                  billingsPending={this.props.billingsPending}
                  billingsError={this.props.billingsError}
                  handleBillingInfoChange={this.handleBillingInfoChange.bind(this)}
                  billingInfo={this.state.billingInfo}
                  servicePending={this.props.servicePending}
                />}
                {this.state.tabValue === 2 && <TermsTab
                  handleTermsChange={this.handleTermsChange.bind(this)}
                  terms={this.state.terms}
                  servicePending={this.props.servicePending}
                />}
                {this.state.tabValue === 3 && <TasksTab
                  handleTasksChange={this.handleTasksChange.bind(this)}
                  clientTasksData={this.state.clientTasksData}
                  teamTasksData={this.state.teamTasksData}
                  servicePending={this.props.servicePending}
                  notificationError={this.props.notificationError}
                />}
              </div>
            </DialogContent>
            <div className="dialog-actions">
              <Button
                className="btn cancel-btn"
                onClick={() => this.closeCreateServiceModal()}
                >
                  Cancel
              </Button>
              <Button
                className="green-btn save-service-btn"
                onClick={() => this.handleSubmitService()}
                disabled={!this.state.serviceName || this.props.billingsPending || this.props.servicePending}
              >
                  {!this.props.serviceToEdit && !this.props.duplicateService && 'Save Service'}
                  {this.props.serviceToEdit && !this.props.duplicateService && 'Update Service'}
                  {this.props.duplicateService && 'Duplicate Service'}
                </Button>
            </div>
          </Dialog>
        </Hidden>
        <Hidden mdUp>
          <Dialog
            fullScreen
            open={this.props.showCreateServiceModal}
            transition={Slide}
            id="create-service-dialog-mobile"
            >
            <AppBar className="app-bar">
              <Toolbar className="toolbar">
                <IconButton color="contrast" onClick={() => this.closeCreateServiceModal()} aria-label="Close">
                  <CloseIcon />
                </IconButton>
                <p className="service-name">{this.state.serviceName}</p>
                <Button
                  onClick={() => this.handleSubmitService()}
                  className="save-service-btn"
                  disabled={!this.state.serviceName || this.props.billingsPending || this.props.servicePending}
                >
                  {!this.props.serviceToEdit && !this.props.duplicateService && 'Save'}
                  {this.props.serviceToEdit && !this.props.duplicateService && 'Update'}
                  {this.props.duplicateService && 'Copy'}
                </Button>
              </Toolbar>
              <Tabs
                value={this.state.tabValue}
                onChange={this.handleTabChange.bind(this)}
                className="horizontal-tabs-mobile"
                fullWidth
                scrollable
                scrollButtons="off"
                indicatorColor="#5bc2a8"
                >
                <Tab label="Scope" className="tab"/>
                <Tab label="Billing" className="tab" disabled={!this.state.serviceName}/>
                <Tab label="Terms" className="tab" disabled={!this.state.serviceName}/>
                <Tab label="Tasks" className="tab" disabled={!this.state.serviceName}/>
              </Tabs>
              <LoadingBar className="loadingBar"/>
            </AppBar>
            <DialogContent className="dialog-content" style={{backgroundColor: (this.state.tabValue === 3) ? '#f6f8fa' : '#fff'}}>
              <div className="content-card">
                {this.state.tabValue === 0 && <ScopeTab
                  handleServiceNameChange={this.handleServiceNameChange.bind(this)}
                  handleScopeChange={this.handleScopeChange.bind(this)}
                  serviceName={this.state.serviceName}
                  scope={this.state.scope}
                  servicePending={this.props.servicePending}
                />}
                {this.state.tabValue === 1 && <BillingTab
                  billings={this.props.billings}
                  billingsPending={this.props.billingsPending}
                  billingsError={this.props.billingsError}
                  handleBillingInfoChange={this.handleBillingInfoChange.bind(this)}
                  billingInfo={this.state.billingInfo}
                  servicePending={this.props.servicePending}
                />}
                {this.state.tabValue === 2 && <TermsTab
                  handleTermsChange={this.handleTermsChange.bind(this)}
                  terms={this.state.terms}
                  servicePending={this.props.servicePending}
                />}
                {this.state.tabValue === 3 && <TasksTab
                  handleTasksChange={this.handleTasksChange.bind(this)}
                  clientTasksData={this.state.clientTasksData}
                  teamTasksData={this.state.teamTasksData}
                  servicePending={this.props.servicePending}
                  notificationError={this.props.notificationError}
                />}
              </div>
            </DialogContent>
          </Dialog>
        </Hidden>
      </div>
    )
  }
}
