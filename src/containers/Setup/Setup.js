import React, { Component } from 'react'
import Grid from 'material-ui/Grid';
import './index.css'
import StepOne from '../../components/setup/StepOne'
import StepTwo from '../../components/setup/StepTwo'
import StaffStep from '../../components/setup/StaffStep'
import Hidden from 'material-ui/Hidden'
import LoadingBar from 'react-redux-loading-bar'
import Notifications from 'react-notification-system-redux'
import SignLeftImageView from '../../components/signLeftImageView'


export default class Setup extends Component {

  constructor(props){
  	super(props);
  	this.state = {
      step: 1
    };
  }

  componentWillMount() {
    if(this.props.isLoggedIn) {
      this.props.push('/')
    }
  }

  handleGoToStep(step, formData) {
    this.setState({step, formData: {...this.state.formData, ...formData}})
  }

  handleCpaFormSubmit(companyForm) {
    const setupData = {
      ...this.state.formData,
      ...companyForm,
      email: this.props.profile.email,
      userType: this.props.profile.userType,
      companyId: this.props.companyId
    }
    this.props.postSetupRequest(setupData);
  }

  handleStaffFormSubmit(formData) {
    const setupData = {...formData, ...this.props.profile}
    this.props.postSetupRequest(setupData);
  }

  componentDidMount() {
    const invitationParams = this.props.location.search;
    if(!this.props.isLoggedIn) {
      this.props.getAccountRequest({invitationParams})
    }
  }

  render() {
    return (
      <Grid container className="root-setup">
        <Notifications
          notifications={this.props.notifications}
        />
        <SignLeftImageView/>
        <Grid item md={8} sm={12} xs={12} className="right-side">
          <LoadingBar className="loadingBar"/>
          <Hidden smDown>
            <div>
              <div className="devider-right"/>
            </div>
          </Hidden>
          <div className="setup-container">
            {
              this.props.location.pathname === "/setupstaff" ?
              <StaffStep {...this.props} handleStaffFormSubmit={this.handleStaffFormSubmit.bind(this)}/> : this.state.step === 2 ?
              <StepTwo {...this.props} handleCpaFormSubmit={this.handleCpaFormSubmit.bind(this)} handleGoToStep={this.handleGoToStep.bind(this)} formData={this.state.formData}/> :
              <StepOne {...this.props} handleGoToStep={this.handleGoToStep.bind(this)} formData={this.state.formData}/>
            }
          </div>
        </Grid>
      </Grid>
    )
  }
}
