import Setup from './Setup'
import connector from './connector'

export default connector(Setup)
