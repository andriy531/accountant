import { connect } from 'react-redux'
import { auth_a } from '../../models/_actions.register'
import { push } from 'react-router-redux'

const mapActionToProps = {
  push: push,
  postSetupRequest: auth_a.postSetupRequest,
  getAccountRequest: auth_a.getAccountRequest
}

const mapStateToProps = (state) => ({
  notifications: state.notifications,
  pending: state.auth.pending,
  profile: state.auth.profile,
  isLoggedIn: state.auth.isLoggedIn
})

export default connect(mapStateToProps, mapActionToProps)
