import { connect } from 'react-redux'
import { clients_a } from '../../models/_actions.register'
import { push } from 'react-router-redux'

const mapActionToProps = {
  push: push,
  getClientsRequest: clients_a.getClientsRequest,
  deleteClientRequest: clients_a.deleteClientRequest,
  sendClientInvitationRequest: clients_a.sendClientInvitationRequest,
  getOneClientRequest: clients_a.getOneClientRequest,
}

const mapStateToProps = (state) => ({
  clients: state.clients.data,
  lengthOfClients: state.clients.lengthOfClients,
})

export default connect(mapStateToProps, mapActionToProps)
