import Clients from './Clients'
import connector from './connector'

export default connector(Clients)
