import React, { Component } from 'react'
import './index.css'
import CustomTable from '../../components/table'
import Paper from 'material-ui/Paper'
import LocalPostOfficeIcon from 'material-ui-icons/LocalPostOffice'
import ReactPaginate from 'react-paginate'
import KeyboardArrowLeftIcon from 'material-ui-icons/KeyboardArrowLeft'
import KeyboardArrowRightIcon from 'material-ui-icons/KeyboardArrowRight'
import IconButton from 'material-ui/IconButton'
import FaAngleRight from  'react-icons/lib/fa/angle-right'
import FilterListIcon from 'material-ui-icons/FilterList'
import CloseIcon from 'material-ui-icons/Close'
import Button from 'material-ui/Button'
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog'
import Slide from 'material-ui/transitions/Slide'
import ClientsFilters from '../../components/clients/filters'
import _ from 'lodash'
import Hidden from 'material-ui/Hidden'
import ClientsMobile from '../../components/clients/mobileList'
import { store } from '../../App'
import { push } from 'react-router-redux'

// we can set multiple props to show in one column (instead id: 'test' -> id: ['test1', 'test2'])
const columnData = [
  {
    id: ['firstName', 'lastName'],
    label: 'Name',
    width: '25%',
    showTooltip: "tags",
    tooltip: (row) => <p>{row.tags && row.tags.join(", ")}</p>,
    onClick: (row) => store.dispatch(push(`clients/${row.client_id}`))
  },
  { id: 'type', label: 'Type', width: '10%' },
  { id: 'companyName', label: 'Company name', width: '15%' },
  { id: 'status', label: 'Status', width: '10%' },
  { id: 'registrationDate', label: 'Registered', date: true, width: '10%' },
  { id: 'engagementsNumber', label: 'Engagements', numeric: true, width: '15%' },
  { id: 'fiscalEndYear', label: 'Fiscal year end', date: true, width: '15%' }
];

export default class Clients extends Component {
  constructor(props) {
  	super(props);
  	this.state = {
      page_number: 1,
      openFilters: false,
      filters: null,
      openDeleteClient: false,
      clientToDelete: null,
      openInviteClient: false,
      clientToInvite: null,
      limit: 10
    };
  }

  componentDidMount() {
    let limit = 10;
    if(window.innerWidth < 960) {
      limit = 5;
      this.setState({limit: 5})
    }
    this.props.getClientsRequest({page_number: 1, limit});
  }

  onCopyClick(row) {
    this.props.getOneClientRequest({client_id: row.client_id, duplicate: true})
  }

  onEditClick(row) {
    this.props.getOneClientRequest({client_id: row.client_id, duplicate: false})
  }

  onDeleteClick(row) {
    this.handleOpenDeleteDialog(row);
  }

  onInviteClick(row) {
    this.handleOpenInviteDialog(row);
  }

  handlePageClick(page) {
    let { selected } = page;
    const { filters, limit } = this.state;
    let page_number = ++selected;
    this.setState({page_number});
    this.props.getClientsRequest({page_number, filters, limit});
  }

  handleFilter(filters) {
    const { limit } = this.state;
    this.setState({filters, page_number: 1});
    this.props.getClientsRequest({page_number: 1, filters, limit});
    this.setState({filteredBy: _.some(filters, Boolean)});
  }

  resetFilter() {
    const { limit } = this.state;
    this.setState({ filters: null });
    this.props.getClientsRequest({page_number: 1, limit});
    this.setState({filteredBy: false});
  }

  toggleOpenFilters = event => {
    this.setState({ openFilters: !this.state.openFilters });
  }

  handleCloseDeleteDialog() {
    this.setState({openDeleteClient: false, clientToDelete: null})
  }

  handleOpenDeleteDialog(client) {
    this.setState({openDeleteClient: true, clientToDelete: client})
  }

  handleCloseInviteDialog() {
    this.setState({openInviteClient: false, clientToInvite: null})
  }

  handleOpenInviteDialog(client) {
    this.setState({openInviteClient: true, clientToInvite: client})
  }

  handleDeleteClient() {
    const { clientToDelete, filters, page_number } = this.state;
    this.props.deleteClientRequest({client_id: clientToDelete.client_id, page_number, filters });
    this.handleCloseDeleteDialog();
  }

  handleInviteClient() {
    const { clientToInvite } = this.state;
    this.props.sendClientInvitationRequest({client_id: clientToInvite.client_id});
    this.handleCloseInviteDialog();
  }

  render() {
    const { openFilters, limit } = this.state;
    return (
      <div id="clients-container">
        <Hidden smDown>
          <div>
            <p className="page-breadcrumbs"><span>People </span><FaAngleRight className="right-icon"/> Clients</p>
            <div className="table-header">
              <h2>Clients</h2>
              <div className="filter-view">
                <span className="filtered-by">{!this.state.openFilters && this.state.filteredBy && 'Filtered'}</span>
                <IconButton aria-label="Filter" className="filter-btn" onClick={(e) => this.toggleOpenFilters(e)}>
                  {openFilters ? <CloseIcon/> : <FilterListIcon/>}
                </IconButton>
              </div>
            </div>
            <Paper className={this.state.openFilters ? "filters-paper active" : "filters-paper"}>
              <ClientsFilters
                handleFilter={this.handleFilter.bind(this)}
                resetFilter={this.resetFilter.bind(this)}
              />
            </Paper>
            {this.state.openFilters && <br/>}
            <Paper className="paper">
              <CustomTable
                data={this.props.clients}
                columnData={columnData}
                showEdit
                showCopy
                showDelete
                onDeleteClick={this.onDeleteClick.bind(this)}
                onEditClick={this.onEditClick.bind(this)}
                onCopyClick={this.onCopyClick.bind(this)}
                disableDelete={(n) => !!n.engagementsNumber}
                customActions={[
                  <div action={(row) => this.onInviteClick(row)} visibility={(row) => row.status === 'offline'}><LocalPostOfficeIcon/>Send invite</div>
                ]}
              />
              <ReactPaginate
                previousLabel={<IconButton aria-label="previeus"><KeyboardArrowLeftIcon/></IconButton>}
                nextLabel={<IconButton aria-label="next"><KeyboardArrowRightIcon/></IconButton>}
                breakLabel={<a href="">...</a>}
                breakClassName={"break-me"}
                pageCount={Math.ceil(this.props.lengthOfClients/limit)}
                marginPagesDisplayed={1}
                pageRangeDisplayed={2}
                onPageChange={(page) => this.handlePageClick(page)}
                containerClassName={"pagination"}
                activeClassName={"active"}
                disabledClassName={"disabled"}
                forcePage={this.state.page_number - 1}
             />
            </Paper>
          </div>
        </Hidden>
        <Hidden mdUp>
          <ClientsMobile
            clients={this.props.clients}
            pageCount={Math.ceil(this.props.lengthOfClients/limit)}
            handlePageClick={this.handlePageClick.bind(this)}
            onDeleteClick={this.onDeleteClick.bind(this)}
            onEditClick={this.onEditClick.bind(this)}
            onCopyClick={this.onCopyClick.bind(this)}
            onInviteClick={this.onInviteClick.bind(this)}
            page_number={this.state.page_number}
            handleFilter={this.handleFilter.bind(this)}
            resetFilter={this.resetFilter.bind(this)}
            filters={this.state.filters}
            push={this.props.push}
          />
        </Hidden>
        <Dialog id="delete-dialog" open={this.state.openDeleteClient} transition={Slide} onRequestClose={() => this.handleCloseDeleteDialog()}>
          <DialogTitle>{"Are you sure you want to delete this client?"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              {this.state.clientToDelete && (this.state.clientToDelete.firstName + ' ' + this.state.clientToDelete.lastName)}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => this.handleCloseDeleteDialog()} className="cancel-btn">
              Cancel
            </Button>
            <Button onClick={() => this.handleDeleteClient()} className="delete-btn">
              Delete
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog id="invite-dialog" open={this.state.openInviteClient} transition={Slide} onRequestClose={() => this.handleCloseInviteDialog()}>
          <DialogTitle>{"Are you sure you want to send invitation to this client?"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              {this.state.clientToInvite && (this.state.clientToInvite.firstName + ' ' + this.state.clientToInvite.lastName)}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => this.handleCloseInviteDialog()} className="cancel-btn">
              Cancel
            </Button>
            <Button onClick={() => this.handleInviteClient()} className="invite-btn">
              Send invite
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}
