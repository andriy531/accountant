import React, { Component } from 'react'
import Grid from 'material-ui/Grid';
import './index.css'
import Input from 'material-ui/Input/Input';
import InputLabel from 'material-ui/Input/InputLabel';
import FormControl from 'material-ui/Form/FormControl';
import Button from 'material-ui/Button';
import ChevronRight from 'material-ui-icons/ChevronRight';
import Hidden from 'material-ui/Hidden';
import FormHelperText from 'material-ui/Form/FormHelperText';
import SignLeftImageView from '../../components/signLeftImageView';

// eslint-disable-next-line
const passwordRegExp = /^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z])(?=.*[!@#$&±%^()<>/|"\"]).{8}/;

export default class RestorePassword extends Component {

  constructor(props) {
  	super(props);
  	this.state = {
      password: null,
      confirmPassword: null,
      passwordMismatch: false
    };
  }

  componentWillMount() {
    if(this.props.isLoggedIn) {
      this.props.push('/')
    }
  }

  handlePasswordChange(e) {
    if(this.state.confirmPassword && e.target.value !== this.state.confirmPassword) {
      this.setState({password: e.target.value, requirePassword: false, passwordMismatch: true})
    } else {
      this.setState({password: e.target.value, requirePassword: false, passwordMismatch: false})
    }
    if(passwordRegExp.test(e.target.value)) {
      this.setState({passwordError: false})
    }
  }

  handlePasswordBlur() {
    if(!passwordRegExp.test(this.state.password)) {
      this.setState({passwordError: true})
    }
  }

  handleConfirmPasswordChange(e) {
    if(e.target.value !== this.state.password) {
      this.setState({confirmPassword: e.target.value, requireConfirmPassword: false, passwordMismatch: true})
    } else {
      this.setState({confirmPassword: e.target.value, requireConfirmPassword: false, passwordMismatch: false})
    }
  }

  handleFormSubmit() {
    let requiredFields = this.checkRequiredFields();
    if(!requiredFields.password && !requiredFields.confirmPassword && !this.state.passwordMismatch) {
      //this.props.push('/setup')
    }
  }

  checkRequiredFields() {
    let requiredFields = {
      password: true,
      confirmPassword: true
    }
    if(this.state.password && !this.state.passwordError) {
      requiredFields.password = false;
    } else {
       this.setState({requirePassword: true})
    }

    if(this.state.confirmPassword) {
      requiredFields.confirmPassword = false;
    } else {
       this.setState({requireConfirmPassword: true})
    }

    return requiredFields;
  }

  render() {
    return (
      <Grid container className="root-restore-pswd">
        <SignLeftImageView/>
        <Grid item md={8} sm={12} xs={12} className="right-side">
          <div className="devider-right"/>
          <div className="sign-in-block">
            <Hidden xsDown>
              <span className="tip">Have an account?</span>
            </Hidden>
            <Button className="btn sign-in-btn" onClick={() => this.props.push('/signin')}>
              Sign In <ChevronRight/>
            </Button>
          </div>
          <Grid container className="form-container">
            <Grid item xs={10} sm={5} className="form-content">
              <h1>Create a new password</h1>
              <p className="tip">Get started by filling your info</p>
              <FormControl fullWidth className="form-control">
                <InputLabel htmlFor="password">
                  New Password
                </InputLabel>
                <Input
                  id="password"
                  placeholder="Enter your password"
                  type="password"
                  onChange={(e) => this.handlePasswordChange(e)}
                  onBlur={() => this.handlePasswordBlur()}
                />
                {this.state.requirePassword && <FormHelperText error>Password is Required</FormHelperText>}
                {this.state.passwordError && <FormHelperText error>The password should be alphanumeric, contain at least one upper case character and special symbol (%, #, !, etc.). Min length 8 characters.</FormHelperText>}
              </FormControl>
              <FormControl fullWidth className="form-control">
                <InputLabel htmlFor="confirmPassword">
                  Confirm Password
                </InputLabel>
                <Input
                  id="confirmPassword"
                  placeholder="Confirm your password"
                  type="password"
                  onChange={(e) => this.handleConfirmPasswordChange(e)}
                />
                {
                  (this.state.requireConfirmPassword && <FormHelperText error>Confirm Password is Required</FormHelperText>) ||
                  (this.state.passwordMismatch && <FormHelperText error>The password doesn't match</FormHelperText>)
                }
              </FormControl>
              <Button raised className="green-btn sign-up-btn" onClick={() => this.handleFormSubmit()}>
                 Confirm
              </Button>
              <br/>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    )
  }
}
