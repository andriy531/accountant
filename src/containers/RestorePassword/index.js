import RestorePassword from './RestorePassword'
import connector from './connector'

export default connector(RestorePassword)
