import React, { Component } from 'react'
import './index.css'
import CustomTable from '../../components/table'
import Paper from 'material-ui/Paper'
import ReactPaginate from 'react-paginate'
import KeyboardArrowLeftIcon from 'material-ui-icons/KeyboardArrowLeft'
import KeyboardArrowRightIcon from 'material-ui-icons/KeyboardArrowRight'
import IconButton from 'material-ui/IconButton'
import FaAngleRight from  'react-icons/lib/fa/angle-right'
import FilterListIcon from 'material-ui-icons/FilterList'
import _ from 'lodash'
import Button from 'material-ui/Button'
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog'
import Slide from 'material-ui/transitions/Slide'
import Hidden from 'material-ui/Hidden'
import ServicesMobile from '../../components/services/mobileList'
import CloseIcon from 'material-ui-icons/Close'
import ServicesFilters from '../../components/services/filters'

const columnData = [
  { id: 'name', label: 'Service name', numeric: false },
  { id: 'billingAmount', label: 'Billing amount', numeric: false },
  { id: 'billingType', label: 'Billing type', numeric: false },
  { id: 'associatedEngagementsNumber', label: 'Engagements' },
  { id: 'dateCreated', label: 'Date created', numeric: false, date: true }
];

export default class Services extends Component {
  constructor(props){
  	super(props);
  	this.state = {
      openFilters: false,
      filters: null,
      openDeleteSevice: false,
      serviceToDelete: null,
      page_number: 1,
      limit: 10
    };
  }

  componentDidMount() {
    let limit = 10;
    if(window.innerWidth < 960) {
      limit = 5;
      this.setState({limit: 5})
    }
    this.props.getServicesRequest({page_number: 1, filters: this.state.filters, limit});
    this.props.getBillingsRequest();
  }

  onCopyClick(row) {
    this.props.getOneServiceRequest({service_id: row.service_id, duplicate: true})
  }

  onEditClick(row) {
    this.props.getOneServiceRequest({service_id: row.service_id, duplicate: false})
  }

  onDeleteClick(row) {
    this.handleOpenDeleteDialog(row);
  }

  handlePageClick(page) {
    let { selected } = page;
    const { filters, limit } = this.state;
    let page_number = ++selected;
    this.setState({page_number});
    this.props.getServicesRequest({page_number, filters, limit});
  }

  toggleOpenFilters = event => {
    this.setState({ openFilters: !this.state.openFilters });
  }

  onChangeFilter(data) {
    let filters = { ...this.state.filters, ...data };
    if(Object.keys(data)[0] === 'sortByName') {
      filters.sortByDate = '';
      filters.sortByBillingAmount = '';
    } else if(Object.keys(data)[0] === 'sortByDate') {
      filters.sortByName = '';
      filters.sortByBillingAmount = '';
    } else if(Object.keys(data)[0] === 'sortByBillingAmount') {
      filters.sortByName = '';
      filters.sortByDate = '';
    }

    this.setState({filters})
  }

  handleFilter(filters) {
    const { limit } = this.state;
    this.setState({filters, page_number: 1});
    this.props.getServicesRequest({page_number: 1, filters, limit});
    this.setState({filteredBy: _.some(filters, Boolean)});
  }

  resetFilters() {
    const { limit } = this.state;
    this.setState({ filters: null });
    this.props.getServicesRequest({page_number: 1, limit});
    this.setState({filteredBy: false});
  }

  handleChangeStartDate(date) {
    this.setState({startDate: date});
  }
  handleChangeEndDate(date) {
    this.setState({endDate: date});
  }

  handleCloseDeleteDialog() {
    this.setState({openDeleteSevice: false, serviceToDelete: null})
  }

  handleOpenDeleteDialog(service) {
    this.setState({openDeleteSevice: true, serviceToDelete: service})
  }

  handleDeleteService() {
    const { serviceToDelete, filters, page_number } = this.state;
    this.props.deleteServiceRequest({service_id: serviceToDelete.service_id, page_number, filters });
    this.handleCloseDeleteDialog();
  }

  render() {
    const { limit } = this.state;
    return (
      <div id="services-container">
        <Hidden smDown>
          <div>
            <p className="page-breadcrumbs"><span>Engagements </span><FaAngleRight className="right-icon"/> Services</p>
            <div className="table-header">
              <h2>Services</h2>
              <div className="filter-view">
                <span className="filtered-by">{!this.state.openFilters && this.state.filteredBy && 'Filtered'}</span>
                <IconButton aria-label="Filter" className="filter-btn" onClick={(e) => this.toggleOpenFilters(e)}>
                  {this.state.openFilters ? <CloseIcon/> : <FilterListIcon/>}
                </IconButton>
              </div>
            </div>
            <Paper className={this.state.openFilters ? "filters-paper active" : "filters-paper"}>
              <ServicesFilters
                handleFilter={this.handleFilter.bind(this)}
                resetFilter={this.resetFilters.bind(this)}
                billings={this.props.billings}
              />
            </Paper>
            {this.state.openFilters && <br/>}
            <Paper className="paper">
              <CustomTable
                data={this.props.services}
                columnData={columnData}
                showEdit
                showCopy
                showDelete
                onCopyClick={this.onCopyClick.bind(this)}
                onEditClick={this.onEditClick.bind(this)}
                onDeleteClick={this.onDeleteClick.bind(this)}
                disableDelete={(n) => !!n.engagementsNumber}
              />
              <ReactPaginate previousLabel={<IconButton aria-label="previeus"><KeyboardArrowLeftIcon/></IconButton>}
                           nextLabel={<IconButton aria-label="next"><KeyboardArrowRightIcon/></IconButton>}
                           breakLabel={<a href="">...</a>}
                           breakClassName={"break-me"}
                           pageCount={Math.ceil(this.props.lengthOfServices/limit)}
                           marginPagesDisplayed={1}
                           pageRangeDisplayed={2}
                           onPageChange={(page) => this.handlePageClick(page)}
                           containerClassName={"pagination"}
                           activeClassName={"active"}
                           disabledClassName={"disabled"}
                           forcePage={this.state.page_number - 1}
                         />
            </Paper>
          </div>
        </Hidden>
        <Hidden mdUp>
          <ServicesMobile
            services={this.props.services}
            pageCount={Math.ceil(this.props.lengthOfServices/limit)}
            handlePageClick={this.handlePageClick.bind(this)}
            onDeleteClick={this.onDeleteClick.bind(this)}
            onEditClick={this.onEditClick.bind(this)}
            onCopyClick={this.onCopyClick.bind(this)}
            filters={this.state.filters}
            onChangeFilter={this.onChangeFilter.bind(this)}
            handleFilter={this.handleFilter.bind(this)}
            resetFilters={this.resetFilters.bind(this)}
            billings={this.props.billings}
            filteredBy={this.state.filteredBy}
            page_number={this.state.page_number}
          />
        </Hidden>
        <Dialog id="delete-dialog" open={this.state.openDeleteSevice} transition={Slide} onRequestClose={() => this.handleCloseDeleteDialog()}>
          <DialogTitle>{"Are you sure you want to delete this service?"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              {this.state.serviceToDelete && this.state.serviceToDelete.name}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => this.handleCloseDeleteDialog()} className="cancel-btn">
              Cancel
            </Button>
            <Button onClick={() => this.handleDeleteService()} className="delete-btn">
              Delete
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}
