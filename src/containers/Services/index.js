import Services from './Services'
import connector from './connector'

export default connector(Services)
