import { connect } from 'react-redux'
import { services_a, billings_a } from '../../models/_actions.register'
import { push } from 'react-router-redux'

const mapActionToProps = {
  push: push,
  getServicesRequest: services_a.getServicesRequest,
  getBillingsRequest: billings_a.getBillingsRequest,
  getOneServiceRequest: services_a.getOneServiceRequest,
  deleteServiceRequest: services_a.deleteServiceRequest
}

const mapStateToProps = (state) => ({
  services: state.services.data,
  lengthOfServices: state.services.lengthOfServices,
  billings: state.billings.data
})

export default connect(mapStateToProps, mapActionToProps)
