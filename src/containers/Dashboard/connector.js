import { connect } from 'react-redux'
//import { auth_a } from '../../models/_actions.register'
import { push } from 'react-router-redux'

const mapActionToProps = {
  push: push,
}

const mapStateToProps = (state) => ({

})

export default connect(mapStateToProps, mapActionToProps)
