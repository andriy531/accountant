import Dashboard from './Dashboard'
import connector from './connector'

export default connector(Dashboard)
