import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { viewport_a } from '../../models/_actions.register'

const mapActionToProps = {
  push: push,
  openCreateServiceModal: viewport_a.openCreateServiceModal,
  openCreateClientModal: viewport_a.openCreateClientModal
}

const mapStateToProps = (state) => ({

})

export default connect(mapStateToProps, mapActionToProps)
