import SideMenu from './SideMenu'
import connector from './connector'
import { withRouter } from 'react-router'

export default connector(withRouter(SideMenu))
