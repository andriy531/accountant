import React, { Component } from 'react'
import './index.css'
import SquareLogo from '../../static/images/square-logo.svg'
import ButtonBase from 'material-ui/ButtonBase';
import IconDashboard from '../../static/images/icon_dashboard.png'
import IconPeople from '../../static/images/icon_people.png'
import IconSuitecase from '../../static/images/icon_suitcase.png'
import IconDoc from '../../static/images/icon_doc.png'
import IconSend from '../../static/images/icon_send.png'
import IconSettings from '../../static/images/icon_settings.png'
import VisibilitySensor from 'react-visibility-sensor'
import Card from 'material-ui/Card'
import Divider from 'material-ui/Divider'
import Fab from '../../components/fab'
import Button from 'material-ui/Button';
import AddIcon from 'material-ui-icons/Add';
import Hidden from 'material-ui/Hidden';

export default class SideMenu extends Component {

  constructor(props){
  	super(props);
  	this.state = {
      openFab: false
    };
  }

  toggleFab() {
    this.setState({openFab: !this.state.openFab})
  }

  isActive(value) {
    return ((this.props.location.pathname.includes(value)) ? 'menu-item active' : 'menu-item');
  }

  render() {
    return (
      <Hidden smDown>
        <div id="side-menu-view">
          <div className="menu-inner-view">
            <img className="square-logo" src={SquareLogo} alt="" onClick={() => this.props.push('/')}/>
            <div className="menu-component">
              <ButtonBase
                focusRipple
                className={(this.props.location.pathname === '/') ? 'menu-item active' : 'menu-item'}
                onClick={() => {this.props.push('/')}}
              >
                <img src={IconDashboard} alt="icon dashboard"/>
              </ButtonBase>
              <VisibilitySensor partialVisibility={true}>
                {({isVisible}) =>
                //for correct position submenu, you need to specify the same height for "submenu-container" and "submenu" classes
                <div className="submenu-container" style={{height: '70px'}}>
                  <Card className={isVisible ? 'submenu' : 'submenu invisible'} style={{height: '70px'}}>
                    <h4 onClick={() => {this.props.push('/')}}>Dashboard</h4>
                  </Card>
                </div>
                }
              </VisibilitySensor>
            </div>
            <div className="menu-component">
              <ButtonBase
                focusRipple
                className="menu-item">
                <img src={IconPeople} alt="icon people"/>
              </ButtonBase>
              <VisibilitySensor partialVisibility={true}>
                {({isVisible}) =>
                //for correct position submenu, you need to specify the same height for "submenu-container" and "submenu" classes
                <div className="submenu-container" style={{height: '100px'}}>
                  <Card className={isVisible ? 'submenu' : 'submenu invisible'} style={{height: '100px'}}>
                    <h4>People</h4>
                    <Divider/>
                    <p onClick={() => {this.props.push('/clients')}}>Clients</p>
                  </Card>
                </div>
                }
              </VisibilitySensor>
            </div>
            <div className="menu-component">
              <ButtonBase
                focusRipple
                className={this.isActive('/engagements')}
                onClick={() => {this.props.push('/engagements')}}>
                <img src={IconSuitecase} alt="icon suitecase"/>
              </ButtonBase>
              <VisibilitySensor>
                {({isVisible}) =>
                //for correct position submenu, you need to specify the same height for "submenu-container" and "submenu" classes
                <div className="submenu-container" style={{height: '140px'}}>
                  <Card className={isVisible ? 'submenu' : 'submenu invisible'} style={{height: '140px'}}>
                    <h4 onClick={() => {this.props.push('/engagements')}}>Engagements</h4>
                    <Divider/>
                    <p onClick={() => {this.props.push('/engagements')}}>All engagements</p>
                    <p onClick={() => {this.props.push('/services')}}>Services</p>
                  </Card>
                </div>
                }
              </VisibilitySensor>
            </div>
            <div className="menu-component">
              <ButtonBase
                focusRipple
                className="menu-item">
                <img src={IconDoc} alt="icon doc"/>
              </ButtonBase>
              <VisibilitySensor partialVisibility={true}>
                {({isVisible}) =>
                //for correct position submenu, you need to specify the same height for "submenu-container" and "submenu" classes
                <div className="submenu-container" style={{height: '100px'}}>
                  <Card className={isVisible ? 'submenu' : 'submenu invisible'} style={{height: '100px'}}>
                    <h4 onClick={() => {this.props.push('/')}}>Templates</h4>
                    <Divider/>
                    <p>Tech Stacks</p>
                  </Card>
                </div>
                }
              </VisibilitySensor>
            </div>
            <div className="menu-component">
              <ButtonBase
                focusRipple
                className="menu-item">
                <img src={IconSend} alt="icon send"/>
              </ButtonBase>
              <VisibilitySensor partialVisibility={true}>
                {({isVisible}) =>
                //for correct position submenu, you need to specify the same height for "submenu-container" and "submenu" classes
                <div className="submenu-container" style={{height: '70px'}}>
                  <Card className={isVisible ? 'submenu' : 'submenu invisible'} style={{height: '70px'}}>
                    <h4 onClick={() => {this.props.push('/')}}>Messanger</h4>
                  </Card>
                </div>
                }
              </VisibilitySensor>
            </div>
            <div className="menu-component">
              <ButtonBase
                focusRipple
                className="menu-item">
                <img src={IconSettings} alt="icon settings"/>
              </ButtonBase>
              <VisibilitySensor partialVisibility={true}>
                {({isVisible}) =>
                //for correct position submenu, you need to specify the same height for "submenu-container" and "submenu" classes
                <div className="submenu-container" style={{height: '70px'}}>
                  <Card className={isVisible ? 'submenu' : 'submenu invisible'} style={{height: '70px'}}>
                    <h4 onClick={() => {this.props.push('/')}}>Settings</h4>
                  </Card>
                </div>
                }
              </VisibilitySensor>
            </div>
          </div>
          <Button
            fab
            aria-label="add"
            className={this.state.openFab ? "main-fab-btn main-fab-btn-opened" : "main-fab-btn"}
            onClick={() => this.toggleFab()}
            >
            <AddIcon className="add-icon"/>
          </Button>
          {this.state.openFab && <Fab toggleFab={this.toggleFab.bind(this)} push={this.props.push} openCreateServiceModal={this.props.openCreateServiceModal} openCreateClientModal={this.props.openCreateClientModal}/>}
        </div>
      </Hidden>
    )
  }
}
