import React, { Component } from 'react'
import './index.css'
import Grid from 'material-ui/Grid'
import BasicInfo from '../../components/createEngagement/basicInformation'
import ServicesInfo from  '../../components/createEngagement/services'
import TechstackInfo from  '../../components/createEngagement/techstack'
import Button from 'material-ui/Button'
import Hidden from 'material-ui/Hidden'


export default class CreateEngagement extends Component {
  constructor(props){
  	super(props);
  	this.state = {
      techStackList: this.props.techStackList
    };
  }

  handleAddTechStack(logo, name, url) {
    let techStackList = this.state.techStackList;
    techStackList.push({
      logo,
      url,
      name
    });
    this.setState({techStackList: techStackList})
  }

  handleRemoveTechStack(index) {
    let techStackList = this.state.techStackList;
    techStackList.splice(index, 1);
    this.setState({techStackList: techStackList})
  }

  render() {
    return (
      <div id="create-engagement-container">
        <Grid container spacing={0}>
          <Grid item xs={12}>
            <h4><span className="pale">Engagements</span> > Create New Engagement</h4>
          </Grid>
          <Grid item xs={12} md={5}>
            <BasicInfo/>
          </Grid>
          <Grid item xs={12} md={4}>
            <ServicesInfo/>
          </Grid>
          <Grid item xs={12} md={3}>
            <TechstackInfo
              handleAddTechStack={this.handleAddTechStack.bind(this)}
              techStackList={this.state.techStackList}
              handleRemoveTechStack={this.handleRemoveTechStack.bind(this)}
            />
          </Grid>
        </Grid>
        <Hidden smUp><p className="pale" style={{textAlign: 'center'}}>You are going to create new engagement. You always could change it or remove.</p></Hidden>
        <div className="bottom-block">
          <Hidden xsDown><p className="pale">You are going to create new engagement. You always could change it or remove.</p></Hidden>
          <Button raised className="green-btn save-btn">
             Save Engagement
          </Button>
        </div>
      </div>
    )
  }
}
