import { connect } from 'react-redux'
//import { auth_a } from '../../models/_actions.register'
import { push } from 'react-router-redux'
import Quickbooks from '../../static/images/quickbooks.png'
import Xerologo from '../../static/images/xero-logo.png'

const mapActionToProps = {
  push: push,
}

const mapStateToProps = (state) => ({
  techStackList: [
    {logo: Quickbooks, url: 'https://quickbooks.intuit.com/', name: 'Quickbooks'},
    {logo: Xerologo, url: 'https://www.xero.com/', name: 'Xero'}
  ]
})

export default connect(mapStateToProps, mapActionToProps)
