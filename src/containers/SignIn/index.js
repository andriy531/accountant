import SignIn from './SignIn'
import connector from './connector'

export default connector(SignIn)
