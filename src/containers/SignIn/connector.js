import { connect } from 'react-redux'
import { auth_a } from '../../models/_actions.register'
import { push } from 'react-router-redux'

const mapActionToProps = {
  push: push,
  postSignInRequest: auth_a.postSignInRequest
}

const mapStateToProps = (state) => ({
  notifications: state.notifications,
  pending: state.auth.pending,
  isLoggedIn: state.auth.isLoggedIn
})

export default connect(mapStateToProps, mapActionToProps)
