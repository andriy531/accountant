import React, { Component } from 'react'
import Grid from 'material-ui/Grid';
import './index.css'
import Input from 'material-ui/Input/Input';
import InputLabel from 'material-ui/Input/InputLabel';
import FormControl from 'material-ui/Form/FormControl';
import Button from 'material-ui/Button';
import ChevronRight from 'material-ui-icons/ChevronRight';
import FormHelperText from 'material-ui/Form/FormHelperText';
import LoadingBar from 'react-redux-loading-bar';
import isEmail from 'validator/lib/isEmail';
import Notifications from 'react-notification-system-redux';
import SignLeftImageView from '../../components/signLeftImageView';


export default class SignIn extends Component {
  constructor(props) {
  	super(props);
  	this.state = {
      invalidEmail: false,
      email: null,
      password: null
    };
  }

  componentWillMount() {
    if(this.props.isLoggedIn) {
      this.props.push('/')
    }
  }

  handleEmailChange(e) {
    const email = e.target.value.trim();
    if(email && isEmail(email)) {
      this.setState({invalidEmail: false})
    }
    this.setState({email: email, requireEmail: false})
  }

  handlePasswordChange(e) {
    this.setState({password: e.target.value, requirePassword: false})
  }

  handleFormSubmit() {
    let requiredFields = this.checkRequiredFields();
    if(!requiredFields.email && !requiredFields.password) {
      const { email, password } = this.state;
      this.props.postSignInRequest({email, password})
    }
  }

  checkRequiredFields() {
    let requiredFields = {
      email: true,
      password: true
    }
    if(this.state.password) {
      requiredFields.password = false;
    } else {
       this.setState({requirePassword: true})
    }
    if (this.state.email && !this.state.invalidEmail) {
      requiredFields.email = false;
    } else {
      this.setState({requireEmail: true})
    }
    return requiredFields;
  }

  validateEmail() {
    if(this.state.email && isEmail(this.state.email)) {
      this.setState({invalidEmail: false})
    } else {
      this.setState({invalidEmail: this.state.email ? true : false})
    }
  }

  render() {
    return (
      <Grid container className="root-signin">
        <Notifications
          notifications={this.props.notifications}
        />
        <SignLeftImageView showText/>
        <Grid item md={8} sm={12} xs={12} className="right-side">
          <LoadingBar className="loadingBar"/>
          <div className="devider-right"/>
          <Button className="btn sign-up-btn" onClick={() => this.props.push('/signup')}>
            Sign Up <ChevronRight/>
          </Button>
          <Grid container className="form-container">
            <Grid item xs={10} sm={5} className="form-content">
              <h1>Nice to see you again!</h1>
              <p className="tip">Enter your info below</p>
              <FormControl fullWidth className="form-control">
                <InputLabel htmlFor="email">
                  Email
                </InputLabel>
                <Input
                  id="email"
                  placeholder="Enter your email"
                  onBlur={() => this.validateEmail()}
                  onChange={(e) => this.handleEmailChange(e)}
                  error={this.state.invalidEmail}
                  disabled={this.props.pending}
                />
                {
                  (this.state.invalidEmail && <FormHelperText error>Invalid Email Address</FormHelperText>) ||
                  (this.state.requireEmail && <FormHelperText error>Email Address is Required</FormHelperText>)
                }
              </FormControl>
              <FormControl fullWidth className="form-control">
                <InputLabel htmlFor="password">
                  Your password
                </InputLabel>
                <Input
                  id="password"
                  placeholder="Enter your password"
                  type="password"
                  onChange={(e) => this.handlePasswordChange(e)}
                  disabled={this.props.pending}
                />
                {this.state.requirePassword && <FormHelperText error>Password is Required</FormHelperText>}
              </FormControl>
              <Button disabled={this.props.pending} raised className="green-btn sign-in-btn" onClick={() => this.handleFormSubmit()}>
                 Sign In
              </Button>
              <br/>
              <br/>
              <Button disabled={this.props.pending} className="btn forgot-psw-btn" onClick={() => this.props.push('/forgotpswd')}>
                <span>Forgot Your Passwort?</span>
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    )
  }
}
