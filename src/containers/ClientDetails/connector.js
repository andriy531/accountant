import { connect } from 'react-redux'
import { client_a } from '../../models/_actions.register'
import { push } from 'react-router-redux'

const mapActionToProps = {
  push: push,
  getClientBasicInfoRequest: client_a.getClientBasicInfoRequest,
  getClientNotesRequest: client_a.getClientNotesRequest,
  postClientNoteRequest: client_a.postClientNoteRequest,
  deleteClientNoteRequest: client_a.deleteClientNoteRequest,
  editClientNoteRequest: client_a.editClientNoteRequest,
  updateClientBasicInfoRequest: client_a.updateClientBasicInfoRequest,
  postClientDocumentRequest: client_a.postClientDocumentRequest,
  getClientDocumentsRequest: client_a.getClientDocumentsRequest,
}

const mapStateToProps = (state) => ({
  client: state.client,
  documents: state.client.documents.data,
  lengthOfDocuments: state.client.documents.lengthOfDocuments,
})

export default connect(mapStateToProps, mapActionToProps)
