import ClientDetails from './ClientDetails'
import connector from './connector'

export default connector(ClientDetails)
