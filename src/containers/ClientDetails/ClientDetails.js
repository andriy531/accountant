import React, { Component } from 'react'
import './index.css'
import Divider from 'material-ui/Divider'
import Grid from 'material-ui/Grid'
import ClientBasicInfo from '../../components/clientDetails/basicInformation'
import ClientBasicInfoMobile from '../../components/clientDetails/basicInformationMobile'
import ClientNotes from '../../components/clientDetails/notes'
import ClientNotesMobile from '../../components/clientDetails/notesMobile'
import Documents from '../../components/clientDetails/documents'
import Tabs, { Tab } from 'material-ui/Tabs'
import FaAngleRight from  'react-icons/lib/fa/angle-right'
import Hidden from 'material-ui/Hidden'

const tabs = [
  {
    id: 'basicInfo',
    label: 'Basic info',
    component: ClientBasicInfo
  },
  {
    id: 'engagements',
    label: 'Engagements',
    component: () => <p>Engagements</p>
  },
  {
    id: 'documents',
    label: 'Documents',
    component: Documents
  },
  {
    id: 'invoices',
    label: 'Invoices',
    component: () => <p>Invoices</p>
  },
  {
    id: 'notes',
    label: 'Notes',
    component: ClientNotes
  }
];

const tabsMobile = [
  {
    id: 'basicInfo',
    label: 'Basic info',
    component: ClientBasicInfoMobile
  },
  {
    id: 'notes',
    label: 'Notes',
    component: ClientNotesMobile
  }
]

export default class ClientDetails extends Component {
  constructor(props){
  	super(props);
  	this.state = {
      tabValue: 'documents'
    };
  }

  handleTabChange(e, value) {
    this.setState({ tabValue: value });
  }

  render() {
    const { tabValue } = this.state;
    return (
      <div>
        <Hidden smDown>
          <div id="client-details-container">
            <div className="breadcrumbs-view">
              <p className="page-breadcrumbs"><span>People </span><FaAngleRight className="right-icon"/> <span>Clients</span> <FaAngleRight className="right-icon"/> Test Name</p>
            </div>
            <div id="details-container">
              <Grid container spacing={0}>
                <Grid item xs={12}>
                  <div id="client-tab-container">
                    <Tabs value={this.state.tabValue} onChange={this.handleTabChange.bind(this)} className="horizontal-tabs">
                      {tabs.map((t, i) => <Tab key={i} value={t.id} label={t.label} className="tab"/>)}
                    </Tabs>
                    <Divider/>
                    <div className="tab-view-container">
                      {tabs.map((t, i) => t.id === tabValue && <div key={i}>{<t.component {...this.props}/>}</div>)}
                    </div>
                  </div>
                </Grid>
              </Grid>
            </div>
          </div>
        </Hidden>
        <Hidden mdUp>
          <div id="client-details-mobile-container">
            <Tabs value={this.state.tabValue} onChange={this.handleTabChange.bind(this)} className="horizontal-tabs" scrollable scrollButtons="off" indicatorColor="#5cc1a7">
              {tabs.map((t, i) => <Tab key={i} value={t.id} label={t.label} className="tab"/>)}
            </Tabs>
            <div className="tab-view-container">
              {tabsMobile.map((t, i) => t.id === tabValue && <div key={i}>{<t.component {...this.props}/>}</div>)}
            </div>
          </div>
        </Hidden>
      </div>
    )
  }
}
