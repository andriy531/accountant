import NotFound from './NotFound'
import connector from './connector'

export default connector(NotFound)
