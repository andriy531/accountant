import { connect } from 'react-redux'
import { goBack } from 'react-router-redux'

const mapActionToProps = {
  goBack: goBack,
}

const mapStateToProps = (state) => ({

})

export default connect(mapStateToProps, mapActionToProps)
