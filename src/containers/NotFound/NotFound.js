import React, { Component } from 'react'
import Grid from 'material-ui/Grid';
import './index.css'
import Button from 'material-ui/Button'
import Logo from '../../components/logo'

export default class NotFound extends Component {

  render() {
    return (
      <Grid container className="root-not-found">
        <Grid item xs={12} className="container">
          <div className="devider"/>
          <Logo/>
          <Grid container className="form-container">
            <Grid item sm={6} hidden={{ smDown: true }}>
              <div className="calculator"/>
            </Grid>
            <Grid item sm={6} className="right-side">
              <p className="error-number">404</p>
              <div className="error-text-block">
                <h1>Looks like you've got lost...</h1>
                <p className="tip">
                  The page you're looking for doesn't exist<br/> or has been moved
                </p>
                <Button raised className="green-btn go-back-btn" onClick={() => this.props.goBack()}>Go Back</Button>
                <br/>
                <br/>
              </div>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    )
  }
}
