import Engagements from './Engagements'
import connector from './connector'

export default connector(Engagements)
