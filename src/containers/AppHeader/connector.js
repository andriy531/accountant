import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { auth_a } from '../../models/_actions.register'
import { viewport_a } from '../../models/_actions.register'

const mapActionToProps = {
  push: push,
  logoutRequest: auth_a.logoutRequest,
  openCreateServiceModal: viewport_a.openCreateServiceModal,
  openCreateClientModal: viewport_a.openCreateClientModal
}

const mapStateToProps = (state) => ({
  notifications: state.notifications,
  profile: state.auth.profile,
  mobileHeader: state.viewport.mobileHeader
})

export default connect(mapStateToProps, mapActionToProps)
