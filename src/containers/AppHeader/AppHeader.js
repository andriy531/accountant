import React, { Component } from 'react'
import './index.css'
import IconButton from 'material-ui/IconButton'
import ExpandMoreIcon from 'material-ui-icons/ExpandMore'
import Collapse from 'material-ui/transitions/Collapse'
import { CardContent } from 'material-ui/Card'
import Divider from 'material-ui/Divider'
import ButtonBase from 'material-ui/ButtonBase'
import Input from 'material-ui/Input/Input'
import Search from 'material-ui-icons/Search'
import Hidden from 'material-ui/Hidden'
import AppHeaderMobile from './AppHeaderMobile'
import Notifications from 'react-notification-system-redux'
import LoadingBar from 'react-redux-loading-bar'

export default class AppHeader extends Component {
  constructor(props){
  	super(props);
  	this.state = {
      expanded: false
    };
  }

  handleExpandClick () {
    this.setState({ expanded: !this.state.expanded });
  };

  render() {
    return (
      <div>
        <Notifications notifications={this.props.notifications}/>
        <Hidden smDown>
          <div id="app-header-view">
            <div className="main-section">
              <img src="http://inveritasoft.com/assets/img/logo.svg" alt="company-logo" className="company-logo"/>
              <div className="search-container">
                <Search className="search-icon"/>
                <Input
                  placeholder="Search"
                  className="search-input"
                />
              </div>
            </div>
            <div className="account-section">
              <img className="avatar" src="https://www.w3schools.com/w3images/avatar3.png" alt="avatar"/>
              <div className="text-section">
                <p><span className="hi-text">Hi,</span> {this.props.profile && this.props.profile.firstName}</p>
                <IconButton
                  className={this.state.expanded ? 'expand expandOpen' : 'expand'}
                  onClick={() => this.handleExpandClick()}
                  aria-expanded={true}
                  aria-label="Show more"
                >
                  <ExpandMoreIcon />
                </IconButton>
              </div>
              <Collapse in={this.state.expanded} transitionDuration="auto" unmountOnExit>
                <div className="dropdown">
                  <CardContent className="card-content">
                    <Divider className="devider"/>
                    <ButtonBase className="button">
                      <p>Edit My Profile</p>
                    </ButtonBase>
                    <Divider className="devider"/>
                    <ButtonBase className="button">
                      <p>Natifications</p>
                    </ButtonBase>
                    <Divider className="devider"/>
                    <ButtonBase className="button" onClick={() => this.props.logoutRequest()}>
                      <p>Log Out</p>
                    </ButtonBase>
                  </CardContent>
                </div>
              </Collapse>
            </div>
          </div>
        </Hidden>
        <Hidden mdUp>
          <AppHeaderMobile {...this.props}/>
        </Hidden>
        <LoadingBar className="loadingBar"/>
      </div>
    )
  }
}
