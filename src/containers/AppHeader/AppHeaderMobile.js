import React, { Component } from 'react'
import './mobile.css'
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar'
import IconButton from 'material-ui/IconButton'
import MenuIcon from 'material-ui-icons/Menu'
import Close from 'material-ui-icons/Close'
import Search from 'material-ui-icons/Search'
import Drawer from 'material-ui/Drawer'
import IconDashboard from '../../static/images/icon_dashboard.png'
import IconPeople from '../../static/images/icon_people.png'
import IconSuitecase from '../../static/images/icon_suitcase.png'
import IconDoc from '../../static/images/icon_doc.png'
import IconSend from '../../static/images/icon_send.png'
import IconSettings from '../../static/images/icon_settings.png'
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List'
import Divider from 'material-ui/Divider'
import Collapse from 'material-ui/transitions/Collapse'
import ExpandMore from 'material-ui-icons/ExpandMore'
import Fab from '../../components/fab'
import Button from 'material-ui/Button'
import AddIcon from 'material-ui-icons/Add'


export default class AppHeaderMobile extends Component {
  constructor(props){
  	super(props);
  	this.state = {
      openDrawer: false,
    };
  }

  toggleDrawer = (side, open) => {
    const drawerState = {};
    drawerState[side] = open;
    this.setState({ open: drawerState });
  };

  handleTopOpen = () => {
    this.setState({ openDrawer: true });
  };

  handleTopClose = () => {
    this.setState({ openDrawer: false });
  };

  handleClick = (value) => {
    this.setState({ [value]: !this.state[value] });
  };

  handleNavigateTo(path) {
    this.props.push(path);
    this.handleTopClose()
  };

  toggleFab() {
    this.setState({openFab: !this.state.openFab})
  };

  render() {
    const { mobileHeader } = this.props;
    return (
      <div id="header-mobile-view">
        <AppBar position="static" className="app-bar">
          <Toolbar className="toolbar">
            <div className="info-block">
              <IconButton
                 onClick={() => this.handleTopOpen()}
                 color="contrast" aria-label="Menu">
                <MenuIcon />
              </IconButton>
              <span className="location">{mobileHeader || this.props.location.pathname.slice(1)}</span>
            </div>
            <Search className="search-icon"/>
          </Toolbar>
        </AppBar>
        <Drawer
          anchor="top"
          open={this.state.openDrawer}
          onRequestClose={this.handleTopClose}
        >
          <div id="app-header-drawer">
            <div className="account-section">
              <img className="avatar" src="https://www.w3schools.com/w3images/avatar3.png" alt="avatar"/>
              <div className="text-section">
                <p><span className="hi-text">Hi,</span> {this.props.profile && this.props.profile.firstName}</p>
                <IconButton
                  onClick={() => this.handleTopClose()}
                  color="contrast" aria-label="Menu">
                  <Close />
                </IconButton>
              </div>
            </div>
            <List>
              <ListItem button onClick={() => this.handleNavigateTo('/')}>
                <ListItemIcon>
                  <img src={IconDashboard} alt="icon dashboard"/>
                </ListItemIcon>
                <ListItemText primary="Dashboard" />
              </ListItem>
              {/* <ListItem button>
                <ListItemIcon>
                  <img src={IconHands} alt="icon dashboard"/>
                </ListItemIcon>
                <ListItemText primary="Drafts" />
              </ListItem> */}
              <ListItem button onClick={() => this.handleClick('clients')}>
                <ListItemIcon>
                  <img src={IconPeople} alt="icon people"/>
                </ListItemIcon>
                <ListItemText primary="People" />
                <ExpandMore className={this.state.clients ? 'expand expandOpen' : 'expand'}/>
              </ListItem>
              <Collapse in={this.state.clients} transitionDuration="auto" unmountOnExit>
                <ListItem button onClick={() => this.handleNavigateTo('/clients')}>
                  <ListItemText inset primary="Clients" />
                </ListItem>
                <Divider/>
              </Collapse>
              <ListItem button onClick={() => this.handleClick('engagements')}>
                <ListItemIcon>
                  <img src={IconSuitecase} alt="icon engagements"/>
                </ListItemIcon>
                <ListItemText primary="Engagements" />
                <ExpandMore className={this.state.engagements ? 'expand expandOpen' : 'expand'}/>
              </ListItem>
              <Collapse in={this.state.engagements} transitionDuration="auto" unmountOnExit>
                <ListItem button onClick={() => this.handleNavigateTo('/engagements')}>
                  <ListItemText inset primary="All Engagements" />
                </ListItem>
                <ListItem button onClick={() => this.handleNavigateTo('/services')}>
                  <ListItemText inset primary="Services" />
                </ListItem>
                <Divider/>
              </Collapse>
              <ListItem button onClick={() => this.handleClick('templates')}>
                <ListItemIcon>
                  <img src={IconDoc} alt="icon templates"/>
                </ListItemIcon>
                <ListItemText primary="Templates" />
                <ExpandMore className={this.state.templates ? 'expand expandOpen' : 'expand'}/>
              </ListItem>
              <Collapse in={this.state.templates} transitionDuration="auto" unmountOnExit>
                <ListItem button>
                  <ListItemText inset primary="Tech Stacks" />
                </ListItem>
                <Divider/>
              </Collapse>
              <ListItem button>
                <ListItemIcon>
                  <img src={IconSend} alt="icon messanger"/>
                </ListItemIcon>
                <ListItemText primary="Messanger" />
              </ListItem>
              <ListItem button>
                <ListItemIcon>
                  <img src={IconSettings} alt="icon settings"/>
                </ListItemIcon>
                <ListItemText primary="Settings" />
              </ListItem>
            </List>
            <Divider/>
            <List>
              <ListItem button>
                <ListItemText primary="Edit my profile" />
              </ListItem>
              <ListItem button>
                <ListItemText primary="Notifications" />
              </ListItem>
              <ListItem button onClick={() => this.props.logoutRequest()}>
                <ListItemText primary="Log Out" />
              </ListItem>
            </List>
          </div>
        </Drawer>
        <Button
          fab
          aria-label="add"
          className={this.state.openFab ? "main-fab-btn main-fab-btn-opened" : "main-fab-btn"}
          onClick={() => this.toggleFab()}
          >
          <AddIcon className="add-icon"/>
        </Button>
        {this.state.openFab && <Fab toggleFab={this.toggleFab.bind(this)} push={this.props.push} openCreateServiceModal={this.props.openCreateServiceModal} openCreateClientModal={this.props.openCreateClientModal}/>}
      </div>
    )
  }
}
