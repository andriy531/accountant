import AppHeader from './AppHeader'
import connector from './connector'
import { withRouter } from 'react-router'

export default connector(withRouter(AppHeader))
