import CreateClient from './CreateClient'
import connector from './connector'

export default connector(CreateClient)
