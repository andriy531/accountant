import { connect } from 'react-redux'
import { viewport_a, clients_a } from '../../models/_actions.register'
import { push } from 'react-router-redux'

const mapActionToProps = {
  push: push,
  openCreateClientModal: viewport_a.openCreateClientModal,
  closeCreateClientModal: viewport_a.closeCreateClientModal,
  postClientRequest: clients_a.postClientRequest,
  cancelEditClient: clients_a.cancelEditClient,
  updateClientRequest: clients_a.updateClientRequest,
}

const mapStateToProps = (state) => ({
  showCreateClientModal: state.viewport.showCreateClientModal,
  pending: state.clients.pending,
  clientToEdit: state.clients.clientToEdit,
  duplicateClient: state.clients.duplicateClient
})

export default connect(mapStateToProps, mapActionToProps)
