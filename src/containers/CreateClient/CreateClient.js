import React, { Component } from 'react'
import './index.css'
import Button from 'material-ui/Button'
import Dialog, { DialogContent } from 'material-ui/Dialog'
import Slide from 'material-ui/transitions/Slide'
import Divider from 'material-ui/Divider'
//import Tabs, { Tab } from 'material-ui/Tabs'
import Hidden from 'material-ui/Hidden'
import CreateClientFirstStep from '../../components/createClient/firstStep'
import CreateClientManualyStep from '../../components/createClient/manualyStep'
import CreateClientInviteStep from '../../components/createClient/inviteStep'
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar'
import IconButton from 'material-ui/IconButton'
import CloseIcon from 'material-ui-icons/Close'

const clientTypes = ['Individual', 'Corporate', 'Non-profit'];

export default class CreateClient extends Component {

  constructor(props){
  	super(props);
  	this.state = {
      tabValue: 1,
      method: 'manualy',
      firstName: null,
      lastName: null,
      email: null,
      type: clientTypes[0],
      clientCountry: "United States",
      clientState: null,
      phone: null,
      address: null,
      taxNumber: null,
      fiscalEndYear: null,
      website: null,
      zipCode: null,
      clientCity: null,
      isFormValid: true,
    };
  }

  handleTabChange(e, value) {
    this.setState({tabValue: value})
  }

  closeCreateClientModal() {
    this.props.closeCreateClientModal();
    this.props.cancelEditClient();
  }

  handleMethodChange(value) {
    this.setState({method: value})
  }

  changeFormValid(value) {
    this.setState({isFormValid: value})
  }

  handleEmailChange(value) {
    this.setState({email: value})
  };

  handleTextFieldChange(data) {
    this.setState(data)
  }

  handleTypeChange(value) {
    this.setState({type: value})
  }

  handlePhoneChange(value) {
    this.setState({phone: value})
  }

  handleWebsiteChange(value) {
    this.setState({website: value})
  }

  handleZipCodeChange(value) {
    this.setState({zipCode: value})
  }

  handleInvite() {
    const { firstName, lastName, email, type } = this.state;
    let body = { firstName, lastName, email, type, status: "invited", isManuallyCreated: false };
    this.props.postClientRequest({body})
  }

  handleSave(status) {
    const { firstName, lastName, email, type, clientCountry, clientState, phone, address, taxNumber, fiscalEndYear, website, zipCode, clientCity } = this.state;
    const body = {
      firstName,
      lastName,
      email,
      status,
      isManuallyCreated: true,
      clientCountry,
      clientState,
      clientCity,
      type,
      phone,
      address,
      taxNumber,
      fiscalEndYear,
      website,
      zipCode
    }

    this.props.postClientRequest({body})
  }

  handleUpdate() {
    const { firstName, lastName, email, type, clientCountry, clientState, phone, address, taxNumber, fiscalEndYear, website, zipCode, clientCity } = this.state;
    const body = {
      ...this.props.clientToEdit,
      firstName,
      lastName,
      email,
      clientCountry,
      clientState,
      clientCity,
      type,
      phone,
      address,
      taxNumber,
      fiscalEndYear,
      website,
      zipCode
    }

    this.props.updateClientRequest({body, client_id: body.client_id});
  }

  handleDuplicate(status) {
    const { firstName, lastName, email, type, clientCountry, clientState, phone, address, taxNumber, fiscalEndYear, website, zipCode, clientCity } = this.state;
    const body = {
      ...this.props.clientToEdit,
      status,
      isManuallyCreated: true,
      firstName,
      lastName,
      email,
      clientCountry,
      clientState,
      clientCity,
      type,
      phone,
      address,
      taxNumber,
      fiscalEndYear,
      website,
      zipCode
    }

    delete body.client_id;
    delete body.clid;

    this.props.postClientRequest({body})
  }

  componentWillReceiveProps(nextProps) {
    //reset state before close modal
    if(nextProps.showCreateClientModal !== this.props.showCreateClientModal && !nextProps.showCreateClientModal) {
      this.setState({
        tabValue: 1,
        method: 'manualy',
        firstName: null,
        lastName: null,
        email: null,
        type: clientTypes[0],
        clientCountry: "United States",
        isFormValid: true
      })
    }

    if(nextProps.showCreateClientModal !== this.props.showCreateClientModal && nextProps.showCreateClientModal) {
      if(nextProps.clientToEdit) {
        this.setState({tabValue: 1, method: 'manualy'})
        this.setState({
          firstName: nextProps.clientToEdit.firstName || null,
          lastName: nextProps.clientToEdit.lastName || null,
          email: nextProps.clientToEdit && !nextProps.duplicateClient ? nextProps.clientToEdit.email : null,
          type: nextProps.clientToEdit.type || clientTypes[0],
          clientCountry: nextProps.clientToEdit.clientCountry || "United States",
          clientState: nextProps.clientToEdit.clientState || null,
          phone: nextProps.clientToEdit.phone || null,
          address: nextProps.clientToEdit.address || null,
          taxNumber: nextProps.clientToEdit.taxNumber || null,
          fiscalEndYear: nextProps.clientToEdit.fiscalEndYear || null,
          website: nextProps.clientToEdit.website || null,
          zipCode: nextProps.clientToEdit.zipCode || null,
          clientCity: nextProps.clientToEdit.clientCity || null,
        })
      }
    }
  }

  render() {
    const { firstName, lastName, email, type, isFormValid } = this.state;
    const { pending, clientToEdit, duplicateClient } = this.props;
    return (
      <div>
        <Hidden smDown>
          <Dialog
            open={this.props.showCreateClientModal}
            transition={Slide}
            id="create-client-dialog"
            >
            <p className="dialog-header">{clientToEdit ? (duplicateClient ? 'Duplicate Client' :'Edit Client') : 'Add New Client'} </p>
            <Divider/>
            {/* {!clientToEdit && <Tabs value={this.state.tabValue} onChange={this.handleTabChange.bind(this)} className="horizontal-tabs">
                <Tab label="Step 1" className="tab"/>
                <Tab label="Step 2" className="tab"/>
              </Tabs>
            } */}
            <Divider/>
            <DialogContent className="dialog-content">
              <div className="content-card">
                {this.state.tabValue === 0 && <CreateClientFirstStep handleMethodChange={this.handleMethodChange.bind(this)} method={this.state.method}/>}
                {
                  this.state.tabValue === 1 && (this.state.method === 'manualy' ?
                  <CreateClientManualyStep
                    handleEmailChange={this.handleEmailChange.bind(this)}
                    handleTextFieldChange={this.handleTextFieldChange.bind(this)}
                    handleTypeChange={this.handleTypeChange.bind(this)}
                    clientTypes={clientTypes}
                    type={type}
                    pending={pending}
                    handlePhoneChange={this.handlePhoneChange.bind(this)}
                    handleWebsiteChange={this.handleWebsiteChange.bind(this)}
                    handleZipCodeChange={this.handleZipCodeChange.bind(this)}
                    clientCountry={this.state.clientCountry}
                    changeFormValid={this.changeFormValid.bind(this)}
                    clientToEdit={clientToEdit}
                    duplicateClient={duplicateClient}
                  /> :
                  <CreateClientInviteStep
                    handleEmailChange={this.handleEmailChange.bind(this)}
                    handleTextFieldChange={this.handleTextFieldChange.bind(this)}
                    handleTypeChange={this.handleTypeChange.bind(this)}
                    clientTypes={clientTypes}
                    type={type}
                    pending={pending}
                  />)
                }
              </div>
            </DialogContent>
            <div className="dialog-actions">
              <Button
                className="btn cancel-btn"
                onClick={() => this.closeCreateClientModal()}
                >
                  Cancel
              </Button>
              {this.state.tabValue === 0 && <Button className="green-btn save-service-btn" onClick={() => this.setState({tabValue: 1})}>Continue</Button>}
              {
                !clientToEdit && this.state.tabValue === 1 && (this.state.method === 'manualy' ?
                <div>
                  <Button
                    className="btn cancel-btn"
                    disabled={!isFormValid || !firstName || !lastName || !email || pending}
                    onClick={() => this.handleSave('offline')}
                  >Save</Button>
                  <Button
                    className="green-btn save-service-btn"
                    disabled={!isFormValid || !firstName || !lastName || !email || pending}
                    onClick={() => this.handleSave('invited')}
                  >Save and Invite</Button></div> :
                <Button
                  className="green-btn save-service-btn"
                  onClick={() => this.handleInvite()}
                  disabled={!isFormValid || !firstName || !lastName || !email || pending}
                >Invite</Button>)
              }
              {
                clientToEdit && !duplicateClient && <Button
                  className="green-btn save-service-btn"
                  onClick={() => this.handleUpdate()}
                  disabled={!isFormValid || !firstName || !lastName || !email || pending}
                >Update</Button>
              }
              {
                clientToEdit && duplicateClient && <div>
                  <Button
                    className="btn cancel-btn"
                    onClick={() => this.handleDuplicate('offline')}
                    disabled={!isFormValid || !firstName || !lastName || !email || pending}
                  >Duplicate</Button>
                  <Button
                    className="green-btn save-service-btn"
                    onClick={() => this.handleDuplicate('invited')}
                    disabled={!isFormValid || !firstName || !lastName || !email || pending}
                  >Duplicate And Invite</Button>
              </div>
              }
            </div>
          </Dialog>
        </Hidden>
        <Hidden mdUp>
          <Dialog
            fullScreen
            open={this.props.showCreateClientModal}
            transition={Slide}
            id="create-client-dialog-mobile"
            >
            <AppBar className="app-bar">
              <Toolbar className="toolbar">
                <IconButton color="contrast" onClick={() => this.closeCreateClientModal()} aria-label="Close">
                  <CloseIcon />
                </IconButton>
                {
                  !clientToEdit && this.state.tabValue === 1 && (this.state.method === 'manualy' ?
                  <div>
                    <Button
                      className="save-client-btn"
                      disabled={!isFormValid || !firstName || !lastName || !email || pending}
                      onClick={() => this.handleSave('offline')}
                    >Save</Button>
                    <Button
                      className="save-client-btn"
                      disabled={!isFormValid || !firstName || !lastName || !email || pending}
                      onClick={() => this.handleSave('invited')}
                    >Save and Invite</Button></div> :
                  <Button
                    className="save-client-btn"
                    onClick={() => this.handleInvite()}
                    disabled={!isFormValid || !firstName || !lastName || !email || pending}
                  >Invite</Button>)
                }
                {
                  clientToEdit && !duplicateClient && <Button
                    className="save-client-btn"
                    onClick={() => this.handleUpdate()}
                    disabled={!isFormValid || !firstName || !lastName || !email || pending}
                  >Update</Button>
                }
                {
                  clientToEdit && duplicateClient && <div>
                    <Button
                      className="save-client-btn"
                      onClick={() => this.handleDuplicate('offline')}
                      disabled={!isFormValid || !firstName || !lastName || !email || pending}
                    >Copy</Button>
                    <Button
                      className="save-client-btn"
                      onClick={() => this.handleDuplicate('invited')}
                      disabled={!isFormValid || !firstName || !lastName || !email || pending}
                    >Copy And Invite</Button>
                </div>
                }
              </Toolbar>
            </AppBar>
            <DialogContent className="dialog-content">
              <div className="content-card">
                <CreateClientManualyStep
                  handleEmailChange={this.handleEmailChange.bind(this)}
                  handleTextFieldChange={this.handleTextFieldChange.bind(this)}
                  handleTypeChange={this.handleTypeChange.bind(this)}
                  clientTypes={clientTypes}
                  type={type}
                  pending={pending}
                  handlePhoneChange={this.handlePhoneChange.bind(this)}
                  handleWebsiteChange={this.handleWebsiteChange.bind(this)}
                  handleZipCodeChange={this.handleZipCodeChange.bind(this)}
                  clientCountry={this.state.clientCountry}
                  changeFormValid={this.changeFormValid.bind(this)}
                  clientToEdit={clientToEdit}
                  duplicateClient={duplicateClient}
                />
              </div>
            </DialogContent>
          </Dialog>
        </Hidden>
      </div>
    )
  }
}
