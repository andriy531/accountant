import ForgotPassword from './ForgotPassword'
import connector from './connector'

export default connector(ForgotPassword)
