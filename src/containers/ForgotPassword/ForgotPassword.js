import React, { Component } from 'react'
import Grid from 'material-ui/Grid';
import './index.css'
import Input from 'material-ui/Input/Input';
import InputLabel from 'material-ui/Input/InputLabel';
import FormControl from 'material-ui/Form/FormControl';
import Button from 'material-ui/Button';
import ChevronRight from 'material-ui-icons/ChevronRight';
import FormHelperText from 'material-ui/Form/FormHelperText';
import isEmail from 'validator/lib/isEmail';
import SignLeftImageView from '../../components/signLeftImageView';

export default class ForgotPassword extends Component {
  constructor(props) {
  	super(props);
  	this.state = {
      invalidEmail: false,
      email: null,
      password: null
    };
  }

  componentWillMount() {
    if(this.props.isLoggedIn) {
      this.props.push('/')
    }
  }

  handleEmailChange(e) {
    const email = e.target.value.trim();
    if(email && isEmail(email)) {
      this.setState({invalidEmail: false})
    }
    this.setState({email: email, requireEmail: false})
  }

  handleFormSubmit() {
    let requiredFields = this.checkRequiredFields();
    if(!requiredFields.email) {
      //this.props.push('/thanks')
    }
  }

  checkRequiredFields() {
    let requiredFields = {
      email: true
    }
    if (this.state.email && !this.state.invalidEmail) {
      requiredFields.email = false;
    } else {
      this.setState({requireEmail: true})
    }
    return requiredFields;
  }

  validateEmail() {
    if(this.state.email && isEmail(this.state.email)) {
      this.setState({invalidEmail: false})
    } else {
      this.setState({invalidEmail: this.state.email ? true : false})
    }
  }

  render() {
    return (
      <Grid container className="root-forgot-pswd">
        <SignLeftImageView/>
        <Grid item md={8} sm={12} xs={12} className="right-side">
          <div className="devider-right"/>
          <Button className="btn sign-up-btn" onClick={() => this.props.push('/signup')}>
            Sign Up <ChevronRight/>
          </Button>
          <Grid container className="form-container">
            <Grid item xs={10} sm={5} className="form-content">
              <h1>Forgot your password?</h1>
              <p className="tip">Enter the e-mail address you used when you joined and we'll send you a link to reset your password.</p>
              <FormControl fullWidth className="form-control">
                <InputLabel htmlFor="email">
                  Email address
                </InputLabel>
                <Input
                  id="email"
                  placeholder="Enter your email"
                  onChange={(e) => this.handleEmailChange(e)}
                  onBlur={() => this.validateEmail()}
                  error={this.state.invalidEmail}
                />
                {
                  (this.state.invalidEmail && <FormHelperText error>Invalid Email Address</FormHelperText>) ||
                  (this.state.requireEmail && <FormHelperText error>Email Address is Required</FormHelperText>)
                }
              </FormControl>
              <Button raised className="green-btn sign-in-btn" onClick={() => this.handleFormSubmit()}>
                 Send
              </Button>
              <br/>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    )
  }
}
