import React, { Component } from 'react'
import Grid from 'material-ui/Grid';
import './index.css'
import Button from 'material-ui/Button';
import ChevronRight from 'material-ui-icons/ChevronRight';
import Hidden from 'material-ui/Hidden';
import LoadingBar from 'react-redux-loading-bar';
import Notifications from 'react-notification-system-redux';
import Thanks from '../../components/thanksRegistration'
import SignUpForm from '../../components/signUpForm'
import SignLeftImageView from '../../components/signLeftImageView'


export default class SignUp extends Component {

  componentWillMount() {
    if(this.props.isLoggedIn) {
      this.props.push('/')
    }
  }

  render() {
    return (
      <Grid container className="root-signup">
        <Notifications
          notifications={this.props.notifications}
        />
        <SignLeftImageView showText/>
        <Grid item md={8} sm={12} xs={12} className="right-side">
          <LoadingBar className="loadingBar"/>
          <div className="devider-right"/>
          <div className="sign-in-block">
            <Hidden xsDown>
              <span className="tip">Have an account?</span>
            </Hidden>
            <Button className="btn sign-in-btn" onClick={() => this.props.push('/signin')}>
              Sign In <ChevronRight/>
            </Button>
          </div>
          <Grid container className="form-container">
            {
              this.props.signUpSucceed ? <Thanks/> : <SignUpForm pending={this.props.pending} postSignUpRequest={this.props.postSignUpRequest}/>
            }
          </Grid>
        </Grid>
      </Grid>
    )
  }
}
