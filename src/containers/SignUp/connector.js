import { connect } from 'react-redux'
import { auth_a } from '../../models/_actions.register'
import { push } from 'react-router-redux'

const mapActionToProps = {
  push: push,
  postSignUpRequest: auth_a.postSignUpRequest
}

const mapStateToProps = (state) => ({
  notifications: state.notifications,
  signUpSucceed: state.auth.signUpSucceed,
  pending: state.auth.pending,
  isLoggedIn: state.auth.isLoggedIn,
})

export default connect(mapStateToProps, mapActionToProps)
