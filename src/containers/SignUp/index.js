import SignUp from './SignUp'
import connector from './connector'

export default connector(SignUp)
